<?php

require_once "C:/xampp/htdocs/class/MarketDbConn/MarketDbConn.php";

/**
 * Klasa Market
 * S�u�y do pobierania informacji dotycz�cych dokument�w z bazy danych.
 */
final class Market
{
    private $market_db = null;

    /**
     * Konstruktor klasy
     * Inicjalizuje po��czenie z baz� danych
     */
    public function __construct()
    {
        $this->market_db = new MarketDbConn();
    }

    /**
     * Pobiera pozycje dokumentu po numerze
     * 
     * @param string $nr_dok Numer dokumentu
     * @return array Tablica z informacjami o pozycjach dokumentu
     */
    public function pobierzPozycjeDokumentu(string $nr_dok): array
    {
        $return_array = array();

        $is_rw = (substr($nr_dok, 0, 8) == '11001_RW');
        $netto = $is_rw ? "round(DCENAZAK, 2)" : "round(dcenadet / (1 + dicvat.dvalue), 2)";
        $cena_zakupu = $is_rw ? "DCENAZAK" : "round(dcenadet / (1 + dicvat.dvalue), 2)";

        $sql = "SELECT
            NLP,
            dcenadet,
            {$netto} AS netto,
            dilosc, 
            snazwa,
            sartindex,
            dicvat.svalue AS vat,
            dicjm.svalue AS jm,
            (SELECT LISTAGG(splu, ''', ''') FROM plu WHERE towar.ltowarref = plu.ltowarref) AS plu, 
            round({$cena_zakupu} * dilosc, 2) AS wartosc
        FROM dokpoz 
        INNER JOIN towar ON dokpoz.ltowarref = towar.ltowarref 
        INNER JOIN dicvat ON dokpoz.byvat = dicvat.ncode 
        INNER JOIN dicjm ON dokpoz.njmsprz = dicjm.ncode
        WHERE ldokref = (SELECT ldokref FROM dok WHERE snumer = '{$nr_dok}')
        ORDER BY nlp";
        $oci_array = $this->market_db->executeQuery($sql);

        while ($row_oci = oci_fetch_array($oci_array)) {
            $return_array[$row_oci['NLP']] = array(
                'nazwa' => $row_oci['SNAZWA'],
                'netto' => str_replace(",", ".", $row_oci['NETTO']),
                'brutto' => str_replace(",", ".", $row_oci['DCENADET']),
                'index' => $row_oci['SARTINDEX'],
                'vat' => $row_oci['VAT'],
                'jm' => $row_oci['JM'],
                'ilosc' => str_replace(",", ".", $row_oci['DILOSC']),
                'plu' => "'" . $row_oci['PLU'] . "'",
                'wartosc' => str_replace(",", ".", $row_oci['WARTOSC'])
            );
        }

        return $return_array;
    }

    /**
     * Pobiera list� numer�w dokument�w
     * 
     * @param bool $is_rw Okre�la czy pobiera� tylko dokumenty typu RW
     * @return array Tablica z numerami dokument�w
     */
    public function pobierzListeNumerowDokumentow(bool $is_rw = true): array
    {
        $return_array = array();
        $queryPart = $is_rw ? "ntyp = 20" : "ntyp = 4 AND lfirmaref = 62348159";

        $sql = "SELECT * 
            FROM dok WHERE ({$queryPart}) 
            ORDER BY czasksieg DESC
            FETCH FIRST 100 ROWS ONLY";

        $oci_array = $this->market_db->executeQuery($sql);

        while ($row_oci = oci_fetch_array($oci_array)) {
            if (strpos($row_oci['SNUMER'], "#####/mm/rr") !== false) {
                continue;
            }

            array_push($return_array, $row_oci['SNUMER']);
        }

        return $return_array;
    }

    /**
     * Pobiera date dokumentu
     *
     * @param string $nr_dok Numer dokumentu
     * @return string Data dokumentu
     */
    public function pobierzDateDokumentu(string $nr_dok): string
    {
        $date = "";

        $sql = "SELECT czasksieg FROM dok WHERE snumer LIKE '{$nr_dok}'";
        $oci_array = $this->market_db->executeQuery($sql);
        $row_oci = oci_fetch_array($oci_array);

        if ($row_oci) {
            $date = str_replace(',', '.', $row_oci['CZASKSIEG']);
            $date = date('Y-m-d', ($date - 25569) * 86400);
        }

        return $date;
    }
}