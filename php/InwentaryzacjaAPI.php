<?php

namespace Inwentaryzacja;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Managers\KierownikInwentaryzacjaManager;
use Inwentaryzacja\Managers\KierownikPanelManager;
use Inwentaryzacja\Managers\KierownikPzDrukowanieManager;
use Inwentaryzacja\Managers\KierownikPzManager;
use Inwentaryzacja\Managers\KierownikPzNowyManager;
use Inwentaryzacja\Managers\KierownikPzUzupelnienieManager;
use Inwentaryzacja\Managers\KierownikTowaryPoliczoneManager;
use Inwentaryzacja\Managers\LogowanieSystemManager;
use Inwentaryzacja\Managers\TowarDodanyManager;
use Inwentaryzacja\Managers\TowarIstniejacyManager;
use Inwentaryzacja\Managers\TowarListaManager;
use Inwentaryzacja\Managers\TowarNowyManager;
use Inwentaryzacja\Managers\TowarPodobnyManager;
use Inwentaryzacja\Managers\TowarPoliczonyManager;

use JsonException;

require_once "InwentaryzacjaManager.php";
require_once "managers/_Managers.php";

if (isset($_COOKIE["user_login"])) {
    $content = trim(file_get_contents("php://input"));
    $data = json_decode($content, true);

    if (!is_null($data)) {
        try {
            new InwentaryzacjaAPI($data);
        } catch (\Exception $e) {
            echo $e->getMessage();
            echo "</br>";
            throw $e;
        }
    }
}

/**
 * Klasa odpowiadajaca za obsluge fetchow
 */
final class InwentaryzacjaAPI
{
    private InwentaryzacjaManager $manager;

    /**
     * Wymaganym elementem w przyjetej tablicy jest "method" z nazwa metody ktora chcemy wywolac
     *
     * @param array $data Tablica, ktora musi posiadac element "method" z nazwa metody oraz pozostalymi wymaganymi przez dana metode danymi
     */
    public function __construct(array $data)
    {
        $method = $data["method"];

        if (isset($_SERVER["HTTP_REFERER"])) {
            $referer = $_SERVER["HTTP_REFERER"];
            $pageManager = basename(parse_url($referer, PHP_URL_PATH), ".php") . "Manager";
            $pageManager = "Inwentaryzacja\\Managers\\" . $pageManager;
            $this->manager = new $pageManager();
        } else {
            $this->manager = new InwentaryzacjaManager();
        }

        if (method_exists($this, $method)) {
            $wasSuccess = $this->$method($data);

            if (!$wasSuccess) {
                throw new \InvalidArgumentException("Metoda {$method} nie otrzymala wymaganych danych");
            }
        } else {
            throw new \InvalidArgumentException("Metoda {$method} nie istnieje");
        }
    }

    private function are_set(array $array, array $variables): bool
    {
        foreach ($variables as $variable) {
            if (!isset($array[$variable])) {
                return false;
            }
        }
        return true;
    }

    private function are_not_empty(...$variables): bool
    {
        foreach ($variables as $variable) {
            // Maybe cause problems when $variable is 0
            if (empty($variable)) {
                return false;
            }
        }
        return true;
    }

    private function echo_processed(string $value): void
    {
        echo json_encode(iconv("Windows-1250", "UTF-8", $value), JSON_THROW_ON_ERROR);
    }

    private function handle_display_inwentaryzacje_for_table(array $data): bool
    {
        if ($this->are_set($data, ["mj_id", "data_od", "data_do"])) {
            $mjid = $data["mj_id"];
            $dataOd = $data["data_od"];
            $dataDo = $data["data_do"];
            $nr = $data["inw_nr"] ?? "";

            if ($this->are_not_empty($mjid, $dataOd, $dataDo)) {
                $htmlInwentaryzacje = $this->manager->display_inwentaryzacje_for_table($dataOd, $dataDo, $mjid, $nr);

                $this->echo_processed($htmlInwentaryzacje);
            }
            return true;
        }
        return false;
    }

    private function handle_display_kontrahenci_for_select(array $data): bool
    {
        if ($this->are_set($data, ["input_kontrahent"])) {
            $inputKontrahent = $data["input_kontrahent"];
            $htmlKontrahenci = "";

            if (strlen($inputKontrahent) >= 3) {
                $kontrahent = iconv("UTF-8", "Windows-1250", $inputKontrahent);
                $htmlKontrahenci = $this->manager->display_kontrahenci_for_select($kontrahent);
            }

            $this->echo_processed($htmlKontrahenci);
            return true;
        }
        return false;
    }

    private function handle_display_towar_policzony_for_table(array $data): bool
    {
        if ($this->are_set($data, ["mj_id", "data_od", "data_do", "uz_id", "stan"])) {
            $mjid = $data["mj_id"];
            $dataOd = str_replace("T", " ", $data["data_od"]);
            $dataDo = str_replace("T", " ", $data["data_do"]);
            $uzid = ($data["uz_id"] == "-1") ? null : $data["uz_id"];
            $stan = $data["stan"];

            if ($this->are_not_empty($mjid, $dataOd, $dataDo, $stan)) {
                $htmlPoliczone = $this->manager->display_towar_policzony_for_table($mjid, $dataOd, $dataDo, $stan, $uzid);

                $this->echo_processed($htmlPoliczone);
            }
            return true;
        }
        return false;
    }

    private function handle_display_towar_policzony_by_inwentaryzacja_for_table(array $data): bool
    {
        if ($this->are_set($data, ["iw_id", "subtotal"])) {
            $iwid = $data["iw_id"];
            $subtotal = $data["subtotal"];

            if ($this->are_not_empty($iwid)) {
                $htmlPoliczoneByInwentaryzacja = $this->manager->display_towar_policzony_by_inwentaryzacja_for_table($iwid, $subtotal);

                $this->echo_processed($htmlPoliczoneByInwentaryzacja);
            }
            return true;
        }
        return false;
    }

    private function handle_display_uzytkownicy_for_select(array $data): bool
    {
        if ($this->are_set($data, ["mj_id"])) {
            $mjid = $data["mj_id"];

            if ($this->are_not_empty($mjid)) {
                $htmlUzytkownicy = $this->manager->display_uzytkownicy_for_select($mjid);

                $this->echo_processed($htmlUzytkownicy);
                return true;
            }
        }
        return false;
    }

    private function handle_clear_cookies(array $data): bool
    {
        InwentaryzacjaManager::clear_cookies();
        return true;
    }

    private function handle_display_towar_policzony_by_for_table(array $data): bool
    {
        if ($this->are_set($data, ["subtotal", "byUzytkownik"])) {
            $subtotal = $data["subtotal"];
            $byUzytkownik = $data["byUzytkownik"];

            $result = $this->manager->display_towar_policzony_by_for_table($subtotal, $byUzytkownik);

            $this->echo_processed($result);
            return true;
        }
        return false;
    }

    private function handle_display_towary_for_table(array $data): bool
    {
        if ($this->are_set($data, ["userInput"])) {
            $userInput = $data["userInput"];

            if ($this->are_not_empty($userInput)) {
                $result = $this->manager->display_towary_for_table($userInput);

                $this->echo_processed($result);
                return true;
            }
        }
        return false;
    }

    private function handle_display_towary_for_select(array $data): bool
    {
        if ($this->are_set($data, ["userInput"])) {
            $userInput = $data["userInput"];

            if ($this->are_not_empty($userInput)) {
                $result = $this->manager->display_towary_for_select($userInput);

                $this->echo_processed($result);
                return true;
            }
        }
        return false;
    }

    private function handle_assign_towary_policzone_to_inwentaryzacja(array $data): bool
    {
        if ($this->are_set($data, ["mj_id", "inw_nr", "data_inw", "data_od", "data_do"])) {
            $mjid = $data["mj_id"];
            $nrInw = iconv("UTF-8", "Windows-1250", $data["inw_nr"]);
            $dataInw = $data["data_inw"];
            $dataOd = str_replace("T", " ", $data["data_od"]);
            $dataDo = str_replace("T", " ", $data["data_do"]);

            if ($this->are_not_empty($mjid, $nrInw, $dataInw, $dataOd, $dataDo)) {
                $isSuccess = $this->manager->assign_towary_policzone_to_inwentaryzacja($dataOd, $dataDo, $dataInw, $nrInw, $mjid);
                $message = $isSuccess ? "OK" : "ERROR";

                $this->echo_processed($message);
            }
            return true;
        }
        return false;
    }

    private function handle_display_towary_kontrahenta_for_table(array $data): bool
    {
        if ($this->are_set($data, ["nr"])) {
            $nr = $data["nr"];

            if ($this->are_not_empty($nr)) {
                $result = $this->manager->display_towary_kontrahenta_for_table($nr);

                $this->echo_processed($result);
                return true;
            }
        }
        return false;
    }

    private function handle_assign_towar_to_kontrahent(array $data): bool
    {
        if ($this->are_set($data, ["kh_id", "tw_id"])) {
            $khid = $data["kh_id"];
            $twid = $data["tw_id"];

            if ($this->are_not_empty($khid, $twid)) {
                $result = "";

                try {
                    $result = $this->manager->assign_towar_to_kontrahent($twid, $khid);
                } catch (\Exception $e) {
                    $result = "ERROR";
                }

                $this->echo_processed($result);
                return true;
            }
        }
        return false;
    }

    private function handle_display_towar_kontrahenta_for_table(array $data): bool
    {
        if ($this->are_set($data, ["kt_id"])) {
            $ktid = $data["kt_id"];

            if ($this->are_not_empty($ktid)) {
                $result = $this->manager->display_towar_kontrahenta_for_table($ktid);

                $this->echo_processed($result);
                return true;
            }
        }
        return false;
    }

    private function handle_new_complete_dokument_pz(array $data): bool
    {
        if ($this->are_set($data, ["numer", "data", "khid", "mjid", "pozycje"])) {
            $numer = $data["numer"];
            $date = $data["data"];
            $khid = $data["khid"];
            $mjid = $data["mjid"];
            $pozycje = json_decode($data["pozycje"]);

            if ($this->are_not_empty($numer, $date, $khid, $mjid, $pozycje)) {
                $isSuccess = $this->manager->new_complete_dokument_pz($numer, $date, $khid, $mjid, $pozycje);
                $message = $isSuccess ? "OK" : "ERROR";

                $this->echo_processed($message);
                return true;
            }
        }
        return false;
    }

    private function handle_display_dokumenty_pz_for_table(array $data): bool
    {
        if ($this->are_set($data, ["data_od", "data_do", "mj_id", "kh_id", "pz_nr"])) {
            $dataOd = $data["data_od"];
            $dataDo = $data["data_do"];
            $mjid = $data["mj_id"];
            $khid = $data["kh_id"];
            $nr = $data["pz_nr"];

            if ($this->are_not_empty($mjid, $dataDo, $dataOd)) {
                $khid = empty($khid) ? -1 : $khid;
                $result = $this->manager->display_dokumenty_pz_for_table($dataOd, $dataDo, $mjid, $khid, $nr);

                $this->echo_processed($result);
                return true;
            }
        }
        return false;
    }

    private function handle_display_pozycje_by_dokument_pz_for_table(array $data): bool
    {
        if ($this->are_set($data, ["pz_id"])) {
            $pzid = $data["pz_id"];

            if ($this->are_not_empty($pzid)) {
                $result = $this->manager->display_pozycje_by_dokument_pz_for_table($pzid);

                $this->echo_processed($result);
                return true;
            }
        }
        return true;
    }

    private function handle_delete_from_ti_inwentaryzacja(array $data): bool
    {
        if ($this->are_set($data, ["tiid"])) {
            $tiid = $data["tiid"];

            if ($this->are_not_empty($tiid)) {
                $this->manager->delete_from_ti_inwentaryzacja($tiid);

                return true;
            }
        }
        return false;
    }

    private function handle_update_ilosc_in_ti_inwentaryzacja(array $data): bool
    {
        if ($this->are_set($data, ["tiid", "ilosc"])) {
            $tiid = $data["tiid"];
            $ilosc = $data["ilosc"];

            if ($this->are_not_empty($tiid, $ilosc)) {
                $this->manager->update_ilosc_in_ti_inwentaryzacja($tiid, $ilosc);

                return true;
            }
        }
        return false;
    }

    private function handle_update_nazwa_in_tw__towar(array $data): bool
    {
        if ($this->are_set($data, ["twid", "nazwa"])) {
            $twid = $data["twid"];
            $nazwa = $data["nazwa"];

            if ($this->are_not_empty($twid, $nazwa)) {
                $this->manager->update_nazwa_in_tw__towar($twid, $nazwa);

                return true;
            }
        }
        return false;
    }

    private function handle_update_jmid_in_tw__towar(array $data): bool
    {
        if ($this->are_set($data, ["twid", "jm"])) {
            $twid = $data["twid"];
            $jmid = $data["jm"];

            if ($this->are_not_empty($twid, $jmid)) {
                $this->manager->update_jmid_in_tw__towar($twid, $jmid);

                return true;
            }
        }
        return false;
    }

    private function handle_update_cena_in_pz_pozycja(array $data): bool
    {
        if ($this->are_set($data, ["cena", "ppid"])) {
            $ppid = $data["ppid"];
            $cena = $data["cena"];

            if ($this->are_not_empty($ppid, $cena)) {
                $this->manager->update_cena_in_pz_pozycja($ppid, $cena);

                return true;
            }
        }
        return false;
    }

    private function handle_get_numery_dokumentow_main(array $data): bool
    {
        if ($this->are_set($data, ["rw"])) {
            $rw = ($data["rw"] == "1");

            $result = $this->manager->get_numery_dokumentow_main($rw);

            $this->echo_processed($result);
            return true;
        }
        return false;
    }

    private function handle_get_mj_for_select(): bool
    {
        $result = $this->manager->display_mj_for_select();

        $this->echo_processed($result);
        return true;
    }

    private function handle_check_if_pz_already_exists(array $data): bool
    {
        if ($this->are_set($data, ["numer", "data"])) {
            $numer = $data["numer"];
            $data = $data["data"];

            if ($this->are_not_empty($numer, $data)) {
                $exists = $this->manager->check_if_pz_already_exists($numer, $data);

                $message = $exists ? "YES" : "NO";

                $this->echo_processed($message);
                return true;
            }
        }
        return false;
    }

    private function handle_get_date_dokumentu(array $data): bool
    {
        if ($this->are_set($data, ["numer"])) {
            $numer = $data["numer"];

            if ($this->are_not_empty($numer)) {
                $result = $this->manager->get_date_dokumentu_main($numer);

                $this->echo_processed($result);
                return true;
            }
        }
        return false;
    }

    private function handle_update_or_insert_to_ic_ceny(array $data): bool
    {
        if ($this->are_set($data, ["twid", "iwid", "cena"])) {
            $twid = intval($data["twid"]);
            $iwid = intval($data["iwid"]);
            $cena = floatval($data["cena"]);

            $this->manager->update_or_insert_to_ic_ceny($iwid, $twid, $cena);
            return true;
        }

        return false;
    }

    private function handle_display_last_activity(array $data): bool
    {
        if ($this->are_set($data, ["mjid"])) {
            $mjid = intval($data["mjid"]);

            if ($this->are_not_empty($mjid)) {
                $result = $this->manager->display_last_activity($mjid);

                $this->echo_processed($result);
                return true;
            }
        }

        return false;
    }
}