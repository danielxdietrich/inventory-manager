<?php

namespace Inwentaryzacja;

require_once "DatabaseConnection.php";

header("Content-Type: text/html; charset=Windows-1250; SameSite=Lax");
setlocale(LC_ALL, "pl_PL");

/**
 * Klasa odpowiadajaca za zarzadzanie baza danych
 */
class DatabaseManager
{
    private $connection;

    public function __construct($connection = null)
    {
        if (!is_null($connection)) {
            $this->connection = $connection;
        } else {
            $this->connection = get_connection();
        }

        if (!$this->connection) {
            exit(print_r(sqlsrv_errors(), true));
        }
    }

    public function __destruct()
    {
        sqlsrv_close($this->connection);
    }

    /**
     * Zwraca przechowywane polaczenie do bazy danych
     */
    public function get_connection()
    {
        return $this->connection;
    }

    /**
     * Metoda pozwala na wykonanie bezposredniego zapytania SQL
     *
     * @param string $sql Zapytanie
     * @param array $params Parametry
     */
    public function query(string $sql, array $params = array())
    {
        $stmt = sqlsrv_query($this->connection, $sql, $params);

        if ($stmt === false) {
            exit(print_r(sqlsrv_errors(), true));
        }

        // $this->start_logging($sql, $params);
        return $stmt;
    }

    /**
     * Metoda zwraca wszystkie zwrocone rekordy
     *
     * @param string $query Zapytanie
     * @param array $params Parametry
     */
    public function fetch_all(string $query, array $params)
    {
        $result = $this->query($query, $params);

        return sqlsrv_fetch_array($result);
    }

    /**
     * Metoda zwraca pierwszy znaleziony rekord w bazie danych
     *
     * @param string $query Zapytanie
     * @param array $params Parametry
     */
    public function fetch_first(string $query, array $params)
    {
        return $this->fetch_all($query, $params)[0];
    }

    /**
     * Metoda zwraca wartosc logiczna wskazujaca na to, czy baza danych zwrocila jakikolwiek rekord
     *
     * @param string $query Zapytanie
     * @param array $params Parametry
     * @return boolean Wartosc logiczna wskazujaca na to, czy baza danych zwrocila jakikolwiek rekord
     */
    public function has_rows(string $query, array $params = array()): bool
    {
        $result = $this->query($query, $params);

        return !empty(sqlsrv_fetch_array($result));
    }

    /**
     * Metoda monitoruje zapytania i zapisuje je do okreslonych plikow
     *
     * @param string $query Zapytanie
     * @param array $params Parametry
     * @return void
     */
    private function start_logging(string $sql, array $params): void
    {
        $dirCount = "\praktyki\daniel\count.txt";
        $dirQueries = "\praktyki\daniel\queries.txt";
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . $dirCount, @file_get_contents($_SERVER['DOCUMENT_ROOT'] . $dirCount) + 1);
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . $dirQueries, @file_get_contents($_SERVER['DOCUMENT_ROOT'] . $dirQueries) . $sql . "\n");
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . $dirQueries, @file_get_contents($_SERVER['DOCUMENT_ROOT'] . $dirQueries) . implode(",", $params) . "\n");
        file_put_contents($_SERVER['DOCUMENT_ROOT'] . $dirQueries, @file_get_contents($_SERVER['DOCUMENT_ROOT'] . $dirQueries) . '-----------------------------------' . "\n");
    }
}