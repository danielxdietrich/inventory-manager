<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Models\SpisInwentaryzacja;

final class KierownikPzDrukowanieManager extends InwentaryzacjaManager
{
    /**
     * Metoda zwraca tablice policzonych towarow w wybranej inwentaryzacji
     *
     * @param integer $iwid ID inwentaryzacji
     * @return string Lista inwentaryzacji sformatowana w HTML jako table row
     */
    public function get_towar_policzony_by_inwentaryzacja_for_pdf(int $iwid): array
    {
        $spis = new SpisInwentaryzacja($iwid);

        return array($spis, $spis->get_ilosc_pozycji());
    }

    /**
     * Metoda usuwa tymczasowe ceny z bazy danych
     * 
     * @param int $iwid ID inwentaryzacji
     * @return void
     */
    public function delete_from_ic_ceny(int $iwid): void
    {
        $query = "DELETE
            FROM ic_ceny
            WHERE ic_iwid = ?";
        $params = array($iwid);

        $this->db->query($query, $params);
    }
}