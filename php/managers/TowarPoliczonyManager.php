<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class TowarPoliczonyManager extends InwentaryzacjaManager
{
    /**
     * Metoda odpowiada za wyszukiwanie zinwentaryzowanego towaru w bazie danych oraz
     * zwracanie ich w formie sformatowanego table row w HTML
     *
     * @param boolean $subtotal Wartosc logiczna wskazujaca na to, czy korzystac z sum czesciowych
     * @return string Lista policzonych towarow sformatowana w HTML jako table row
     */
    public function display_towar_policzony_by_for_table(bool $subtotal, bool $byUzytkownik): string
    {
        $key = $byUzytkownik ? "ti_uzid" : "ti_mjid";
        $value = $byUzytkownik ? $_COOKIE["uzid"] : $_COOKIE["mjid"];
        $result = $this->get_towar_policzony_resource_where($key, $value, $subtotal);

        $str = "";
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $ilosc = floatval($row["ti_ilosc"]);
            $tiid = $row['ti_id'];
            $editButton = $subtotal ? "" : "<button onclick='editTowar({$tiid})' style='margin: 0; padding: 5px 7px;'><i class='iconoir-edit-pencil' style='display: inline-block; vertical-align: middle;'></i></button>";

            $str .= "
            <tr id='tr-{$tiid}'>
                <td id='td-nazwa-{$tiid}'>{$row['tw_nazwa']}</td>
                <td id='td-ilosc-{$tiid}'><span id='span-ilosc-{$tiid}'>{$ilosc}</span> {$_COOKIE["sl_jm"][$row["tw_jmid"]]}</td>
                <td id='td-timestamp-{$tiid}'>{$row["ti_timestamp"]->format(self::TIMESTAMP_FORMAT)}</td>
                <td>{$editButton}</td>
            </tr>";
        }

        return $str;
    }


    /**
     * Metoda zwraca resource z zapytania wyszukujacego policzone towary zgodnie z przyjętymi argumentami
     * 
     * @param string $key Nazwa kolumny
     * @param string $value Wartosc rekordu
     * @param boolean $subtotal Wartosc logiczna wskazujaca na to, czy korzystac z sum czesciowych
     */
    private function get_towar_policzony_resource_where(string $key, string $value, bool $subtotal)
    {
        $params = array($value);
        $query = null;

        if ($subtotal) {
            $query = "SELECT TOP(1000)  
                    tw_nazwa, 
                    MAX(te_code) AS te_code,
                    SUM(ti_ilosc) AS ti_ilosc, 
                    MAX(jm_nazwa) AS jm_nazwa, 
                    MAX(ti_timestamp) AS ti_timestamp, 
                    MAX(tw_jmid) AS tw_jmid,
                    MAX(ti_id) AS ti_id
                FROM ti_inwentaryzacja
                INNER JOIN tw__towar
                ON tw_id = ti_twid
                INNER JOIN sl_jm
                ON tw_jmid = jm_id
                OUTER APPLY (SELECT TOP(1) te_code AS te_code FROM te_ean WHERE te_twid = tw_id) AS te_code
                WHERE ti_iwid is NULL AND {$key} = ?
                GROUP BY tw_nazwa
                ORDER BY ti_timestamp DESC";
        } else {
            $query = "SELECT TOP(1000) 
                    tw_nazwa, 
                    (SELECT TOP(1) te_code FROM te_ean WHERE te_twid = tw_id) AS te_code,
                    ti_ilosc,
                    jm_nazwa, 
                    ti_timestamp, 
                    tw_jmid,
                    ti_id
                FROM ti_inwentaryzacja
                INNER JOIN tw__towar
                ON tw_id = ti_twid
                INNER JOIN sl_jm
                ON tw_jmid = jm_id
                WHERE ti_iwid is NULL AND {$key} = ?
                ORDER BY ti_timestamp DESC";
        }

        $result = $this->db->query($query, $params);

        return $result;
    }

    /**
     * Metoda usuwa z bazy danych rekord o podanym id
     *
     * @param integer $tiid ID policzonego towaru
     * @return void
     */
    public function delete_from_ti_inwentaryzacja(int $tiid): void
    {
        $query = "DELETE
            FROM ti_inwentaryzacja
            WHERE ti_id = ? AND ti_iwid IS NULL";
        $params = array($tiid);

        $this->db->query($query, $params);
    }

    /**
     * Metoda zmienia ilosc towaru w bazie danych
     *
     * @param integer $tiid ID policzonego towaru
     * @param float $ilosc Nowa ilosc towaru
     * @return void
     */
    public function update_ilosc_in_ti_inwentaryzacja(int $tiid, float $ilosc): void
    {
        $query = "UPDATE ti_inwentaryzacja
            SET ti_ilosc = ?
            WHERE ti_id = ? AND ti_iwid IS NULL";
        $params = array($ilosc, $tiid);

        $this->db->query($query, $params);
    }
}