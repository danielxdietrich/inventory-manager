<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class KierownikPanelManager extends InwentaryzacjaManager
{
    /**
     * Metoda zwraca obiekty klas, ktore sa podstronami menu glownego kierownika
     *
     * @return array Tablica obiektow klas (nazwa_pliku => obiekt)
     */
    private function get_kierownik_panel_subpages(): array
    {
        // Wczytanie podklas AbstractPageKierownik, ktore powinny byc widoczne
        {
            $abortDisplaying = true;
            include_once 'KierownikTowaryPoliczone.php';
            include_once 'KierownikInwentaryzacja.php';
            include_once 'KierownikPzNowy.php';
            include_once 'KierownikPz.php';
            unset($abortDisplaying);
        }

        $abstractClass = new \ReflectionClass('Inwentaryzacja\Pages\Parent\AbstractPageKierownik');
        $arrayObjects = array();

        foreach (get_declared_classes() as $className) {
            $reflectionClass = new \ReflectionClass($className);
            if ($reflectionClass->isSubclassOf($abstractClass) && !$reflectionClass->isAbstract()) {
                $kierownikPage = new $className(true);

                if ($kierownikPage->get_show_in_kierownik_panel()) {
                    $fileName = basename($reflectionClass->getFileName());
                    $arrayObjects[$fileName] = $kierownikPage;
                }
            }
        }

        $titles = array();
        foreach ($arrayObjects as $key => $row) {
            $titles[$key] = $row->get_kierownik_menu_position();
        }
        array_multisort($titles, SORT_ASC, $arrayObjects);

        return $arrayObjects;
    }


    /**
     * Metoda zwraca kod HTML menu kierownika
     * 
     * @return string Kod HTML menu kierownika
     */
    public function get_kierownik_panel_menu(): string
    {
        return $this->get_menu($this->get_kierownik_panel_subpages());
    }
}