<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class TowarDodanyManager extends InwentaryzacjaManager
{
    private string $inwentaryzacjaDate = "";
    private float $inwentaryzacjaQuantity = 0.0;

    /**
     * Metoda wstawia inwentaryzacje do bazy danych
     *
     * @param array $newInwentaryzacjaData Dane inwentaryzacji
     * - ti_timestamp: czas inwentaryzacji,
     * - ti_ilosc: ilosc towaru,
     * @return void
     */
    public function insert_into_ti_inwentaryzacja(array $newInwentaryzacjaData): void
    {
        if ($this->get_towar_exists()) {
            $ti_timestamp = $newInwentaryzacjaData["ti_timestamp"];
            $ti_ilosc = $newInwentaryzacjaData["ti_ilosc"];

            if (floatval($ti_ilosc) == 0) {
                throw new \RangeException("Wprowadzana ilosc nie moze byc zerem");
            }

            $query = "INSERT
                INTO ti_inwentaryzacja (ti_timestamp, ti_twid, ti_ilosc, ti_uzid, ti_mjid)
                VALUES (?, ?, ?, ?, ?)";

            $params = array($ti_timestamp, $this->get_twid(), $ti_ilosc, $this->get_uzid(), $this->get_mjid());

            $this->db->query($query, $params);
            $_SESSION['inwentaryzacja_added'] = 1;
        }
    }

    /**
     * Metoda odpowiada za uruchamianie procesu tworzenia nowej inwentaryzacji
     *
     * @return void
     */
    public function handle_new_inwentaryzacja(): void
    {
        $areSet = !isset($_SESSION['inwentaryzacja_added']) && isset($_POST['input-ilosc']);

        if ($areSet && !empty($_POST['input-ilosc']) && (floatval($_POST['input-ilosc']) >= self::WEIGHT_PRECISION)) {
            $date = date('Y-m-d H:i:s');

            $newInwentaryzacjaData = array(
                "ti_timestamp" => $date,
                "ti_ilosc" => trim($_POST['input-ilosc']),
            );

            $this->insert_into_ti_inwentaryzacja($newInwentaryzacjaData);
            $this->inwentaryzacjaDate = $newInwentaryzacjaData["ti_timestamp"];
            $this->inwentaryzacjaQuantity = floatval($newInwentaryzacjaData["ti_ilosc"]);
            $this->set_last_towar_cookies();
        } elseif (isset($_SESSION['inwentaryzacja_added'])) {
            InwentaryzacjaManager::head_to();
        }
    }

    public function get_inwentaryzacja_date(): string
    {
        return $this->inwentaryzacjaDate;
    }

    public function get_inwentaryzacja_quantity(): float
    {
        return $this->inwentaryzacjaQuantity;
    }

    private function set_last_towar_cookies(): void
    {
        $this->set_cookie("last_towar_nazwa", $this->get_towar_nazwa(), true);
        $this->set_cookie("last_towar_ilosc", $this->inwentaryzacjaQuantity, true);
        $this->set_cookie("last_towar_jm", $this->get_towar_jm(), true);
    }
}