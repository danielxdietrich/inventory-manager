<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class KierownikInwentaryzacjaManager extends InwentaryzacjaManager
{
    /**
     * Metoda zwraca liste zamknietych inwentaryzacji na podstawie otrzymanych argumentow
     *
     * @param string $dataOd Najwczesniejsza data zamknietych inwentaryzacji
     * @param string $dataDo Najpozniejsza data zamknietych inwentaryzacji
     * @param integer $mjid ID miejsca wykonanio inwentaryzacji
     * @param string $nr Opcjonalna nazwa inwentaryzacji
     * @return string Lista inwentaryzacji sformatowana w HTML jako table row
     */
    public function display_inwentaryzacje_for_table(string $dataOd, string $dataDo, int $mjid, string $nr = ""): string
    {
        $query = "SELECT
                iw_id,
                iw_nr,
                iw_data,
                iw_mjid
            FROM iw__inwentaryzacja
            WHERE iw_data BETWEEN ? AND ? AND iw_mjid = ? AND iw_nr LIKE ?
            ORDER BY iw_data DESC";

        $params = array($dataOd, $dataDo, $mjid, "%{$nr}%");
        $result = $this->db->query($query, $params);

        $str = "";
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $str .= "
                <tr class='tr-inwentaryzacja' id='tr-{$row["iw_id"]}' onclick='displayTowarPoliczonyByInwentaryzacja({$row["iw_id"]}, \"{$row["iw_nr"]}\", \"{$row["iw_data"]->format(self::DATE_FORMAT)}\");'>
                    <td>{$row["iw_nr"]}</td>
                    <td>{$row["iw_data"]->format(self::DATE_PRINT_FORMAT)}</td>
                </tr>";
        }

        return $str;
    }

    /**
     * Metoda zwraca tablice policzonych towarow w wybranej inwentaryzacji
     *
     * @param integer $iwid ID inwentaryzacji
     * @param bool $subtotal Czy wyswietlic sumy czesciowe
     * @return string Lista inwentaryzacji sformatowana w HTML jako table row
     */
    public function display_towar_policzony_by_inwentaryzacja_for_table(int $iwid, bool $subtotal): string
    {
        $result = $this->get_towar_policzony_by_inwentaryzacja_resource($iwid, $subtotal);

        $str = "";
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $ilosc = floatval($row['ti_ilosc']);
            $str .= "
                <tr>
                    <td>{$row['tw_nazwa']}</td>
                    <td>{$row['te_code']}</td>
                    <td>{$ilosc} {$_COOKIE["sl_jm"][$row['tw_jmid']]}</td>
                    <td>{$row['imie_nazwisko']}</td>
                </tr>";
        }

        return $str;
    }
}