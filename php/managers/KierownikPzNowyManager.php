<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;
use Market as MarketDb;

final class KierownikPzNowyManager extends InwentaryzacjaManager
{
    /**
     * Metoda odpowiada za pobieranie przypisanego do kontrahenta towaru o podanym ID oraz
     * zwracanie go w formie sformatowanego table row w HTML
     *
     * @param string $khid ID kontrahenta
     * @return string Towar sformatowany w HTML jako table row
     */
    public function display_towar_kontrahenta_for_table(int $ktid): string
    {
        $result = $this->get_kontrahent_towar_resource($ktid);

        $jm = $_COOKIE["sl_jm"][$result['tw_jmid']];
        $step = $this->get_step($jm);

        $str = "
            <tr id='tr-{$ktid}' twid='{$result['tw_id']}' class='tr-pozycja'>
                <td>{$result['tw_nazwa']}</td>
                <td>{$result['te_code']}</td>
                <td>
                    <input type='number' pattern='(^[0-9]+([\.,][0-9]+)?$' step='{$step}' min='0' id='input-ilosc-{$ktid}' oninput='updateWartosc({$ktid})' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center; margin: 0;'> 
                </td>
                <td>
                    {$jm}
                </td>
                <td>
                    <input type='number' pattern='(^[0-9]+([\.,][0-9]+)?$' step='0.01' min='0' id='input-cena-{$ktid}' oninput='updateWartosc({$ktid})' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center; margin: 0;'>
                </td>
                <td
                    <span id='input-wartosc-{$ktid}' style='text-align: center;'></span>
                </td>
            </tr>";

        return $str;
    }

    /**
     * Metoda odpowiada za wyszukiwanie towarow w bazie danych przypisanych do kontrahenta oraz
     * zwracanie ich w formie sformatowanego table row w HTML
     *
     * @param string $khid ID kontrahenta
     * @param string $nr Numer dokumentu
     * @return string Lista towarow sformatowana w HTML jako table row
     */
    public function display_towary_kontrahenta_for_table(string $nr = ""): string
    {
        $db = new MarketDb();

        $khidMain = 2;
        $arrayMarket = $db->pobierzPozycjeDokumentu($nr);

        // Pobranie jednostek miary
        $jm = $this->get_all_jm();
        foreach ($jm as $key => $value) {
            $jm[$value] = $key;
        }

        foreach ($arrayMarket as $key => $poz) {
            $ean = explode("', '", $poz['plu']);
            $arrayMarket[$key]['ean'] = str_replace("'", "", $ean[0]);

            $query = "SELECT
                    tw_id,
                    tw_jmid,
                    (SELECT kt_id FROM kh_towar WHERE kt_khid = {$khidMain} AND kt_twid = tw_id) AS kt_id
                FROM tw__towar
                WHERE tw_id IN (SELECT te_twid FROM te_ean WHERE te_code IN ({$poz['plu']}))";

            // Gdy jest towar w bazie
            if ($this->db->has_rows($query)) {
                $result = $this->db->query($query);
                $row = sqlsrv_fetch_array($result);
                $arrayMarket[$key]['tw_id'] = $row['tw_id'];
                $arrayMarket[$key]['tw_jmid'] = $row['tw_jmid'];

                // Weryfikacja czy towar jest przypisany do kontrahenta
                if ($row['kt_id']) {
                    $arrayMarket[$key]['kt_id'] = $row['kt_id'];
                } else {
                    $query = "INSERT INTO kh_towar (kt_khid, kt_twid)
                        OUTPUT Inserted.kt_id
                        VALUES (?, ?)";
                    $params = array($khidMain, $row['tw_id']);
                    $arrayMarket[$key]['kt_id'] = $this->db->fetch_first($query, $params);
                }

                // Wprowadzenie wszystkich kod�w kreskowych dla danego towaru i aktualizacja symbolu
                $query = "DELETE FROM te_ean WHERE te_code IN ({$poz['plu']}); ";
                $query .= "INSERT INTO te_ean (te_twid, te_code) VALUES ";
                $query .= "({$row['tw_id']}, " . implode("'), ({$row['tw_id']}, '", explode("', '", $poz['plu'])) . ");";
                $this->db->query($query);
            } else {
                // Za�o�enie nowego towaru
                $arrayMarket[$key]['tw_jmid'] = $jm[$poz['jm']];
                $query = "INSERT INTO tw__towar (tw_nazwa, tw_jmid, tw_symbol) OUTPUT Inserted.tw_id VALUES (?, ?, ?)";
                $params = array($poz['nazwa'], $jm[$poz['jm']], $poz['index']);
                $arrayMarket[$key]['tw_id'] = $this->db->fetch_first($query, $params);

                // Powi�zanie kontrahenta z towarem
                $query = "INSERT INTO kh_towar (kt_khid, kt_twid) OUTPUT Inserted.kt_id VALUES (?, ?)";
                $params = array($khidMain, $arrayMarket[$key]['tw_id']);
                $arrayMarket[$key]['kt_id'] = $this->db->fetch_first($query, $params);

                // Wprowadzenie kod�w kreskowych dla danego towaru
                $query = "INSERT INTO te_ean (te_twid, te_code) VALUES ";
                $query .= "({$arrayMarket[$key]['tw_id']}, " . implode("'), ({$arrayMarket[$key]['tw_id']}, '", explode("', '", $poz['plu'])) . ")";
                $this->db->query($query);
            }

            $arrayMarket[$key]['kt_id'] = $key;
        }

        // Lista towar�w do wy�wietlenia
        $str = "";
        foreach ($arrayMarket as $key => $row) {
            $jm = $_COOKIE["sl_jm"][$row['tw_jmid']];
            $step = $this->get_step($jm);
            $ilosc = floatval($row['ilosc']);
            $cena = floatval($row['netto']);

            $str .= "
                <tr id='tr-{$row['kt_id']}' twid='{$row['tw_id']}' class='tr-pozycja'>
                    <td>{$row['nazwa']}</td>
                    <td>{$row['ean']}</td>
                    <td>
                        <input type='number' value='{$ilosc}' step='0.001' min='0' class='kh-input' id='input-ilosc-{$row['kt_id']}' oninput='updateWartosc({$row['kt_id']})' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center; margin: 0;'>
                    </td>
                    <td>{$jm}</td>
                    <td>
                        <input type='number' value='{$cena}' step='0.01' min='0' class='kh-input' id='input-cena-{$row['kt_id']}' oninput='updateWartosc({$row['kt_id']})' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center; margin: 0;'>
                    </td>
                    <td style='text-align: center;'>
                        <span id='input-wartosc-{$row['kt_id']}'>{$row['wartosc']}</span>
                    </td>
                </tr>";
        }

        return $str;
    }

    /** 
     * Metoda zwraca liste numerow dokumentow z bazy danych dla glownego kontrahenta
     *
     * @param boolean $isRW Czy dokument jest typu RW
     * @return string Lista numerow dokumentow
     */
    public function get_numery_dokumentow_main(bool $isRW = true): string
    {
        $dbMarket = new MarketDb();
        $dokumenty = $dbMarket->pobierzListeNumerowDokumentow($isRW);

        $dokumentySelect = "";
        foreach ($dokumenty as $numer) {
            $dokumentySelect .= "<option value='{$numer}'>{$numer}</option>";
        }

        return $dokumentySelect;
    }

    /**
     * Metoda zwraca date dokumentu z bazy danych dla okreslonego numeru dokumentu
     * 
     * @param string $numer Numer dokumentu
     * @return string Data dokumentu
     */
    public function get_date_dokumentu_main(string $numer): string
    {
        $dbMarket = new MarketDb();
        $data = $dbMarket->pobierzDateDokumentu($numer);

        return $data;
    }

    /**
     * Metoda zwraca wartosc logiczna czy dokument PZ juz istnieje w bazie danych
     *
     * @param string $numer  Numer dokumentu
     * @param string $data  Data dokumentu
     * @return boolean  Czy dokument istnieje
     */
    public function check_if_pz_already_exists(string $numer, string $data): bool
    {
        $query = "SELECT
                pz_nr,
                pz_data
            FROM pz__dokument
            WHERE pz_nr = ?";

        $params = array($numer);

        return $this->db->has_rows($query, $params);
    }

    /**
     * Metoda wstawia nowy dokument i zwiazane z nim pozycje
     *
     * @param string $numer Numer/nazwa dokumentu
     * @param string $data Data dokumentu
     * @param string $khid ID kontrahenta
     * @param string $mjid ID miejsca
     * @param array $pozycje Tablica tablic, gdzie:
     * @return boolean Wartosc logiczna wskazujaca na to, czy operacja sie powiodla
     */
    public function new_complete_dokument_pz(string $numer, string $data, string $khid, string $mjid, array $pozycje): bool
    {
        if (sqlsrv_begin_transaction($this->db->get_connection()) === false) {
            exit(print_r(sqlsrv_errors(), true));
        }

        $pzid = $this->insert_into_pz__dokument($numer, $data, $khid, $mjid);
        $stmt = $this->insert_into_pz_pozycja($pzid, $pozycje);

        if ($stmt && is_numeric($pzid)) {
            return sqlsrv_commit($this->db->get_connection());
        } else {
            return sqlsrv_rollback($this->db->get_connection());
        }
    }

    /**
     * Metoda przypisuje towar do kontrahenta
     * 
     * @param integer $twid ID towaru
     * @param integer $khid ID kontrahenta
     * @return int ID nowego polaczenia w tabeli kh_towar
     */
    public function assign_towar_to_kontrahent(int $twid, int $khid): int
    {
        $query = "
            IF (NOT EXISTS (SELECT 1 FROM kh_towar WHERE kt_twid = ? AND kt_khid = ?))
            INSERT INTO kh_towar (kt_twid, kt_khid)
            OUTPUT Inserted.kt_id
            VALUES (?, ?)";

        $params = array($twid, $khid, $twid, $khid);
        $result = $this->db->fetch_first($query, $params);

        return $result;
    }

    /**
     * Metoda odpowiada za wyszukiwanie towarow w bazie danych na podstawie odebranego argumentu oraz
     * zwracanie ich w formie sformatowanego table row w HTML
     *
     * @param string $userInput Wyszukiwana nazwa lub kod EAN
     * @return string Lista towarow sformatowana w HTML jako table row
     */
    public function display_towary_for_select(string $userInput): string
    {
        $result = $this->get_towary_resource($userInput);

        $str = "";
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $thatEan = $row['te_code'] == '' ? '' : "({$row['te_code']})";
            $str .= "<option value='{$row['tw_id']}'>{$row['tw_nazwa']} {$thatEan}</option>\n";
        }

        return $str;
    }

    /**
     * Metoda wyszukuje w bazie danych towary przypisane do kontrahent i zwraca resource zapytania
     * 
     * @param integer $ktid ID kontrahenta
     */
    private function get_kontrahent_towar_resource(int $ktid)
    {
        $query = "SELECT 
                tw_nazwa,
                MAX(tw_id) AS tw_id,
                MAX(te_code) AS te_code, 
                MAX(tw_jmid) AS tw_jmid
            FROM kh_towar
            LEFT JOIN tw__towar
            ON kt_twid = tw_id
            LEFT JOIN te_ean
            ON kt_twid = te_twid
            WHERE kt_id = ?
            GROUP BY tw_nazwa";

        $params = array($ktid);
        $result = $this->db->fetch_all($query, $params);

        return $result;
    }

    /**
     * Metoda tworzy nowy dokument PZ na podstawie otrzymanych parametrow i zwraca jego ID
     *
     * @param string $numer Numer/nazwa dokumentu
     * @param string $data Data dokumentu
     * @param string $khid ID kontrahenta
     * @param string $mjid ID miejsca
     * @return integer ID wstawionego dokumentu
     */
    private function insert_into_pz__dokument(string $numer, string $data, string $khid, string $mjid): int
    {
        $query = "INSERT
            INTO pz__dokument (pz_nr, pz_data, pz_khid, pz_mjid)
            OUTPUT Inserted.pz_id
            VALUES (?, ?, ?, ?)";

        $params = array($numer, $data, $khid, $mjid);
        $result = $this->db->fetch_first($query, $params);

        return $result;
    }
}