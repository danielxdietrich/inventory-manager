<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class KierownikTowaryPoliczoneManager extends InwentaryzacjaManager
{
    /**
     * Metoda zwraca HTML z ostatnia aktywnoscia inwentaryzacji dla danego miejsca
     *
     * @param integer $mjid Miejsce inwentaryzacji
     * @return string HTML z ostatnia aktywnoscia inwentaryzacji dla danego miejsca
     */
    public function display_last_activity(int $mjid): string
    {
        $lastActivity = $this->get_last_activity($mjid);
        $mj = $_COOKIE["sl_mj"][$mjid];

        return "Lista policzonych towar�w &rarr; {$mj}, ostatnia aktywno��: <span class='standout-text'>{$lastActivity}</span>";
    }

    /**
     * Metoda zwraca tablice policzonych towarow zalezna od podanych parametrow
     *
     * @param integer $mjid Miejsce inwentaryzacji
     * @param string $dataOd Najwczesniejsza data liczenia towarow
     * @param string $dataDo Najpozniejsza data liczenia towarow
     * @param integer $stan Stan inwentaryzacji. 1: wszystkie, 2: otwarte, 3: zamkniete
     * @param integer|null $uzid ID uzytkownika
     * @return string Lista policzonych towarow sformatowana w HTML jako table row
     */
    public function display_towar_policzony_for_table(int $mjid, string $dataOd, string $dataDo, int $stan = 1, int $uzid = null): string
    {
        $stanRange = '1-3';
        if (!preg_match("/^[{$stanRange}]$/", $stan)) {
            throw new \InvalidArgumentException("Stan value must be be in range {$stanRange}");
        }

        $query = "SELECT 
                tw_nazwa,  
                ti_ilosc,
                tw_jmid,
                ti_timestamp, 
                (SELECT TOP(1) te_code FROM te_ean WHERE te_twid = tw_id) as te_code,
                CONCAT(uz_imie, ' ', uz_nazwisko) AS imie_nazwisko,
                CASE WHEN ti_iwid IS NOT NULL THEN '1' ELSE '0' END AS zakonczona
            FROM ti_inwentaryzacja
            INNER JOIN tw__towar
            ON ti_twid = tw_id
            INNER JOIN uz__uzytkownik
            ON ti_uzid = uz_id
            WHERE ti_timestamp BETWEEN ? AND ? AND ti_mjid = ? ";
        $params = array($dataOd, $dataDo, $mjid);

        if ($stan == 2) {
            $query .= "AND ti_iwid IS NULL ";
        } elseif ($stan == 3) {
            $query .= "AND ti_iwid IS NOT NULL ";
        }

        if (!is_null($uzid)) {
            $query .= "AND uz_id = ? ";
            array_push($params, $uzid);
        }

        $query .= " ORDER BY ti_timestamp DESC";
        $result = $this->db->query($query, $params);

        $str = "";
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $ilosc = floatval($row['ti_ilosc']);
            $zakonczona = ($row['zakonczona'] == '1') ? 'Zamkni�ta' : 'Otwarta';
            $styleColor = ($row['zakonczona'] == '1') ? 'style="margin: 0;"' : 'style="border-right: 0.25rem solid var(--next-bg-color); margin: 0;"';
            $str .= "
                <tr>
                    <td>{$row['tw_nazwa']}</td>
                    <td>{$row['te_code']}</td>
                    <td>{$ilosc} {$_COOKIE["sl_jm"][$row['tw_jmid']]}</td>
                    <td>{$row['imie_nazwisko']}</td>
                    <td><p $styleColor>{$zakonczona}</p></td>
                </tr>";
        }

        return $str;
    }

    /**
     * Metoda aktualizuje kolumne ti_iwid w okreslonym zakresie dat z NULL na id nowego rekordu w tabeli iw__inwentaryzacja
     *
     * @param integer $mjid ID miejsca inwentaryzacji
     * @param string $dataOd Poczatek inwentaryzacji
     * @param string $dataDo Koniec inwentaryzacji
     * @param string $dataInw Data wykonania inwentaryzacji
     * @param string $nrInw Numer inwentaryzacji
     * @return bool Wartosc logiczna wskazujaca na to, czy przypisanie iwid policzonym towarom sie powiodlo
     */
    public function assign_towary_policzone_to_inwentaryzacja(string $dataOd, string $dataDo, string $dataInw, string $nrInw, int $mjid): bool
    {
        if (!$this->check_all_policzony_towar($dataOd, $dataDo, $mjid)) {
            return false;
        }

        $iwid = $this->insert_into_iw__inwentaryzacja($mjid, $dataInw, $nrInw);

        $query = "UPDATE ti_inwentaryzacja
            SET ti_iwid = ?
            FROM ti_inwentaryzacja
            INNER JOIN tw__towar
            ON tw_id = ti_twid
            WHERE ti_mjid = ? AND ti_iwid IS NULL AND ti_timestamp BETWEEN ? AND ?";

        $params = array($iwid, $mjid, $dataOd, $dataDo);
        $this->db->query($query, $params);

        return true;
    }

    /**
     * Metoda sprawdza czy inwentaryzacje w okreslonym zakresie dat nie sa przypisane do zadnej inwentaryzacji
     *
     * @param integer $mjid Miejsce inwentaryzacji
     * @param string $dataOd Poczatek inwentaryzacji
     * @param string $dataDo Koniec inwentaryzacji
     * @return boolean Wartosc logiczna oznaczajaca czy wszystkie rekordy w podanym zakresie nie maja przypisanej inwentaryzacji
     */
    private function check_all_policzony_towar(string $dataOd, string $dataDo, int $mjid): bool
    {
        $query = "SELECT
                MAX(CASE
                        WHEN ti_iwid IS NULL THEN 0
                        ELSE 1
                    END)
            FROM ti_inwentaryzacja        
            INNER JOIN tw__towar
            ON tw_id = ti_twid
            WHERE ti_mjid = ? AND ti_timestamp BETWEEN ? AND ?";

        $params = array($mjid, $dataOd, $dataDo);
        $result = $this->db->fetch_first($query, $params);

        return ($result == '0');
    }

    /**
     * Tworzy nowy rekord w tabeli iw__inwentaryzacja
     *
     * @param integer $mjid Miejsce inwentaryzacji
     * @param string $dataInw Data wykonania inwentaryzacji
     * @param string $nrInw Numer inwentaryzacji
     * @return integer ID nowej inwentaryzacji
     */
    private function insert_into_iw__inwentaryzacja(int $mjid, string $dataInw, string $nrInw): int
    {
        $query = "INSERT
            INTO iw__inwentaryzacja (iw_nr, iw_data, iw_mjid)
            OUTPUT Inserted.iw_id
            VALUES (?, ?, ?)";

        $params = array($nrInw, $dataInw, $mjid);
        $result = $this->db->fetch_first($query, $params);

        return $result;
    }

    /**
     * Metoda zwraca date ostatniej aktywnosci dla danego mj
     *
     * @param integer $mjid Identyfikator miejsca
     * @return string Data ostatniej aktywnosci w formacie d/m/Y
     */
    private function get_last_activity(int $mjid): string
    {
        $query = "SELECT TOP(1)
                ti_timestamp
            FROM ti_inwentaryzacja
            WHERE ti_mjid = ?
            ORDER BY ti_timestamp DESC";
        $params = array($mjid);

        $result = $this->db->fetch_first($query, $params);

        return $result->format(self::DATE_PRINT_FORMAT);
    }

}