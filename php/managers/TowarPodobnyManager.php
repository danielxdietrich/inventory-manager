<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class TowarPodobnyManager extends InwentaryzacjaManager
{
    private array $newTowarData;
    private string $htmlTowar = "";

    /**
     * Metoda wyszukuje towary w bazie danych, gdzie zgadza sie ID jednostki miary oraz czesc nazwy i
     * zwraca ich liste w formacie HTML
     *
     * @param string $tw_nazwa Czesc nazwy wyszukiwanego towaru
     * @param string $tw_jmid Jednostka miary wyszukiwanego towaru
     * @return string Lista podobnych towarow sformatowana jako ciag przyciskow w HTML
     */
    public function display_similiar_towar_as_buttons(string $tw_nazwa, string $tw_jmid): string
    {
        $params = array("%{$tw_nazwa}%", $tw_jmid);
        $query = null;

        if ($this->is_without_ean()) {
            $query = "SELECT TOP(100)
                    tw_id,
                    tw_nazwa
                FROM tw__towar
                LEFT JOIN te_ean
                ON te_twid = tw_id
                WHERE tw_nazwa LIKE ? AND tw_jmid = ? AND te_id IS NULL";
        } else {
            $query = "SELECT TOP(100)
                    tw_id,
                    tw_nazwa,
                    te_code
                FROM tw__towar
                OUTER APPLY (SELECT TOP(1) te_code FROM te_ean WHERE te_twid = tw_id) AS te_code
                WHERE tw_nazwa LIKE ? AND tw_jmid = ?";
        }

        $result = $this->db->query($query, $params);

        $str = "";
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $code = $row["te_code"] ?? "-1";
            $showCode = ($code == '-1') ? "none" : "inline";

            $str .= "
                <button style='padding: 5px; margin-right: 0;' type='button' id='{$row["tw_id"]}' class='btn-item'>
                    <table style='margin: 0'>
                        <tr>
                            <td style='width: 55%'>{$row["tw_nazwa"]}</td>
                            <td style='width: 45%; text-align: right'><span style='display: {$showCode}'>{$code}</span></td>
                        </tr>
                    </table>
                </button>\n";
        }

        return $str;
    }

    /**
     * Metoda odpowiada za uruchomienie procesu wyszukiwania podobnego towaru
     *
     * @return void
     */
    public function handle_search_towar(): void
    {
        $areSet = !isset($_SESSION["towar_added"]) && isset($_POST["nazwa"]) && isset($_POST["ean"]) && isset($_POST["jm"]);

        if ($areSet && (!empty($_POST["nazwa"])) && !empty($_POST["ean"]) && !empty($_POST["jm"])) {
            $thatNewTowarData = array(
                "tw_nazwa" => trim($_POST["nazwa"]),
                "te_code" => trim($_POST["ean"]),
                "tw_jmid" => trim($_POST["jm"]),
            );

            $this->newTowarData = $thatNewTowarData;
            $this->htmlTowar = $this->display_similiar_towar_as_buttons($this->newTowarData["tw_nazwa"], $this->newTowarData["tw_jmid"]);
        } else {
            $this->newTowarData = array();
        }
    }

    public function get_new_towar_data(): array
    {
        return $this->newTowarData;
    }

    public function get_html_towar(): string
    {
        return $this->htmlTowar;
    }
}