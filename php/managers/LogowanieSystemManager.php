<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class LogowanieSystemManager extends InwentaryzacjaManager
{
    /**
     * Metoda zwraca wartosc logiczna wskazujaca na to, czy uzytkownik o danym loginie istnieje
     *
     * @param string $login Login uzytkownika
     * @return boolean Czy login uzytkownik o podanym loginie istnieje w bazie
     */
    public function login_exists(string $login): bool
    {
        $query = "SELECT uz_id
            FROM uz__uzytkownik
            WHERE uz_login = ?";

        $params = array($login);

        return $this->db->has_rows($query, $params);
    }

    /**
     * Metoda zmienia haslo uzytkownika o podanym loginie na nowe
     *
     * @param string $login Login uzytkownika
     * @param string $newPassword Nowe haslo
     * @return void
     */
    public function set_password(string $login, string $newPassword): void
    {
        $query = "UPDATE uz__uzytkownik 
            SET uz_haslo = ?
            WHERE uz_login = ?";

        $params = array($newPassword, $login);

        $this->db->query($query, $params);
    }

    /**
     * Metoda zwraca haslo uzytkownika z bazy danych
     *
     * @param string $login Login uzytkownika
     */
    public function get_password(string $login): ?string
    {
        $query = "SELECT uz_haslo
            FROM uz__uzytkownik
            WHERE uz_login = ?";

        $params = array($login);
        $result = $this->db->fetch_first($query, $params);

        return $result;
    }

    /**
     * Metoda zwraca wartosc logiczna wskazujaca na to, czy dany uzytkownik posiada haslo
     *
     * @param string $login Login uzytkownika
     * @return boolean Wartosc logiczna wskazujaca na to, czy uzytkownik posiada haslo
     */
    public function has_password(string $login): bool
    {
        return !is_null($this->get_password($login));
    }

    /**
     * Metoda ustawia czas waznosci wszystkich powiazanych z uzytkownikiem ciasteczek
     *
     * @param integer $expirationTime Czas waznosci. (This is a Unix timestamp so is in number of seconds since the epoch.)
     * @return void
     */
    public function set_cookies_expiration_time(int $expirationTime): void
    {
        setcookie("auth_expire", $expirationTime, $expirationTime, "/");
    }


    /**
     * Metoda ustawia ciasteczka uzytkownika oraz slownikow
     *
     * @param string $login Login uzytkownika
     * @return void
     */
    public function configure_cookies(string $login, int $expirationDate): void
    {
        $this->set_cookies_expiration_time($expirationDate);

        $this->set_user_cookies($login);
        $this->set_dictionaries_cookies($login);
    }

    /**
     * Metoda pobiera slowniki miejsca oraz jednostek miary i
     * zapisuje je w ciasteczkach. Dodatkowo, w przypadku tablicy miejsc
     * zapisywana jest w ciasteczkach aktualna tablica uzytkownikow.
     *
     * @return void
     */
    private function set_dictionaries_cookies(string $login = null): void
    {
        $jm_dict = $this->get_all_jm();
        $mj_dict = $this->get_all_mj();

        foreach ($jm_dict as $id => $nazwa) {
            $this->set_cookie("sl_jm[{$id}]", $nazwa);
        }

        foreach ($mj_dict as $id => $nazwa) {
            $this->set_cookie("sl_mj[{$id}]", $nazwa);

            if ($this->is_manager($login)) {
                $this->get_all_uzytkownicy($id);
            }
        }
    }

    /**
     * Metoda ustawia ciasteczka uzytkownika
     *
     * @param string $login Login uzytkownika
     * @return void
     */
    private function set_user_cookies(string $login): void
    {
        $this->set_cookie("user_login", $login);
        $this->get_mjid($login);
        $this->get_uzid($login);
        $this->is_manager($login);
        $this->get_full_name($login);
    }
}