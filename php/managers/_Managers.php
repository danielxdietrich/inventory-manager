<?php

$fileName = "InwentaryzacjaManager.php";
if (file_exists("../{$fileName}")) {
    require_once "../{$fileName}";
} else {
    require_once "{$fileName}";
}

if (isset($_SERVER["HTTP_REFERER"])) {
    $referer = $_SERVER["HTTP_REFERER"];
    $manager = basename(parse_url($referer, PHP_URL_PATH), ".php") . "Manager.php";
    require_once $manager;
} else {
    $managers = glob(__DIR__ . "\*Manager.php");
    foreach ($managers as $manager) {
        require_once $manager;
    }
}