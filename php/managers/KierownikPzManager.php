<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class KierownikPzManager extends InwentaryzacjaManager
{
    /**
     * Metoda zwraca liste dokumentow PZ na podstawie otrzymanych argumentow
     *
     * @param string $dataOd Najwczesniejsza data na dokumencie
     * @param string $dataDo Najpozniejsza data na dokumencie
     * @param integer $mjid ID miejsca
     * @param integer $khid Opcjonalne ID kontrahenta
     * @param string $nr Opcjonalny numer dokumentu
     * @return string Lista dokumentow sformatowana w HTML jako table row
     */
    public function display_dokumenty_pz_for_table(string $dataOd, string $dataDo, int $mjid, int $khid = -1, string $nr = ""): string
    {
        $params = array($dataOd, $dataDo, $mjid);

        $query = "SELECT
                pz_id,
                pz_nr,
                pz_data,
                pz_khid,
                pz_mjid,
                kh_nazwa
            FROM pz__dokument
            INNER JOIN kh__kontrahent
            ON kh_id = pz_khid
            WHERE pz_data BETWEEN ? AND ? AND pz_mjid = ?";

        if ($khid != -1) {
            $query .= " AND pz_khid = ? ";
            array_push($params, $khid);
        }

        if (!empty($nr)) {
            $query .= " AND pz_nr LIKE ? ";
            array_push($params, "%{$nr}%");
        }

        $query .= " ORDER BY pz_data DESC";
        $result = $this->db->query($query, $params);

        $str = "";
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $str .= "
                <tr class='tr-dokument-pz' id='tr-{$row["pz_id"]}' onclick='getPozycjeByDokumentPz({$row["pz_id"]}, \"{$row["pz_nr"]}\", \"{$row["pz_data"]->format(self::DATE_PRINT_FORMAT)}\");'>
                    <td>{$row["pz_nr"]}</td>
                    <td>{$row["kh_nazwa"]}</td>
                    <td>{$row["pz_data"]->format(self::DATE_PRINT_FORMAT)}</td>
                </tr>";
        }

        return $str;
    }

    /**
     * Metoda zwraca tablice pozycji dokumentu PZ
     *
     * @param integer $pzid ID dokumentu PZ
     * @return string Lista pozycji sformatowana w HTML jako table row
     */
    public function display_pozycje_by_dokument_pz_for_table(int $pzid): string
    {
        $result = $this->get_pozycje_pz_resource($pzid, true);
        $suma = 0;

        $str = "";
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $ilosc = floatval($row["pp_ilosc"]);
            $cena = floatval($row["pp_cena"]);
            $wartosc = round($ilosc * $cena, 2);
            $suma = round($suma + $wartosc, 2);

            $str .= "
                <tr>
                    <td>{$row["tw_nazwa"]}</td>
                    <td>{$row["te_code"]}</td>
                    <td>{$ilosc} {$_COOKIE["sl_jm"][$row["tw_jmid"]]}</td>
                    <td>{$cena}</td>
                    <td>{$wartosc}</td>
                </tr>";
        }

        $str .= "
                <tr style='display: none;'>
                    <td colspan='4'>Suma</td>
                    <td>{$suma}</td>
                </tr>";

        return $str;
    }
}