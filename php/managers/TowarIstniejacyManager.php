<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class TowarIstniejacyManager extends InwentaryzacjaManager
{
    /**
     * Metoda odpowiada za uruchamianie procesu tworzenia nowego towaru
     *
     * @return void
     */
    public function handle_new_towar(): bool
    {
        $displayPage = true;
        $areSet = isset($_POST["nazwa"]) && isset($_POST["ean"]) && isset($_POST["jm"]);

        if ($areSet && isset($_POST["towar_z_listy"]) && ($_POST["towar_z_listy"] == 1)) {
            // Po wybraniu towaru z listy
            $newTowarData = array(
                "tw_nazwa" => trim($_POST["nazwa"]),
                "te_code" => trim($_POST["ean"]),
                "tw_jmid" => trim($_POST["jm"]),
                "tw_id" => trim($_POST["twid"])
            );

            $this->set_towar_nazwa($newTowarData["tw_nazwa"]);
            $this->set_towar_jmid($newTowarData["tw_jmid"]);
        } elseif ($areSet && !isset($_SESSION["towar_added"]) && !empty($_POST["nazwa"]) && !empty($_POST["ean"]) && !empty($_POST["jm"])) {
            // Je�li towar jeszcze nie zosta� dodany
            $newTowarData = array(
                "tw_nazwa" => trim($_POST["nazwa"]),
                "te_code" => trim($_POST["ean"]),
                "tw_jmid" => trim($_POST["jm"]),
            );

            if (isset($_POST["towar_nowy"]) && ($_POST["towar_nowy"] == 0)) {
                // Je�li towar zosta� wybrany z listy i nie jest nowy
                $this->insert_into_te_ean();
            } elseif (!$this->get_ean_exists()) {
                // Je�li towar o danym kodzie EAN jeszcze nie istnieje
                $this->insert_into_tw__towar($newTowarData);
            } else {
                $displayPage = false;
                $_SESSION["towar_added"] = 1;
            }

            if ($_POST["ean"] != InwentaryzacjaManager::NO_EAN_CODE) {
                $this->set_ean_exists(true);
            }
        } elseif ($areSet || isset($_SESSION["towar_added"])) {
            InwentaryzacjaManager::head_to();
        }

        return $displayPage;
    }

    /**
     * Metoda wstawia przechowywany kod EAN do bazy danych
     *
     * @return boolean Wartosc logiczna wskazujaca na to, czy przechowywany w obiekcie kod EAN zostal wstawiony
     */
    public function insert_into_te_ean(): bool
    {
        if ($this->is_without_ean()) {
            return false;
        }

        $query = "
            IF NOT EXISTS(SELECT * FROM te_ean WHERE te_code= ? AND te_twid= ?)
            BEGIN
                INSERT INTO te_ean (te_code, te_twid) values (?, ?);
            END";

        $params = array($this->get_ean(), $this->get_twid(), $this->get_ean(), $this->get_twid());

        $this->db->query($query, $params);
        $this->set_ean_exists(true);

        return true;
    }

    /**
     * Metoda wstawia towar do bazy danych
     *
     * @param array $newTowarData Dane wstawianego towaru
     * - tw_nazwa: nazwa,
     * - te_code: kod EAN,
     * - tw_jmid: id jednostki miary.
     * @return void
     */
    public function insert_into_tw__towar(array $newTowarData): void
    {
        if (!$this->get_ean_exists() && !$this->get_towar_exists()) {
            $tw_nazwa = $newTowarData["tw_nazwa"];
            $te_code = $newTowarData["te_code"];
            $tw_jmid = $newTowarData["tw_jmid"];

            if (empty($tw_nazwa)) {
                throw new \LengthException("Nazwa nie moze byc pusta");
            }

            $tw_nazwa = str_replace('"', "", $tw_nazwa);
            $tw_nazwa = str_replace("'", "", $tw_nazwa);

            $this->set_towar_nazwa($tw_nazwa);
            $this->set_towar_jmid($tw_jmid);

            $query = "INSERT
                INTO tw__towar (tw_nazwa, tw_jmid)
                OUTPUT Inserted.tw_id
                VALUES (?, ?)";

            $params = array($tw_nazwa, $tw_jmid);
            $thatTwid = $this->db->fetch_first($query, $params);

            $this->handle_ean();
            $this->handle_twid($thatTwid);

            if ($te_code != self::NO_EAN_CODE) {
                $this->insert_into_te_ean();
            } else {
                $this->set_ean_exists(false);
            }

            $this->set_towar_exists(true);
            $_SESSION["towar_added"] = 1;
        }
    }

    /**
     * Metoda wyswietla klawiature numeryczna dla wybranego pola typu input
     *
     * @param string $element ID pola input
     * @return void
     */
    public function display_numpad(string $element): void
    {
        $isDisabled = "";

        echo "
        <div class='component-bottom'>
            <hr/>
            <table class='table-numpad'>
                <tr>
                    <td class='numpad_td' style='width: 25%'><input type='button' class='numpad_key' value='1' onclick='numpad_insert(\"1\", \"{$element}\")'></td>
                    <td class='numpad_td' style='width: 25%'><input type='button' class='numpad_key' value='2' onclick='numpad_insert(\"2\", \"{$element}\")'></td>
                    <td class='numpad_td' style='width: 25%'><input type='button' class='numpad_key' value='3' onclick='numpad_insert(\"3\", \"{$element}\")'></td>
                    <td class='numpad_td' style='width: 25%'><input type='button' class='numpad_key' value='&larr;' onclick='numpad_backspace(\"{$element}\")'></td>
                </tr>
                <tr>
                    <td class='numpad_td'><input type='button' class='numpad_key' value='4' margin: 0px;' onclick='numpad_insert(\"4\", \"{$element}\")'></td>
                    <td class='numpad_td'><input type='button' class='numpad_key' value='5' onclick='numpad_insert(\"5\", \"{$element}\")'></td>
                    <td class='numpad_td'><input type='button' class='numpad_key' value='6' onclick='numpad_insert(\"6\", \"{$element}\")'></td>
                    <td class='numpad_td'><input type='button' class='numpad_key' value='C' onclick='numpad_clear(\"{$element}\")'></td>
                </tr>
                <tr>
                    <td class='numpad_td'><input type='button' class='numpad_key' value='7' onclick='numpad_insert(\"7\", \"{$element}\")'></td>
                    <td class='numpad_td'><input type='button' class='numpad_key' value='8' onclick='numpad_insert(\"8\", \"{$element}\")'></td>
                    <td class='numpad_td'><input type='button' class='numpad_key' value='9' onclick='numpad_insert(\"9\", \"{$element}\")'></td>
                    <td class='numpad_td'><input type='button' class='numpad_key' value='-' onclick='numpad_insert(\"-\", \"{$element}\")'></td>
                </tr>
                <tr>
                    <td class='numpad_td' colspan='3'><input type='button' class='numpad_key' value='0' onclick='numpad_insert(\"0\", \"{$element}\")'></td>
                    <td class='numpad_td'><input type='button' class='numpad_key' value='.' onclick='numpad_insert(\".\", \"{$element}\")' {$isDisabled}></td>
                </tr>
                <tr>
                    <td class='numpad_td'></td>
                    <td class='numpad_td' colspan='2'><button type='button' onclick='confirmSubmit()' class='numpad_key' style='margin-top: 8px;'><span class='standout-text bold'>Zapisz</span></button></td>
                    <td class='numpad_td'></td>
                </tr>
            </table>
            </div>

            <script>
                function numpad_insert(character, object) {
                    var inputValue = document.getElementById(object).value;
                    var commaIndex = inputValue.indexOf(\".\");
                    var minusIndex = inputValue.indexOf(\"-\");
                
                    if (((commaIndex == -1 || character !=\".\") && character !=\"-\") || (character == \"-\" && inputValue.length == 0)) {
                        document.getElementById(object).value = inputValue += character;
                    }
                }

                function numpad_backspace(object) {
                    var inputValue = document.getElementById(object).value;
                    document.getElementById(object).value = inputValue.slice(0, -1)
                }

                function numpad_clear(object) {
                    document.getElementById(object).value = '';
                }

                function hideNumpad() {
                    document.getElementById(\"numpad\").style.visibility = \"collapse\";
                }

                function showNumpad() {
                    document.getElementById(\"numpad\").style.visibility = \"visible\";
                }
            </script>";
    }
}