<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class TowarListaManager extends InwentaryzacjaManager
{
    /**
     * Metoda odpowiada za wyszukiwanie towarow w bazie danych na podstawie odebranego argumentu oraz
     * zwracanie ich w formie sformatowanego table row w HTML
     *
     * @param string $userInput Wyszukiwana nazwa lub kod EAN
     * @return string Lista towarow sformatowana w HTML jako table row
     */
    public function display_towary_for_table(string $userInput): string
    {
        $result = $this->get_towary_resource($userInput);

        $str = "";
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $nazwa = preg_replace('/[^A-Za-z0-9\.\,\- ]/', ' ', $row["tw_nazwa"]);

            $str .= "
                <tr>
                    <td>{$row["tw_nazwa"]}</td>
                    <td>{$row["te_code"]}</td>
                    <td>{$_COOKIE["sl_jm"][$row["tw_jmid"]]}</td>
                    <td>
                        <button class='btn-list' type='button' onclick=\"select('{$nazwa}', '{$row["te_code"]}', '{$row["tw_jmid"]}', '{$row["tw_id"]}')\" style='width: 100%; padding: 0.5em 1em; min-height: 24px; border: 1px solid var(--border-darker);'>
                            <i class='iconoir-check' style='display: inline-block; vertical-align: middle;'></i>
                        </button>
                    </td>
                </tr>";
        }

        return $str;
    }
}