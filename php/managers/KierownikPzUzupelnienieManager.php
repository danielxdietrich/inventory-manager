<?php

namespace Inwentaryzacja\Managers;

use Inwentaryzacja\InwentaryzacjaManager;

final class KierownikPzUzupelnienieManager extends InwentaryzacjaManager
{
    /**
     * Metoda zmienia nazwe towaru w bazie danych
     *
     * @param integer $twid ID towaru
     * @param string $nazwa Nowa nazwa towaru
     * @return void
     */
    public function update_nazwa_in_tw__towar(int $twid, string $nazwa): void
    {
        $nazwa = iconv("UTF-8", "Windows-1250", $nazwa);
        $query = "UPDATE tw__towar
            SET tw_nazwa = ?
            WHERE tw_id = ?";
        $params = array($nazwa, $twid);

        $this->db->query($query, $params);
    }

    /**
     * Metoda zmienia jednostke miary towaru w bazie danych
     *
     * @param integer $twid ID towaru
     * @param integer $jmid Nowe ID jednostki miary
     * @return void
     */
    public function update_jmid_in_tw__towar(int $twid, int $jmid): void
    {
        $query = "UPDATE tw__towar
            SET tw_jmid = ?
            WHERE tw_id = ?";
        $params = array($jmid, $twid);

        $this->db->query($query, $params);
    }

    /**
     * Metoda zmienia cene towaru w dokumencie pz w bazie danych
     *
     * @param integer $twid ID towaru
     * @param float $cena Nowa cena towaru
     * @return void
     */
    public function update_cena_in_pz_pozycja(int $ppid, float $cena): void
    {
        $dataDodania = date(self::DATE_FORMAT);
        $query = "UPDATE pz_pozycja
            SET pp_cena = ?, pp_data_dodania = ?
            WHERE pp_id = ?";
        $params = array($cena, $dataDodania, $ppid);

        $this->db->query($query, $params);
    }

    /**
     * Metoda zapisuje tymczasowe ceny do bazy danych
     * 
     * @param int $iwid ID inwentaryzacji
     * @param int $twid ID towaru
     * @param float $cena Cena towaru
     * @return void
     */
    public function update_or_insert_to_ic_ceny(int $iwid, int $twid, float $cena): void
    {
        $query = "
            IF EXISTS (SELECT * FROM ic_ceny WHERE ic_iwid = ? AND ic_twid = ?)
            BEGIN
                UPDATE ic_ceny SET ic_cena = ? WHERE ic_iwid = ? AND ic_twid = ?
            END
            ELSE
            BEGIN
                INSERT INTO ic_ceny (ic_iwid, ic_twid, ic_cena) VALUES (?, ?, ?)
            END";

        $params = array($iwid, $twid, $cena, $iwid, $twid, $iwid, $twid, $cena);

        $this->db->query($query, $params);
    }
}