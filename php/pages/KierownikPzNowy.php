<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageKierownik;
use Inwentaryzacja\Managers\KierownikPzNowyManager;
use Market;

require_once "parent/AbstractPageKierownik.php";
require_once "../managers/KierownikPzNowyManager.php";
require_once "../Market.php";

final class KierownikPzNowy extends AbstractPageKierownik
{
    protected string $title = "Wprowadzanie dokument�w PZ";
    protected string $icon = "iconoir-page-edit";
    protected int $kierownikMenuPosition = 3;

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        $this->display_page();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony policzonego towaru
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar(true, true);

        echo "
        {$this->inw->display_head()}

        <body {$this->widerBodyStyle}>
        {$titleBar}
        <hr/><br/>
        
        <div style='display: inline-block; width: 100%;'>
            <span display='vertical-align: middle;'>
                <span style='align-items: center; display: flex; justify-content: space-between; margin-top: 16px; margin-bottom: 8px;'>
                    <h3 style='margin: auto 0;'>Szczeg�y dokumentu</h3>

                    <button style='margin: 0;' onclick='wczytajDokumentMain();'>
                        <span class='standout-text bold'>Wczytaj dokument z Marketu</span>
                    </button>
                </span>
            </span>
        </div>

        <form method='post' id='myForm' onkeydown='return event.key != \"Enter\";' onsubmit='newCompleteDokumentPz(); return false;'>
        <table>
        <thead class='thead'>
        <tr>
            <th width='20%'>Numer</th>
            <th width='20%'>Data</th>
            <th width='20%'>Dzia�</th>
            <th width='15%'>Kontrahent</th>
            <th width='35%'></th>
        </tr>
        </thead>
        
        <tbody>
            <tr>
                <td>
                    <input placeholder='Numer dokumentu' type='text' id='input-numer' name='input-numer' style='width: 100%; padding-left: 0; padding-right: 0; margin-right: 0; text-align: center;' required>
                </td>

                <td>
                    <input type='date' id='input-data' name='input-data' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center;' value='' required>
                </td>

                <td>
                    <select name='select-dzial' id='select-dzial' style='width: 100%' required>
                        {$this->inw->display_mj_for_select()}
                    </select>
                </td>

                <td>
                    <input type='text' id='input-kontrahent' name='input-kontrahent' value='' placeholder='Nazwa / NIP' oninput='getKontrahenci();' style='width: 100%; padding-left: 0; padding-right: 0; margin-right: 0; text-align: center;'>
                </td>

                <td>
                    <select name='select-kontrahent' id='select-kontrahent' style='width: 100%' required>
                    </select>
                </td>
            </tr>
        </tbody>
        </table>

        <div id='div-restrict' class='disabled-zone'>
            <details style='margin: 8px 6px 16px;'>
            <summary style='text-align: middle; text-decoration: none; padding-left: 16px;'><h4 style='display: inline; margin-left: 6px; margin-top: 0;'>Przypisz nowy towar do kontrahenta</h4></summary>
            <table>

            <tbody>
                <tr>
                    <td>
                        <input type='text' id='input-ean-nazwa' name='input-ean-nazwa' placeholder='Nazwa / kod EAN' oninput='displayTowaryForSelect();' style='width: 100%; padding-left: 0; padding-right: 0; margin-right: 0; text-align: center;'>
                    </td>

                    <td>
                        <select name='select-towar' id='select-towar' style='width: 100%'>
                        </select>
                    </td>

                    <td>
                        <button type='button' style='width: 100%' onclick='assignTowarToKontrahent();'>Wykonaj przypisanie</button>
                    </td>
                </tr>
            </tbody>
            </table>
            </details>

            <h3>Lista towar�w</h3>
            <div class='table-container' style='min-height: 25vh; max-height: 50vh;'>
                <table id='table-towary' width='100%' class='table-autofilter table-autosort'>
                    <thead class='thead'>
                        <tr>
                            <th width='40%' class='table-sortable:default table-sortable hover-underline-animation'>Nazwa</th>
                            <th width='15%' class='table-sortable:default table-sortable hover-underline-animation'>EAN</th>
                            <th width='10%' class='table-sortable:numeric table-sortable hover-underline-animation'>Ilo��</th>
                            <th width='5%'></th>
                            <th width='15%' class='table-sortable:numeric table-sortable hover-underline-animation'>Cena</th>
                            <th width='15%' class='table-sortable:numeric table-sortable hover-underline-animation'>Warto��</th>
                        </tr>
                    </thead>

                    <tbody id='tbody-towary-kontrahent'></tbody>

                    <tfoot class='tfoot'>
                        <tr>
                            <td id='td-suma-text' colspan='5' style='text-align: right;'></td>
                            <td id='td-suma' style='text-align: center;'></td>
                        </tr>
                </table>
            </div>
            <br/>

            <button type='submit' style='float: right; margin-bottom: 5%;'><span class='bold'>Wprowad� dokument</span></button>
        </div>
        </form> 

        <script src='../../js/table.js?v={$this->inw->get_resources_version()}'></script>
        {$this->get_page_js()}
        </body>
        
        </html>";

        $this->cleanup();
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new KierownikPzNowyManager($this->title, $this->icon);
    }
}

new KierownikPzNowy(isset($abortDisplaying));