<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageUzytkownik;
use Inwentaryzacja\Managers\TowarIstniejacyManager;

require_once "parent/AbstractPageUzytkownik.php";
require_once "../managers/TowarIstniejacyManager.php";

final class TowarIstniejacy extends AbstractPageUzytkownik
{
    protected string $title = "Inwentaryzacja towaru";
    protected string $icon = "iconoir-scan-barcode";

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        if ($this->inw->handle_new_towar()) {
            $this->display_page();
            $this->inw->display_numpad('input-ilosc');
        } else {
            echo "
            {$this->inw->display_head()}

            <caption>
                <h2><i class='iconoir-info-empty' style='display: inline-block; vertical-align: middle;'></i> Towar o podanym kodzie EAN ju� istnieje</h2>
            </caption>";
        }
    }

    /**
     * Metoda odpowiada za wyswietlanie strony istniejacego towaru.
     * Formularz przekierowuje do TowarDodany.php i przesyla:
     * - input-ilosc: ilosc towaru do inwentaryzacji,
     * - twid: id towaru inwentaryzowanego.
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar();
        $lastInw = $this->inw->display_last_inwentaryzacja();

        echo "
        {$this->inw->display_head()}

        <body>
        {$lastInw}
        {$titleBar}
        <hr/>
        
        <table>
            <tbody>
                <tr>
                    <td width='20%'><strong>Nazwa</strong></td>
                    <td width='80%'>{$this->inw->get_towar_nazwa()}</td>
                </tr>
                {$this->inw->try_print_ean_tr()}
                <tr>
                    <td><strong>Miejsce</strong></td>
                    <td>{$this->inw->get_towar_mj()}</td>
                </tr>
            </tbody>
        </table>
        <hr/>

        <form action='TowarDodany.php' method='post' id='myForm' onkeydown='return event.key != \"Enter\";' onsubmit='return false;'>
            <label for='input-ilosc' style='margin-right: 5px'>Ilo��:</label>
            <input style='display: inline; vertical-align: middle; width: 25%;' type='text' id='input-ilosc' readonly='readonly' name='input-ilosc' min='{$this->inw->get_step()}' step='{$this->inw->get_step()}' autofocus required>
            <p style='display: inline;'>{$this->inw->get_towar_jm(true)}</p>
            <input name='twid' id='twid' value='{$this->inw->get_twid()}' type='hidden' readonly='readonly' required>
        </form>

        {$this->get_page_js()}
        </body>
        
        </html>";

        $this->cleanup();
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new TowarIstniejacyManager($this->title, $this->icon);
    }
}

new TowarIstniejacy(isset($abortDisplaying));