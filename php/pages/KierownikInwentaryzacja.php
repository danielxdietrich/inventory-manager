<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Managers\KierownikInwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageKierownik;

require_once "parent/AbstractPageKierownik.php";
require_once "../managers/KierownikInwentaryzacjaManager.php";

final class KierownikInwentaryzacja extends AbstractPageKierownik
{
    protected string $title = "Zarz�dzanie zamkni�tymi inwentaryzacjami";
    protected string $icon = "iconoir-multiple-pages";
    protected int $kierownikMenuPosition = 2;

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        $this->display_page();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony zarzadzania inwentaryzacjami
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar(true, true);
        $dateFrom = date('Y-m-t', strtotime('last day of previous month'));
        $dateTo = date('Y-m-d');

        echo "
        {$this->inw->display_head()}

        <body {$this->widerBodyStyle}>
        {$titleBar}
        <hr/><br/>

        <h3>Filtrowanie inwentaryzacji</h3>
        <form method='post' id='myForm' onkeydown='return event.key != \"Enter\";' onsubmit='displayInwentaryzacje(); return false;'>
        <table>
        <thead class='thead'>
        <tr>
            <th width='15%'>Miejsce</th>
            <th width='15%'>Data od</th>
            <th width='15%'>Data do</th>
            <th width='35%'>Numer inwentaryzacji</th>
            <th width='20%'></th>
        </tr>
        </thead>
        
        <tbody>
            <tr>
                <td>
                    <select onchange='displayInwentaryzacje();' name='select-dzial1' id='select-dzial1' style='width: 100%' required>
                        {$this->inw->display_mj_for_select()}
                    </select>
                </td>
                
                <td>
                    <input onchange='displayInwentaryzacje();' type='date' id='input-data-od1' name='input-data-od1' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center;' value='{$dateFrom}' required>
                </td>
                
                <td>
                    <input onchange='displayInwentaryzacje();' type='date' id='input-data-do1' name='input-data-do1' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center;' value='{$dateTo}' required>
                </td>
                
                <td>
                    <input type='text' id='input-inwentaryzacja-nr' name='input-inwentaryzacja-nr' style='width: 100%; padding-left: 0; padding-right: 0; margin-right: 0; text-align: center;' >
                </td>

                <td>
                    <button type='submit' style='width: 100%'><span class='bold'>Wy�wietl</span></button>
                </td>
            </tr>
        </tbody>
        </table>
        </form>

        <div id='div-restrict' class='disabled-zone'>
            <h3>Lista inwentaryzacji<span id='span-lista-inwentaryzacji'></span></h3>
            <div class='table-container' style='min-height: 25vh; max-height: 50vh;'>
                <table class='table-autofilter table-autosort' width='100%'>
                    <thead class='thead'>
                        <tr>
                            <th width='50%' class='table-sortable:default table-sortable hover-underline-animation'>Numer</th>
                            <th width='50%' class='table-sortable:numeric table-sortable hover-underline-animation'>Data</th>
                        </tr>
                    </thead>

                    <tbody id='tbody-inwentaryzacje'></tbody>
                </table>
            </div>
            <br/>

            <div id='div-restrict-2' class='disabled-zone'>

            <span style='align-items: center; display: flex; justify-content: space-between;'>
            <h3 style='margin: auto 0;'>Szczeg�y inwentaryzacji <span id='span-inwentaryzacja-title'></span></h3>
                        
            <form method='post' action='KierownikPzUzupelnienie.php' target='_blank'>
                <input id='input-iwid' type='hidden' name='iwid'/>
                <input id='input-mjid' type='hidden' name='mjid'/>
                <button id='button-drukowanie' style='margin: 0;' type='submit' disabled><span class='bold'>Przygotuj dokument</span></button>
            </form>
            </span>
            <input type='checkbox' id='input-sumy-czesciowe' name='input-sumy-czesciowe' onchange='subtotalClick();' checked><label for='input-sumy-czesciowe'> Sumy cz�ciowe</label>

            <div class='table-container' style='max-height: 35vh;'>
                <table class='table-autofilter table-autosort' width='100%'>
                    <thead class='thead'>
                        <tr>
                            <th width='45%' class='table-sortable:default table-sortable hover-underline-animation'>Nazwa</th>
                            <th width='25%' class='table-sortable:default table-sortable hover-underline-animation'>EAN</th>
                            <th width='15%' class='table-sortable:numeric table-sortable hover-underline-animation'>Ilo��</th>
                            <th width='15%' class='table-sortable:default table-sortable hover-underline-animation'>Osoba</th>
                        </tr>
                    </thead>

                    <tbody id='tbody-policzone'></tbody>
                </table>
            </div>
            
            <br/>
        </div>
        </div>
        
        <script src='../../js/table.js?v={$this->inw->get_resources_version()}'></script>
        {$this->get_page_js()}
        </body>

        </html>";

        $this->cleanup();
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new KierownikInwentaryzacjaManager($this->title, $this->icon);
    }
}

new KierownikInwentaryzacja(isset($abortDisplaying));