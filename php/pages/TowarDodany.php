<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageUzytkownik;
use Inwentaryzacja\Managers\TowarDodanyManager;

require_once "parent/AbstractPageUzytkownik.php";
require_once "../managers/TowarDodanyManager.php";

final class TowarDodany extends AbstractPageUzytkownik
{
    protected string $title = "Inwentaryzacja wykonana";
    protected string $icon = "iconoir-check-circled-outline";

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        $this->inw->handle_new_inwentaryzacja();
        $this->display_page();

        InwentaryzacjaManager::clear_session();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony dodanego towaru.
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar();

        echo "
        {$this->inw->display_head()}

        <body>
        {$titleBar}
        <hr/>
        
        <table>
            <tbody>
                <tr>
                    <th><strong>Nazwa</strong></th>
                    <td>{$this->inw->get_towar_nazwa()}</td>
                </tr>
                {$this->inw->try_print_ean_tr()}
                <tr>
                    <th><strong>Ilo��</strong></th>
                    <td>{$this->inw->get_inwentaryzacja_quantity()} {$this->inw->get_towar_jm()}</td>
                </tr>
                <tr>
                    <th><strong>Miejsce</strong></th>
                    <td>{$this->inw->get_towar_mj()}</td>
                </tr>
                <tr>
                    <th><strong>Data dodania</strong></th>
                    <td>{$this->inw->get_inwentaryzacja_date()}</td>
                </tr>
            </tbody>
        </table>
        
        {$this->get_page_js()}
        </body>
        
        </html>";

        $this->cleanup();
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new TowarDodanyManager($this->title, $this->icon);
    }
}

new TowarDodany(isset($abortDisplaying));