<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageUzytkownik;
use Inwentaryzacja\Managers\TowarPoliczonyManager;

require_once "parent/AbstractPageUzytkownik.php";
require_once "../managers/TowarPoliczonyManager.php";

final class TowarPoliczony extends AbstractPageUzytkownik
{
    protected string $title = "Policzony towar";
    protected string $icon = "iconoir-task-list";
    protected int $mainMenuPosition = 2;

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        $this->display_page();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony zinwentaryzowanego towaru
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar();

        echo "
        {$this->inw->display_head()}

        <body>
        {$titleBar}
        <hr/>

        <input type='text' id='input-search' onkeyup='searchTable()' placeholder='Nazwa towaru...' style='margin-bottom: 24px; margin-left: auto; margin-right: auto; width: 90%; text-align: center;'>
        <input type='checkbox' id='input-tylko-uzytkownika' name='input-tylko-uzytkownika' onchange='displayTowarPoliczony();'><label for='input-tylko-uzytkownika'> Tylko policzone przeze mnie</label><br/>
        <input type='checkbox' id='input-sumy-czesciowe' name='input-sumy-czesciowe' onchange='displayTowarPoliczony();'><label for='input-sumy-czesciowe'> Sumy cz�ciowe</label>
        <div class='table-container' style='height: auto; max-height: 75vh;'>
            <table id='table-policzone' class='table-autofilter table-autosort' width='100%' style='table-layout: auto;'>
            <thead class='thead'>
                <tr>
                    <th width='40%' class='table-sortable:default table-sortable hover-underline-animation'>Nazwa</th>
                    <th width='20%' class='table-sortable:numeric table-sortable hover-underline-animation'>Ilo��</th>
                    <th width='30%' class='table-sortable:numeric table-sortable hover-underline-animation'>Data</th>
                    <th width='10%'></th>
                </tr>
            </thead>
                
            <tbody id='tbody-element'></tbody>
            </table>
        </div>
                
        <script src='../../js/table.js?v={$this->inw->get_resources_version()}'></script>
        {$this->get_page_js()}
        <script>displayTowarPoliczony();</script>
        </body>
        
        </html>";

        $this->cleanup();
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new TowarPoliczonyManager($this->title, $this->icon);
    }
}

new TowarPoliczony(isset($abortDisplaying));