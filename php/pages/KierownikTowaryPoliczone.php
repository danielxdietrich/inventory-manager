<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageKierownik;
use Inwentaryzacja\Managers\KierownikTowaryPoliczoneManager;

require_once "parent/AbstractPageKierownik.php";
require_once "../managers/KierownikTowaryPoliczoneManager.php";

final class KierownikTowaryPoliczone extends AbstractPageKierownik
{
    protected string $title = "Zarz�dzanie policzonym towarem";
    protected string $icon = "iconoir-page";
    protected int $kierownikMenuPosition = 1;

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        $this->display_page();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony policzonego towaru
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar(true, true);
        $dateFrom = date('Y-m-01');
        $dateTo = date('Y-m-d');

        echo "
        {$this->inw->display_head()}

        <body {$this->widerBodyStyle}>
        {$titleBar}
        <hr/><br/>

        <h3>Filtrowanie policzonych towar�w</h3>
        <form method='post' id='myForm' onkeydown='return event.key != \"Enter\";' onsubmit='return false;'>
            <table>
            <thead>
                <tr>
                    <th><label for='select-dzial1'>Dzia�</label></th>
                    <th><input id='input-osoba' type='checkbox' name='input-osoba' style='vertical-align: middle;' onclick='handleCheckboxOsoba()'><label for='input-osoba'>Osoba</label></th>
                    <th><label for='input-data-od1'>Data od</label></th>
                    <th><label for='input-data-do1'>Data do</label></th>
                    <th><label for='select-stan'>Stan</label></th>
                    <th></th>
                </tr>
            </thead>
            
            <tbody>
                <tr>
                    <td>
                        <select name='select-dzial1' id='select-dzial1' style='width: 100%' onchange='selectDzial1Click();' required>
                            {$this->inw->display_mj_for_select()}
                        </select>
                    </td>

                    <td>
                        <select onchange='displayTowarPoliczony()' name='select-osoba' id='select-osoba' style='width: 100%' disabled></select>
                    </td>

                    <td>
                        <input onchange='displayTowarPoliczony()' type='datetime-local' id='input-data-od1' name='input-data-od1' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center;' value='{$dateFrom}T00:00' required>
                    </td>

                    <td>
                        <input onchange='displayTowarPoliczony()' type='datetime-local' id='input-data-do1' name='input-data-do1' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center;' value='{$dateTo}T23:59' required>
                    </td>

                    <td>
                        <select onchange='displayTowarPoliczony()' name='select-stan' id='select-stan' style='width: 100%' required>
                            <option value='1'>Wszystkie</option>
                            <option value='2' selected='selected'>Otwarte</option>
                            <option value='3'>Zamkni�te</option>
                        </select>
                    </td>

                    <td>
                        <button type='button' style='width: 100%' onclick='displayTowarPoliczony()'><span class='bold'>Wy�wietl</span></button>
                    </td>
                </tr>
            </tbody>
            </table>
        </form>
        
        <div id='div-restrict' class='disabled-zone'>
            <h3 id='h3-lista'>Lista policzonych towar�w</h3>
            <div class='table-container' style='min-height: 25vh; max-height: 50vh;'>
                <table width='100%' class='table-autofilter table-autosort'>
                    <thead class='thead'>
                        <tr>
                            <th width='35%' class='table-sortable:default table-sortable hover-underline-animation'>Nazwa</th>
                            <th width='15%' class='table-sortable:default table-sortable hover-underline-animation'>EAN</th>
                            <th width='10%' class='table-sortable:numeric table-sortable hover-underline-animation'>Ilo��</th>
                            <th width='15%' class='table-sortable:default table-sortable hover-underline-animation'>Osoba</th>
                            <th width='10%' class='table-sortable:default table-sortable hover-underline-animation'>Stan</th>
                        </tr>
                    </thead>

                    <tbody id='tbody-policzone'></tbody>
                </table>
            </div>

            <h3>Zamykanie inwentaryzacji</h3>
            <form action='#' onsubmit='assignTowaryPoliczoneToInwentaryzacja(); return false;'>
            <table>
                <thead>
                    <tr>
                        <th><label for='select-dzial2'>Dzia�</label></th>
                        <th><label for='input-data-od2'>Data skanowania od</label></th>
                        <th><label for='input-data-do2'>Data skanowania do</label></th>
                        <th><label for='input-inwentaryzacja-nr'>Numer inwentaryzacji</label></th>
                        <th><label for='input-inwentaryzacja-nr'>Data inwentaryzacji</label></th>
                        <th></th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr>
                        <td>
                            <select name='select-dzial2' id='select-dzial2' style='width: 100%' disabled>
                                {$this->inw->display_mj_for_select()}
                            </select>
                        </td>

                        <td class='disabled-zone'>
                            <input type='datetime-local' id='input-data-od2' name='input-data-od2' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center;' required>
                        </td>

                        <td class='disabled-zone'>
                            <input type='datetime-local' id='input-data-do2' name='input-data-do2' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center;' required>
                        </td>

                        <td>
                            <input type='text' id='input-inwentaryzacja-nr' name='input-inwentaryzacja-nr' style='width: 100%; padding-left: 0; padding-right: 0; margin-right: 0; text-align: center;' required>
                        </td>

                        <td>
                            <input type='date' id='input-inwentaryzacja-data' name='input-data-do2' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center;'  required>
                        </td>

                        <td>
                            <button type='submit' style='width: 100%'><span class='bold'>Zamknij</span></button>
                        </td>
                    </tr>
                </tbody>
            </table>
            </form>
        </div>

        <script src='../../js/table.js?v={$this->inw->get_resources_version()}'></script>     
        {$this->get_page_js()}
        </body>
        
        </html>";

        $this->cleanup();
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new KierownikTowaryPoliczoneManager($this->title, $this->icon);
    }
}

new KierownikTowaryPoliczone(isset($abortDisplaying));