<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Managers\KierownikPanelManager;
use Inwentaryzacja\Pages\Parent\AbstractPageKierownik;

require_once "parent/AbstractPageKierownik.php";
require_once "../managers/KierownikPanelManager.php";

final class KierownikPanel extends AbstractPageKierownik
{
    protected string $title = "Panel kierownika";
    protected string $icon = "iconoir-user-circle-alt";

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        $this->display_page();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony panelu kierownika
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar(true);

        echo "
        {$this->inw->display_head()}
        
        <body {$this->widerBodyStyle}>
        {$titleBar}
        <hr/><br/>
        <div class='container'>
            {$this->inw->get_kierownik_panel_menu()}
        </div>
        
        {$this->inw->display_statusbar()}
        {$this->get_page_js()}
        </body>

        </html>";

        $this->cleanup();
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new KierownikPanelManager($this->title, $this->icon);
    }
}

new KierownikPanel(isset($abortDisplaying));