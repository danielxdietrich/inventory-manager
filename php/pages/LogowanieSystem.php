<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPage;
use Inwentaryzacja\Managers\LogowanieSystemManager;

require_once "parent/AbstractPage.php";
require_once "../managers/LogowanieSystemManager.php";

final class LogowanieSystem extends AbstractPage
{

    public function __construct()
    {
        parent::__construct();

        $login = trim($_POST['login']);
        $password = trim($_POST['password']);

        $this->try_login($login, $password);
    }

    protected function display_page(): void
    {
    }

    /**
     * Metoda odpowiada za proces logowania i odpowiednie przekierowania zalezne od podjetej przez uzytkownika akcji
     *
     * @param string $login Login
     * @param string $password Haslo
     * @return void
     */
    private function try_login(string $login, string $password): void
    {
        $expirationDate = time() + (86400 * 30);

        if ($this->inw->is_logged_in()) {
            InwentaryzacjaManager::head_to();
        }

        if ($this->inw->login_exists($login)) {
            if ($this->inw->has_password($login)) {
                $isCorrectPassword = password_verify($password, $this->inw->get_password($login));

                if ($isCorrectPassword) {
                    $this->inw->configure_cookies($login, $expirationDate);
                    InwentaryzacjaManager::head_to();
                } else {
                    InwentaryzacjaManager::head_to("Logowanie.php?error=password");
                }
            } else {
                $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

                $this->inw->set_password($login, $hashedPassword);
                $this->inw->configure_cookies($login, $expirationDate);

                InwentaryzacjaManager::head_to();
            }
        } else {
            // Incorrect login
            InwentaryzacjaManager::head_to("Logowanie.php?error=login");
        }
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new LogowanieSystemManager($this->title, $this->icon);
    }
}

new LogowanieSystem();