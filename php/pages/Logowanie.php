<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPage;

require_once "parent/AbstractPage.php";

final class Logowanie extends AbstractPage
{
    protected string $title = "Panel logowania";
    protected string $icon = "iconoir-user-circle-alt";

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        if ($this->inw->is_logged_in()) {
            InwentaryzacjaManager::head_to();
        }

        $this->display_page();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony logowania.
     * Formularz przekierowuje do LogowanieSystem.php i przesyla:
     * - login: login uzytkownika,
     * - password: haslo uzytkownika.
     *
     * @return void
     */
    protected function display_page(): void
    {
        $errorDisplay = isset($_GET['error']) ? '' : "style='display: none'";
        $errorMessage = $this->get_error_message();
        $titleBar = $this->inw->display_title_bar(false);

        echo "
        {$this->inw->display_head()}

        <body>
        <form action='LogowanieSystem.php' method='post'>
            {$titleBar}
            <hr/>
            <div class='error-msg' {$errorDisplay}>
                <i class='iconoir-warning-circled-outline' style='display: inline-block; vertical-align: middle;'></i>
                {$errorMessage}
            </div>
            <label for='login'>Login:</label>
            <input type='text' id='login' name='login' placeholder='Login' required>
            <label for='password'>Has這:</label>
            <input type='password' id='password' name='password' placeholder='Has這' required>
            <br/>
            <hr/>
            <button type='submit'><span class='bold'>Zaloguj</span></button>
        </form>
        </body>
        
        </html>";

        $this->cleanup();
    }

    /**
     * Metoda wyswietla odpowiednie bledy, jezeli zostaly odebrane za pomoca GET
     *
     * @return string
     */
    private function get_error_message(): string
    {
        $errorMessage = "";
        if (isset($_GET['error'])) {
            if ($_GET['error'] == "login") {
                $errorMessage = "Wprowadzony login jest nieprawid這wy.";
            } elseif ($_GET['error'] == "password") {
                $errorMessage = "Wprowadzone has這 jest nieprawid這we.";
            }
        }
        return $errorMessage;
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new InwentaryzacjaManager($this->title, $this->icon);
    }
}

new Logowanie(isset($abortDisplaying));