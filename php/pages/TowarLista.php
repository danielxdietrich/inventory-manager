<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageUzytkownik;
use Inwentaryzacja\Managers\TowarListaManager;

require_once "parent/AbstractPageUzytkownik.php";
require_once "../managers/TowarListaManager.php";

final class TowarLista extends AbstractPageUzytkownik
{
    protected string $title = "Wyszukiwarka towaru";
    protected string $icon = "iconoir-search";
    protected int $mainMenuPosition = 1;

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        InwentaryzacjaManager::clear_session();

        $this->display_page();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony listy towaru.
     * Formularz przekierowuje do TowarIstniejacy.php i przesyla:
     * - nazwa: nazwa towaru,
     * - ean: kod EAN towaru,
     * - jm: jednostka miary towaru.
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar();
        $lastInw = $this->inw->display_last_inwentaryzacja();

        echo "
        {$this->inw->display_head()}

        <body>
        <div style='max-height: 35%;'>
            {$lastInw}
            {$titleBar}
            <hr/>
            
            <form onsubmit='return false'>
                <input type='text' minlength='3' name='input-ean-nazwa' id='input-ean-nazwa' placeholder='Nazwa lub kod EAN...' autofocus>
                <br/>
                <button type='submit' onclick='displayTowaryForTable();' style='float: left;'><span class='bold'>Wyszukaj</span></button>
            </form>
            <br/>
            <br/>
            <br/>
        </div>

        <div class='table-container' style='height: auto; max-height: 65%;'>
            <table width='100%' class='table-autofilter table-autosort'>
                <thead class='thead'>
                    <tr>
                        <th width='25%' class='table-sortable:default table-sortable hover-underline-animation'>Nazwa</th>
                        <th width='40%' class='table-sortable:numeric table-sortable hover-underline-animation'>EAN</th>
                        <th width='15%' class='table-sortable:default table-sortable hover-underline-animation'>Jm</th>
                        <th width='20%'></th>
                    </tr>
                </thead>

                <tbody id='tbody-element'></tbody>
            </table>
        </div>

        <form action='TowarIstniejacy.php' method='post' id='myForm' style='display: none'>
            <input name='nazwa' id='nazwa' type='text' required>
            <input name='ean' id='ean'>
            <input name='jm' id='jm' required>
            <input name='twid' id='twid' value='-1' required>
            <input name='towar_nowy' id='towar_nowy' value='0' required>
            <input name='towar_z_listy' value='1' required>
        </form>

        <script src='../../js/table.js?v={$this->inw->get_resources_version()}'></script>
        {$this->get_page_js()}
        </body>
        
        </html>";

        $this->cleanup();
    }
    /**
     */
    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new TowarListaManager($this->title, $this->icon);
    }
}

new TowarLista(isset($abortDisplaying));