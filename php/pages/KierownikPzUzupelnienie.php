<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageKierownik;
use Inwentaryzacja\Managers\KierownikPzUzupelnienieManager;
use Inwentaryzacja\Models\SpisInwentaryzacja;

require_once "parent/AbstractPageKierownik.php";
require_once "../managers/KierownikPzUzupelnienieManager.php";

final class KierownikPzUzupelnienie extends AbstractPageKierownik
{
    protected string $title = "Uzupe�nienie spisu inwentaryzacyjnego";
    protected string $icon = "iconoir-page-edit";

    private SpisInwentaryzacja $spis;

    private int $mjid;
    private int $iwid;

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        if (empty($_POST['iwid']) || empty($_POST['mjid'])) {
            $message = "";
            if (empty($_POST['iwid'])) {
                $message .= "Nie odebrano ID inwentaryzacji. ";
            }
            if (empty($_POST['mjid'])) {
                $message .= "Nie odebrano ID miejsca. ";
            }

            echo '<strong>Co� posz�o nie tak, spr�buj ponownie.</strong> Karta zostanie automatycznie zamkni�ta za 120s. Naci�nij spacj� aby anulowa� zamkni�cie karty.<hr/>';
            echo '
            <script type="text/javascript">
                let shouldClose = true;

                setTimeout("close_tab();", 120000);

                document.body.onkeyup = function (e) {
                    if (e.key == " " || e.code == "Space" || e.keyCode == 32) {
                        shouldClose = false;
                    }
                }

                function close_tab() {
                    if (shouldClose) {
                        window.close();
                    }
                }
            </script>';

            throw new \LogicException($message);
        } elseif (!is_numeric($_POST['iwid'])) {
            throw new \InvalidArgumentException('$_POST["iwid"] nie jest typu liczbowego');
        } elseif (!is_numeric($_POST['mjid'])) {
            throw new \InvalidArgumentException('$_POST["mjid"] nie jest typu liczbowego');
        }

        $this->iwid = intval($_POST["iwid"]);
        $this->mjid = intval($_POST["mjid"]);
        $this->spis = new SpisInwentaryzacja($this->iwid, $this->mjid);

        $this->display_page();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony policzonego towaru
     *
     * @return void
     */
    protected function display_page(): void
    {
        $inwDane = $this->inw->get_iw__inwentaryzacja_dane($this->spis->get_iwid());

        $numer = $inwDane["iw_nr"];
        $miejsce = $_COOKIE["sl_mj"][$this->mjid];
        $this->inw->set_title("Uzupe�nienie spisu inwentaryzacyjnego &rarr; <span class='standout-text'>{$miejsce}, {$numer}</span>");

        $titleBar = $this->inw->display_title_bar(false, false);

        echo "
        {$this->inw->display_head()}

        <body {$this->widerBodyStyle}>
        {$titleBar}
        <hr/><br/>

        <input oninput='searchTable();' type='text' id='input-search' name='input-search' style='width: 50%; margin-left: auto; margin-right: auto; margin-bottom: 1rem; padding-left: 0; padding-right: 0; text-align: center;' placeholder='Wyszukaj po nazwie lub kodzie EAN...'>
        <span id='span-iwid' style='display: none;' hidden>{$this->spis->get_iwid()}</span>
        <form method='post' style='margin-bottom: 5%;' id='myForm' action='KierownikPzDrukowanie.php?druk_iwid={$this->spis->get_iwid()}' onkeydown='return event.key != \"Enter\";'>
        <div class='table-container' style='min-height: 25vh; max-height: 75vh;'>
            <table id='table-towary' class='table-autofilter table-autosort'>
                <thead class='thead'>
                <tr>
                    <th width='5%' class='table-sortable:numeric table-sortable hover-underline-animation'>Poz.</th>
                    <th width='44%' class='table-sortable:default table-sortable hover-underline-animation'>Towar</th>
                    <th width='16%' class='table-sortable:default table-sortable hover-underline-animation'>EAN</th>
                    <th width='5%' class='table-sortable:default table-sortable hover-underline-animation'>Jm</th>
                    <th width='10%' class='table-sortable:numeric table-sortable hover-underline-animation'>Ilo��</th>
                    <th width='10%' class='table-sortable:numeric table-sortable hover-underline-animation'>Cena jedn.</th>
                    <th width='10%' class='table-sortable:numeric table-sortable hover-underline-animation'>Warto��</th>
                </tr>
                </thead>
            
                <tbody>
                    {$this->spis->to_string(true)}
                </tbody>

                <tfoot class='tfoot'>
                    <tr>
                        <td colspan='6' style='text-align: right; font-weight: bold;'>Suma</td>
                        <td style='font-weight: bold;'><strong><span id='span-suma' style='float: right; padding-right: 1em;'></span></strong></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <br/>
        
        <button type='button' style='float: left;' onclick='exportTableToCSV(\"{$miejsce}_{$numer}\")'>Pobierz CSV</button>
        <button type='submit' style='float: right;'><span class='bold'>Wygeneruj dokument</span></button>
        </div>
        </form> 

        <script src='../../js/modern_context.js?v={$this->inw->get_resources_version()}'></script>
        <script src='../../js/table.js?v={$this->inw->get_resources_version()}'></script>
        {$this->get_page_js()}
        </body>
        
        </html>";

        $this->cleanup();
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new KierownikPzUzupelnienieManager($this->title, $this->icon);
    }
}

new KierownikPzUzupelnienie(isset($abortDisplaying));