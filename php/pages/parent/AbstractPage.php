<?php

namespace Inwentaryzacja\Pages\Parent;

use Inwentaryzacja\InwentaryzacjaManager;

require_once "../InwentaryzacjaManager.php";

abstract class AbstractPage
{
    protected InwentaryzacjaManager $inw;

    protected string $title = "";
    protected string $icon = "";

    protected function __construct()
    {
        $this->inw = $this->instantiate_manager();
    }

    final protected function cleanup(): void
    {
        flush();
        gc_collect_cycles();
    }

    final protected function get_page_js(): string
    {
        $version = $this->inw->get_resources_version();
        $filename = $this->get_js_filename();
        $filePath = "../../js/pages/{$filename}";
        $script = "";

        if (!file_exists($filePath)) {
            $script = "";
        } else {
            $script = "<script src='{$filePath}?v={$version}'></script>";
        }

        return $script;
    }

    final private function get_js_filename(): string
    {
        $basename = basename($_SERVER['PHP_SELF']);
        $basename[0] = strtolower($basename[0]);
        return str_replace(".php", ".js", $basename);
    }

    final public function get_title(): string
    {
        return $this->title;
    }

    final public function get_icon(): string
    {
        return $this->icon;
    }

    /**
     * Metoda odpowiada za wyswietlanie strony
     *
     * @return void
     */
    abstract protected function display_page(): void;

    /**
     * Metoda odpowiada za utworzenie wlasciwego obiektu klasy InwentaryzacjaManager
     * 
     * @return void
     */
    abstract protected function instantiate_manager(): InwentaryzacjaManager;
}