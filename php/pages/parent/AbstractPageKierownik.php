<?php

namespace Inwentaryzacja\Pages\Parent;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageUzytkownik;

require_once "AbstractPageUzytkownik.php";

abstract class AbstractPageKierownik extends AbstractPageUzytkownik
{
    protected string $widerBodyStyle = "style='max-width: 1400px;'";
    // Number should be different than 0 in subclasses if it should be shown as option in the KierownikPanel
    protected int $kierownikMenuPosition = 0;

    protected function __construct()
    {
        parent::__construct();

        if (!$this->inw->is_manager()) {
            InwentaryzacjaManager::head_to();
        }
    }

    final public function get_show_in_kierownik_panel(): bool
    {
        return (0 !== $this->kierownikMenuPosition);
    }

    final public function get_kierownik_menu_position(): int
    {
        return $this->kierownikMenuPosition;
    }
}