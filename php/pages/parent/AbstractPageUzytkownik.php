<?php

namespace Inwentaryzacja\Pages\Parent;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPage;

require_once "AbstractPage.php";

abstract class AbstractPageUzytkownik extends AbstractPage
{
    // Number should be different than 0 in subclasses if it should be shown as option in the main menu
    protected int $mainMenuPosition = 0;

    protected function __construct()
    {
        parent::__construct();

        if (!$this->inw->is_logged_in()) {
            InwentaryzacjaManager::head_to("Logowanie.php");
        }
    }

    public function get_show_in_main_menu(): bool
    {
        return (0 !== $this->mainMenuPosition);
    }

    public function get_main_menu_position(): int
    {
        return $this->mainMenuPosition;
    }
}