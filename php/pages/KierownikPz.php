<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Managers\KierownikPzManager;
use Inwentaryzacja\Pages\Parent\AbstractPageKierownik;

require_once "parent/AbstractPageKierownik.php";
require_once "../managers/KierownikPzManager.php";

final class KierownikPz extends AbstractPageKierownik
{
    protected string $title = "Zarz�dzanie dokumentami PZ";
    protected string $icon = "iconoir-archive";
    protected int $kierownikMenuPosition = 4;


    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        $this->display_page();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony zarzadzania inwentaryzacjami
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar(true, true);
        $dateFrom = date('Y-m-t', strtotime('last day of previous month'));
        $dateTo = date('Y-m-d');

        echo "
        {$this->inw->display_head()}

        <body {$this->widerBodyStyle}>
        {$titleBar}
        <hr/><br/>

        <h3>Filtrowanie dokument�w</h3>
        <form method='post' id='myForm' onkeydown='return event.key != \"Enter\";' onsubmit='getDokumentyPz(); return false;'>
        <table>
        <thead class='thead'>
        <tr>
            <th width='10%'>Miejsce</th>
            <th width='10%'>Data od</th>
            <th width='10%'>Data do</th>
            <th width='15%'>Kontrahent</th>
            <th width='25%'></th>
            <th width='15%'>Numer</th>
            <th width='15%'></th>
        </tr>
        </thead>
        
        <tbody>
            <tr>
                <td>
                    <select onchange='getDokumentyPz();' name='select-dzial' id='select-dzial' style='width: 100%' required>
                        {$this->inw->display_mj_for_select()}
                    </select>
                </td>
                
                <td>
                    <input onchange='getDokumentyPz();' type='date' id='input-data-od' name='input-data-od' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center;' value='{$dateFrom}' required>
                </td>
                
                <td>
                    <input onchange='getDokumentyPz();' type='date' id='input-data-do' name='input-data-do' style='width: 100%; padding-left: 0; padding-right: 0; text-align: center;' value='{$dateTo}' required>
                </td>

                <td>
                    <input type='text' id='input-kontrahent' name='input-kontrahent' placeholder='Nazwa / NIP' oninput='getKontrahenci();' style='width: 100%; padding-left: 0; padding-right: 0; margin-right: 0; text-align: center;' >
                </td>

                <td>
                    <select name='select-kontrahent' id='select-kontrahent' style='width: 100%'>
                    </select>
                </td>

                <td>
                    <input type='text' id='input-numer' name='input-numer' style='width: 100%; padding-left: 0; padding-right: 0; margin-right: 0; text-align: center;' >
                </td>

                <td>
                    <button type='submit' style='width: 100%'><span class='bold'>Wy�wietl</span></button>
                </td>
            </tr>
        </tbody>
        </table>
        </form>

        <div id='div-restrict' class='disabled-zone'>
            <h3>Lista dokument�w<span id='span-lista-dokumentow'></span></h3>
            <div class='table-container' style='min-height: 25vh; max-height: 50vh;'>
                <table width='100%' class='table-autofilter table-autosort'>
                    <thead class='thead'>
                        <tr>
                            <th width='15%' class='table-sortable:default table-sortable hover-underline-animation'>Numer</th>
                            <th width='70%' class='table-sortable:default table-sortable hover-underline-animation'>Kontrahent</th>
                            <th width='15%' class='table-sortable:numeric table-sortable hover-underline-animation'>Data</th>
                        </tr>
                    </thead>

                    <tbody id='tbody-dokumenty-pz'>
                    </tbody>
                </table>
            </div>

            <h3>Szczeg�y dokumentu <span id='span-dokument-pz-title'></span></h3>
            <div class='table-container' style='max-height: 35vh;'>
                <table id='table-pozycje' width='100%' class='table-autofilter table-autosort'>
                    <thead class='thead'>
                        <tr>
                            <th width='40%' class='table-sortable:default table-sortable hover-underline-animation'>Nazwa</th>
                            <th width='20%' class='table-sortable:default table-sortable hover-underline-animation'>EAN</th>
                            <th width='10%' class='table-sortable:numeric table-sortable hover-underline-animation'>Ilo��</th>
                            <th width='15%' class='table-sortable:numeric table-sortable hover-underline-animation'>Cena</td>
                            <th width='15%' class='table-sortable:numeric table-sortable hover-underline-animation'>Warto��</th>
                        </tr>
                    </thead>

                    <tbody id='tbody-pozycje'></tbody>
                    <tfoot class='tfoot'>
                        <tr>
                            <td colspan='4' style='text-align: right; font-weight: bold;'>Suma</td>
                            <td id='td-suma' style='font-weight: bold;'>0</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <br/>
        </div>

        <script src='../../js/table.js?v={$this->inw->get_resources_version()}'></script>
        {$this->get_page_js()}
        </body>

        </html>";

        $this->cleanup();
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new KierownikPzManager($this->title, $this->icon);
    }
}


new KierownikPz(isset($abortDisplaying));