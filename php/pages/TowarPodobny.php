<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageUzytkownik;
use Inwentaryzacja\Managers\TowarPodobnyManager;

require_once "parent/AbstractPageUzytkownik.php";
require_once "../managers/TowarPodobnyManager.php";

// Uzytkownik moze stworzyc identyczny towar
final class TowarPodobny extends AbstractPageUzytkownik
{
    protected string $title = "Podobny towar";
    protected string $icon = "iconoir-search";

    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        $this->inw->handle_search_towar();

        if ($this->inw->get_ean_exists(true)) {
            InwentaryzacjaManager::head_to("../../index.php?ean={$this->inw->get_ean()}");
        } elseif (empty($this->inw->get_html_towar())) {
            $this->redirect();
        } else {
            $this->display_page();
        }
    }

    /**
     * Metoda odpowiada za wyswietlanie strony zarzadzajecej nowym towarem gdy zostaly znalezione podobne towary.
     * Formularz przekierowuje do TowarIstniejacy.php i przesyla:
     * - nazwa: nazwa towaru,
     * - ean: kod EAN towaru,
     * - jm: jednostka miary towaru.
     * - twid: towar id wybranego towaru. 
     *  (-1): Kontynuuj tworzenie nowego towaru, 
     * !(-1): kontynuuj z wybranym towarem,
     * 
     * - towar_nowy: 
     *  (1): Kontynuuj tworzenie nowego towaru, 
     *  (0): kontynuuj z wybranym towarem,
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar();
        $lastInw = $this->inw->display_last_inwentaryzacja();
        $newTowarData = $this->inw->get_new_towar_data();
        $jm = $_COOKIE["sl_jm"][$newTowarData["tw_jmid"]];

        echo "
        {$this->inw->display_head()}

        <body>
        {$lastInw}
        {$titleBar}
        <hr/>
        
        <p style='margin: 8px; width: 100%; text-align: center; padding-bottom: 4px;'><em>Zaznacz towar z listy lub kontynuuj tworzenie nowego</em></p>
        <div class='table-container' style='height: auto; max-height: 50vh'>
            <div style='width: 100%;'>
            {$this->inw->get_html_towar()}
            </div>
        </div>

        <form action='TowarIstniejacy.php' method='post' id='myForm' onkeydown='return event.key != \"Enter\";' onsubmit='return false;'>
            <caption>
                <h2><i class='iconoir-info-empty' style='display: inline-block; vertical-align: middle;'></i> Wybrany towar</h2>
            </caption>
            <hr/>

            <label for='nazwa'>Nazwa</label>
            <input type='text' name='nazwa' id='nazwa' placeholder='Nazwa towaru' value='{$newTowarData["tw_nazwa"]}' readonly='readonly' required>
            
            <label for='ean'>EAN</label>
            <input name='ean' id='ean' value='{$newTowarData["te_code"]}' readonly='readonly'>

            <label for='jm'>Jednostka miary</label>
            <input id='jm' value='{$jm}' readonly='readonly' required>

            <input name='jm' id='jm' value='{$newTowarData["tw_jmid"]}' type='hidden' readonly='readonly' required>
            <input name='twid' id='twid' value='-1' type='hidden' readonly='readonly' required>
            <input id='towar_nowy' name='towar_nowy' value='1' type='hidden' readonly='readonly' required>
            <br/><hr/>

            <button type='submit' id='submit-input' onclick='document.getElementById(\"myForm\").submit()'><span class='bold'>Kontynuuj</span></button>
        </form>

        {$this->get_page_js()}
        </body>
        
        </html>";

        $this->cleanup();
    }

    /**
     * Metoda odpowiada za przekierowanie do TowarIstniejacy.php w przypadku gdy nie zostaly znalezione podobne towary
     * @return void
     */
    private function redirect(): void
    {
        $newTowarData = $this->inw->get_new_towar_data();
        $nazwa = $newTowarData['tw_nazwa'] ?? "";
        $ean = $newTowarData['te_code'] ?? "";
        $jm = $newTowarData['tw_jmid'] ?? "";

        echo "   
        <body onload=\"document.redirectform.submit()\">
            <form method='POST' action='TowarIstniejacy.php' name='redirectform' style='display: none;'>
            <input name='nazwa' value='{$nazwa}'>
            <input name='ean' value='{$ean}'>
            <input name='jm' value='{$jm}'>
            <input name='towar_nowy' value='1'>
            <input name='twid' value='-1'>
            </form>
        </body>";
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new TowarPodobnyManager($this->title, $this->icon);
    }
}

new TowarPodobny(isset($abortDisplaying));