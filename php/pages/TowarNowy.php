<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageUzytkownik;
use Inwentaryzacja\Managers\TowarNowyManager;

require_once "parent/AbstractPageUzytkownik.php";
require_once "../managers/TowarNowyManager.php";

final class TowarNowy extends AbstractPageUzytkownik
{
    protected string $title = "Nowy towar";
    protected string $icon = "iconoir-box-iso";
    protected int $mainMenuPosition = 3;

    public function __construct(bool $abortDisplaying = false)
    {
        if (!$abortDisplaying) {
            InwentaryzacjaManager::clear_session(true);
        }
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        $this->display_page();
    }

    /**
     * Metoda odpowiada za wyswietlanie strony nowego towaru.
     * Formularz przekierowuje do TowarPodobny.php i przesyla:
     * - nazwa: nazwa towaru,
     * - ean: kod EAN towaru,
     * - jm: jednostka miary towaru.
     *
     * @return void
     */
    protected function display_page(): void
    {
        $titleBar = $this->inw->display_title_bar();
        $lastInw = $this->inw->display_last_inwentaryzacja();

        echo "
        {$this->inw->display_head()}

        <body>
        {$lastInw}
        {$titleBar}
        <hr/>

        <form action='TowarPodobny.php' method='post' id='myForm' onkeydown='return event.key != \"Enter\";' onsubmit='return false;'>
            <label for='nazwa' id='nazwa-label'>Nazwa:</label>
            <input type='text' name='nazwa' id='nazwa' placeholder='Nazwa towaru' pattern='^[a-zA-Z0-9 .,-]*$' autofocus required>
            <br/>
            
            <label id='label-ean' for='ean'>EAN:</label><br id='br-ean'>
            <input type='text' name='ean' id='ean' value='{$this->inw->get_ean()}' maxlength='15' pattern='-?\d+' required>

            <span>
                <input id='input-ean' type='checkbox' onclick='handleEANCheckBoxClick(this);' style='vertical-align: middle;'>
                <label for='input-ean' style='display: inline-block; vertical-align: middle;'>Towar bez kodu EAN</label>
                <br/>
                <br/>
            </span>

            <label for='jm'>Jednostka miary:</label>
            <select name='jm' id='jm' required>
                {$this->inw->display_jm_for_select()}
            </select>
            <br/>
            <hr/>

            <button type='button' onclick=\"window.location.href='TowarLista.php'\" style='float: left;'>Wybierz z listy</button>
            <button type='submit' onclick='confirmSubmit();' style='float: right;'><span class='bold'>Kontynuuj</span></button>
        </form>

        {$this->get_page_js()}
        </body>
        
        </html>";

        $this->cleanup();
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new TowarNowyManager($this->title, $this->icon);
    }
}

new TowarNowy(isset($abortDisplaying));