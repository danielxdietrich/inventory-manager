<?php

namespace Inwentaryzacja\Pages;

use Inwentaryzacja\InwentaryzacjaManager;
use Inwentaryzacja\Pages\Parent\AbstractPageKierownik;
use Inwentaryzacja\Managers\KierownikPzDrukowanieManager;
use Inwentaryzacja\Models\SpisInwentaryzacja;

require_once "parent/AbstractPageKierownik.php";
require_once "../managers/KierownikPzDrukowanieManager.php";
require_once "C:/xampp/htdocs/vendor/autoload.php";

/**
 * Klasa odpowiedzialna za generowanie dokumentu PDF z spisem inwentaryzacyjnym
 */
final class KierownikPzDrukowanie extends AbstractPageKierownik
{
    protected string $title = "Spis inwentaryzacji";

    private SpisInwentaryzacja $spis;

    private int $iwid;
    private string $dataFormat = "d.m.Y";

    private string $adres1 = "XYZ Sp. z o.o";
    private string $adres2 = "ul. XYZ";
    private string $adres3 = "00-000 XYZ";
    private string $nip = "NIP 0000000000";

    private string $miejsce = "";
    private string $numer = "";
    private string $data = "";

    private int $fontSize = 9;

    /**
     * Konstruktor sprawdza czy sa dostepne wymagane dane $_GET["druk_iwid"] && $_POST["towar"]) by kontynuowac generowanie dokumentu PDF
     */
    public function __construct(bool $abortDisplaying = false)
    {
        parent::__construct();

        if ($abortDisplaying) {
            return;
        }

        if (empty($_GET["druk_iwid"])) {
            echo '<strong>Co� posz�o nie tak, spr�buj ponownie.</strong> Karta zostanie automatycznie zamkni�ta za 120s. Naci�nij spacj� aby anulowa� zamkni�cie karty.<hr/>';
            echo '
            <script type="text/javascript">
                let shouldClose = true;

                setTimeout("close_tab();", 120000);

                document.body.onkeyup = function (e) {
                    if (e.key == " " || e.code == "Space" || e.keyCode == 32) {
                        shouldClose = false;
                    }
                }

                function close_tab() {
                    if (shouldClose) {
                        window.close();
                    }
                }
            </script>';

            throw new \LogicException('Nie znaleziono $_GET["druk_iwid"]');
        } elseif (!is_numeric($_GET["druk_iwid"])) {
            throw new \InvalidArgumentException('$_GET["druk_iwid"] nie jest typu liczbowego');
        } else {
            $this->iwid = intval($_GET["druk_iwid"]);
        }

        $this->initialize_data();
        $this->display_page();
    }

    /**
     * Metoda wyswietla wygenerowany dokument PDF
     *
     * @return void
     */
    protected function display_page(): void
    {
        $mpdf = new \Mpdf\Mpdf([
            'margin_left' => 5,
            'margin_right' => 5,
            'margin_top' => 5,
            'margin_bottom' => 15,
            'margin_header' => 10,
            'margin_footer' => 10
        ]);

        $footer = array(
            'odd' => array(
                'C' => array(
                    'content' => '{PAGENO}/{nbpg}',
                ),
            )
        );

        $mpdf->allow_charset_conversion = true;
        $mpdf->charset_in = "Windows-1250";
        $mpdf->SetProtection(array("print"));
        $mpdf->SetTitle("{$this->get_title()}");
        $mpdf->SetAuthor("ABC Sp. z o.o.");
        $mpdf->SetDisplayMode("fullpage");
        $mpdf->setFooter($footer);

        $html = $this->get_html();
        $mpdf->WriteHTML($html);

        try {
            $mpdf->Output();
        } catch (\Mpdf\MpdfException $exception) {
            echo $exception->getMessage();
            echo "<pre>";
            throw $exception;
        }
    }

    /**
     * Metoda inicjalizuje dane potrzebne do wygenerowania dokumentu PDF oraz dodaje recznie przypisane ceny do bazy danych
     *
     * @return void
     */
    private function initialize_data(): void
    {
        $this->inw->delete_from_ic_ceny($this->iwid);

        $inwDane = $this->inw->get_iw__inwentaryzacja_dane($this->iwid);
        $this->spis = new SpisInwentaryzacja();

        $this->miejsce = $_COOKIE["sl_mj"][$inwDane["iw_mjid"]];
        $this->numer = $inwDane["iw_nr"];
        $this->data = $inwDane["iw_data"]->format($this->dataFormat);

        if ($inwDane["iw_mjid"] == 2) {
            $this->adres1 = "XYZ Sp. j.";
            $this->adres2 = "ul. XYZ";
            $this->adres3 = "00-000 XYZ";
            $this->nip = "NIP 0000000000";
        } else {
            $this->adres1 = "XYZ Sp. z o.o";
            $this->adres2 = "ul. XYZ";
            $this->adres3 = "00-000 XYZ";
            $this->nip = "NIP 0000000000";
        }

        if (isset($_POST["towar"])) {
            // $_POST["towar"] jest tablica z towarem, ktorych ceny zostaly recznie przypisane
            $this->spis->load_towary_array($_POST["towar"]);
            $this->spis->add_dodane_pozniej_to_db($inwDane["iw_data"]->format(InwentaryzacjaManager::DATE_FORMAT), $inwDane["iw_mjid"]);
        }
    }

    /**
     * Metoda zwraca kod CSS potrzebny do wygenerowania dokumentu PDF
     *
     * @return string Kod CSS
     */
    private function get_stylesheet(): string
    {
        $fontMonospace = "Courier New";

        return "
            body {
                font-family: sans-serif;
                font-size: {$this->fontSize}pt;
            }
            table {
                table-layout: fixed;
                overflow: wrap;
                border: none;
            }
            .table-items tbody tr td,
            .table-items thead tr td {
                word-wrap: break-word;
                border: 0.1mm solid black;
            }
            .table-items thead tr td {
                background-color: lightgray;
            }
            .monospace {
                font-family: '{$fontMonospace}';
            }
            td {
                line-height: 1;
            }
            .pozycja {
                line-height: 1;
            }";
    }

    /**
     * Metoda generuje kod HTML dokumentu PDF
     * 
     * @return string Kod HTML dokumentu PDF
     */
    private function get_html(): string
    {
        $style = $this->get_stylesheet();
        $header = $this->get_html_header();
        $rows = $this->spis->to_string();
        $lastIndex = $this->spis->get_ilosc_pozycji();

        return "
            <html>
            <head>
            <style>{$style}</style>
            </head>
            <body>
            {$header}
            <table class='table-items' width='100%' style='border-collapse: collapse;' cellpadding='4'>
                <thead>
                    <tr>
                        <td width='5%'>Poz.</td>
                        <td width='44%'>Towar</td>
                        <td width='16%'>EAN</td>
                        <td width='5%'>Jm</td>
                        <td width='10%'>Ilo��</td>
                        <td width='10%'>Cena jedn.</td>
                        <td width='10%'>Warto��</td>
                    </tr>
                </thead>
                <tbody>
                    {$rows}
                </tbody>
            </table>

            <table width='100%'>
                <tr>
                    <td width='50%'>Spis zako�czono na pozycji:&nbsp;{$lastIndex}</td>
                    <td width='50%'></td>
                </tr>
                <tr>
                    <td width='50%'>Podpisy cz�onk�w zespo�u spisowego</td>
                    <td width='50%' style='text-align: right;'>Podpis osoby odpowiedzialnej materialnie</td>
                </tr>
            </table>

            </body>
            </html>";
    }

    /**
     * Metoda zwraca naglowek dokumentu PDF w postaci kodu HTML
     * @return string Naglowek dokumentu PDF w postaci kodu HTML
     */
    private function get_html_header(): string
    {
        return "
            <table width='100%' cellpadding='4'>
                <tr>
                    <td width='30%'>
                        {$this->adres1}<br />
                        {$this->adres2}<br />
                        {$this->adres3}<br />
                        {$this->nip}
                    </td>

                    <td width='35%'></td>
                    <td width='35%'></td>
                </tr>

                <tr>
                    <td colspan='3' style='text-align: center;'>Arkusz spisu z natury nr {$this->numer}</td>
                </tr>

                <tr><td colspan='3'><br /></td></tr>

                <tr>
                    <td valign='top'>Nazwa lub numer pola spisowego</td>
                    <td valign='top'>{$this->miejsce}<br/>surowce</td>
                    <td valign='top'></td>
                </tr>

                <tr>
                    <td valign='top'>Przedmiot spisu</td>
                    <td valign='top'>{$this->miejsce}</td>
                    <td valign='top'></td>
                </tr>

                <tr>
                    <td valign='top'>Osoba odpowiedzialna materialnie</td>
                    <td valign='top'><p>{$this->get_dotted_row(40)}</p></td>
                    <td valign='top'></td>
                </tr>

                <tr>
                    <td valign='top'>Sk�ad zespo�u spisowego:<br /><p>{$this->get_input_rows(40, 3)}</p></td>
                    <td valign='top'><p></p></td>
                    <td valign='top'>Inne osoby obecne przy spisie:<br /><p>{$this->get_input_rows(40, 3)}</p></td>
                </tr>

                <tr>
                    <td colspan='3' style='text-align: center;'>Spis z natury na dzie�: {$this->data}</td>
                </tr>
            </table>";
    }

    /**
     * Metoda generuje kod HTML z wierszami do wpisywania danych
     *
     * @param integer $dotCount Ilo�� kropek w wierszu
     * @param integer $rows Ilo�� wierszy
     * @return string
     */
    private function get_input_rows(int $dotCount, int $rows): string
    {
        $row = "{$this->get_dotted_row($dotCount)}<br/>";
        $output = "";

        for ($i = 0; $i < $rows; $i++) {
            $output .= "{$row}";
        }
        return $output;
    }

    /**
     * Metoda generuje kod HTML z wierszem z kropek
     *
     * @param integer $dotCount Ilo�� kropek w wierszu
     * @return string
     */
    private function get_dotted_row($dotCount): string
    {
        $output = "";
        for ($i = 0; $i < $dotCount; $i++) {
            $output .= ".";
        }
        return $output;
    }

    protected function instantiate_manager(): InwentaryzacjaManager
    {
        return new KierownikPzDrukowanieManager($this->title, $this->icon);
    }
}

new KierownikPzDrukowanie(isset($abortDisplaying));