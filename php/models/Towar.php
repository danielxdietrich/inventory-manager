<?php

namespace Inwentaryzacja\Models;

/**
 * Klasa reprezentujaca towar w spisie inwentaryzacyjnym
 */
final class Towar
{
    private string $nazwa;
    private string $ean = "";
    private int $twid;
    private int $jmid;
    private int $zl = 0;
    private int $gr = 0;
    private float $ilosc = 0;

    // ID pozycji w PZ
    private int $ppid = -1;
    // Numer dokumentu PZ
    private string $pznr = "";

    public function get_twid(): int
    {
        return $this->twid;
    }

    public function get_zl(): int
    {
        return $this->zl;
    }

    public function get_gr(): int
    {
        return $this->gr;
    }

    public function get_ilosc(): float
    {
        return $this->ilosc;
    }

    public function get_nazwa(): string
    {
        return $this->nazwa;
    }

    public function get_ean(): string
    {
        return $this->ean;
    }

    public function get_jmid(): int
    {
        return $this->jmid;
    }

    public function get_cena(): float
    {
        return round($this->zl + $this->gr / 100, 2);
    }

    public function get_ppid(): int
    {
        return $this->ppid;
    }

    public function get_pznr(): string
    {
        return $this->pznr;
    }

    public function set_twid(int $twid): self
    {
        $this->twid = $twid;
        return $this;
    }

    public function set_cena(float $cena): self
    {
        $this->zl = (int) $cena;
        $this->gr = (int) (round(($cena - $this->zl) * 100, 2));

        return $this;
    }

    public function set_zl(int $zl): self
    {
        $this->zl = $zl;
        return $this;
    }

    public function set_gr(int $gr): self
    {
        $this->gr = $gr;
        return $this;
    }

    public function set_ilosc(float $ilosc): self
    {
        $this->ilosc = $ilosc;
        return $this;
    }

    public function set_nazwa(string $nazwa): self
    {
        $this->nazwa = $nazwa;
        return $this;
    }

    public function set_ean(?string $ean): self
    {
        if (!is_null($ean)) {
            $this->ean = $ean;
        }
        return $this;
    }

    public function set_jmid(int $jmid): self
    {
        $this->jmid = $jmid;
        return $this;
    }

    public function set_ppid(int $ppid): self
    {
        $this->ppid = $ppid;
        return $this;
    }

    public function set_pznr(string $pznr): self
    {
        $this->pznr = $pznr;
        return $this;
    }

    public function add_ilosc(float $ilosc): self
    {
        $this->ilosc += $ilosc;
        return $this;
    }

    /**
     * Metoda tworzy identyfikator towaru na podstawie jego twid oraz ceny
     * 
     * @return string Identyfikator towaru
     */
    public function get_identifier(): string
    {
        $cenaFormat = number_format($this->get_cena(), 2, ',', ' ');
        return "{$this->get_twid()}_{$cenaFormat}";
    }

    /**
     * Metoda zwraca rzad tabeli z danymi towaru w formacie HTML wraz z ukrytymi polami input
     *
     * @param integer $pozycja Numer pozycji w tabeli
     * @param boolean $editable Czy rzad jest edytowalny
     * @return string Rzad tabeli w formacie HTML
     */
    public function get_table_row(int $pozycja, bool $editable = false, float $savedCena = 0): string
    {
        $ilosc = number_format($this->get_ilosc(), 2);
        $cena = number_format(($editable ? $savedCena : $this->get_cena()), 2);
        $wartosc = number_format(round($this->get_ilosc() * $cena, 2), 2);

        $dodaneRecznie = $editable ? 1 : 0;
        $token = "{$pozycja}-{$this->twid}-{$ilosc}";

        $savedCenaZl = (int) $cena;
        $savedCenaGr = (int) (round(($cena - $savedCenaZl) * 100, 2));

        $zl = $editable ? $savedCenaZl : $this->get_zl();
        $gr = $editable ? $savedCenaGr : $this->get_gr();

        $bgColor = ($editable && $cena > 0) ? 'var(--next-border-color)' : 'var(--error-bg-color)';

        $cenaRow = $editable ? 
            "<td><input id='input-cena-{$pozycja}' value='{$cena}' class='{$token}' oninput='updateWartosc({$pozycja});' onchange='savePriceTemp({$pozycja});' type='number' min='0.01' step='0.01' name='cena' style='margin: 0; text-align: right; max-width: 80%; height: 0.5em; background-color: {$bgColor};' required></td>" :
            "<td id='td-cena-{$pozycja}' style='text-align: right;'>{$cena}</td>";

        return trim("
            <tr id='poz-{$pozycja}' class='pozycja tr-editable'>
                <td style='text-align: right;'>{$pozycja}</td>
                <td class='td-nazwa-twid-{$this->get_twid()}'>{$this->get_nazwa()}</td>
                <td id='td-ean-{$pozycja}' style='text-align: right;' class='monospace td-ean-twid-{$this->get_twid()}'>{$this->ean}</td>
                <td class='td-jm-twid-{$this->get_twid()}'>{$_COOKIE["sl_jm"][$this->get_jmid()]}</td>
                <td id='td-ilosc-{$pozycja}' style='text-align: right;'>{$ilosc}</td>
                {$cenaRow}
                <td id='td-wartosc-{$pozycja}' style='text-align: right;'>{$wartosc}</td>

                <input type='hidden' name='towar[{$pozycja}][pznr]' id='input-pznr-{$pozycja}' value='{$this->get_pznr()}'>
                <input type='hidden' name='towar[{$pozycja}][ppid]' id='input-ppid-{$pozycja}' value='{$this->get_ppid()}'>
                <input type='hidden' name='towar[{$pozycja}][twid]' id='input-twid-{$pozycja}' value='{$this->get_twid()}'>
                <input type='hidden' name='towar[{$pozycja}][jmid]' id='input-jmid-{$pozycja}' value='{$this->get_jmid()}' class='input-jmid-twid-{$this->get_twid()}'>
                <input type='hidden' name='towar[{$pozycja}][ilosc]' value='{$this->get_ilosc()}' id='input-ilosc-{$pozycja}'>
                <input type='hidden' name='towar[{$pozycja}][zl]' value='{$zl}' id='input-zl-{$pozycja}'>
                <input type='hidden' name='towar[{$pozycja}][gr]' value='{$gr}' id='input-gr-{$pozycja}'>
                <input type='hidden' name='towar[{$pozycja}][nazwa]' id='input-nazwa-{$pozycja}' value='{$this->get_nazwa()}' class='input-nazwa-twid-{$this->get_twid()}'>
                <input type='hidden' name='towar[{$pozycja}][ean]' id='input-ean-{$pozycja}' value='{$this->get_ean()}' class='input-ean-twid-{$this->get_twid()}'>
                <input type='hidden' name='towar[{$pozycja}][dodane_recznie]' id='input-dodane-recznie-{$pozycja}' value='{$dodaneRecznie}'>
            </tr>");
    }
}