<?php

namespace Inwentaryzacja\Models;

use Inwentaryzacja\InwentaryzacjaManager;

require_once __DIR__ . "\..\InwentaryzacjaManager.php";
require_once "Towar.php";

/**
 * Klasa reprezentujaca spis inwentaryzacyjny
 */
final class SpisInwentaryzacja
{
    // Liczba okreslajaca, ile dokumentow wstecz ma byc brane pod uwage przy tworzeniu spisu inwentaryzacyjnego
    private const ILOSC_SKANOWANYCH_PZ = 2000;

    private InwentaryzacjaManager $inwManager;

    // Tablica z indeksami dokumentow PZ
    public array $arrayDokumentyPzIds = array();
    // Tablica towarow policzonych w inwentaryzacji
    private array $arrayTowaryPoliczone = array();
    // Tablica tablic towarow w dokumentach PZ
    private array $arrayDokumentyPz = array();
    // Tablica towarow ze znalezionymi cenami
    private array $arrayPelnySpisTowarow = array();
    // Tablica towarow z cenami przypisanymi recznie
    private array $arrayDodaneRecznie = array();
    // Tablica towarow z cenami tymczasowymi wczytanymi z bazy danych
    private array $arrayCenyTymczasowe = array();

    private int $iwid;

    /**
     * Konstruktor. Jezeli parametr $iwid jest ponizej 1, to oczekiwane jest wczytanie pelnego spisu inwentaryzacyjnego z danych json
     *
     * @param integer $iwid Identyfikator inwentaryzacji
     */
    public function __construct(int $iwid = 0, int $mjid = 1)
    {
        $this->inwManager = new InwentaryzacjaManager();

        if ($iwid > 0) {
            $inwData = $this->inwManager->get_iw__inwentaryzacja_dane($iwid)["iw_data"]->format(InwentaryzacjaManager::DATE_FORMAT);

            $this->arrayDokumentyPzIds = $this->inwManager->get_dokumenty_pz_do($inwData, self::ILOSC_SKANOWANYCH_PZ, $mjid);
            $this->arrayCenyTymczasowe = $this->inwManager->get_ic_ceny($iwid);
            $this->arrayTowaryPoliczone = $this->get_array_towary_policzone($iwid);
            $this->arrayPelnySpisTowarow = array();
            $this->iwid = $iwid;

            $this->process_results();
        }
    }

    /**
     * Przyjmuje tablice z surowymi danymi towarow i przetwarza je na tablice obiektow Towar 
     *
     * @param array $towary Surowe dane towarow
     * @return void
     */
    public function load_towary_array(array $towary): void
    {
        $this->arrayPelnySpisTowarow = array();

        foreach ($towary as $towar) {
            $towarObj = (new Towar())
                ->set_twid($towar["twid"])
                ->set_nazwa($towar["nazwa"])
                ->set_ilosc($towar["ilosc"])
                ->set_jmid($towar["jmid"])
                ->set_zl($towar["zl"])
                ->set_gr($towar["gr"])
                ->set_ean($towar["ean"]);

            $identifier = $towarObj->get_identifier();

            if (array_key_exists($identifier, $this->arrayPelnySpisTowarow)) {
                $this->arrayPelnySpisTowarow[$identifier]->add_ilosc($towarObj->get_ilosc());
            } else {
                $this->arrayPelnySpisTowarow[$identifier] = $towarObj;
            }

            if ($towar["dodane_recznie"] == 1) {
                $this->arrayDodaneRecznie[$identifier] = $towarObj;
            }
        }
    }

    /**
     * Dodaje wszystkie towary z cenami dodanymi recznie do ostatniego dokumentu PZ w bazie danych
     *
     * @param string $data
     * @return void
     */
    public function add_dodane_pozniej_to_db(string $data, string $mjid): void
    {
        $pzid = $this->inwManager->get_dokumenty_pz_do($data, 1, $mjid)[0];
        $arrayTowar = array();

        if (empty($pzid)) {
            echo "<strong>Nie znaleziono �adnego wprowadzonego dokumentu PZ dla {$_COOKIE["mjid"][$mjid]}. Prosz� wprowadzi� dokument PZ i spr�bowa� ponownie.</strong> Karta zostanie automatycznie zamkni�ta za 120s. Naci�nij spacj� aby anulowa� zamkni�cie karty.<hr/>";
            echo '
            <script type="text/javascript">
                let shouldClose = true;

                setTimeout("close_tab();", 120000);

                document.body.onkeyup = function (e) {
                    if (e.key == " " || e.code == "Space" || e.keyCode == 32) {
                        shouldClose = false;
                    }
                }

                function close_tab() {
                    if (shouldClose) {
                        window.close();
                    }
                }
            </script>';

            throw new \LogicException("Nie znaleziono �adnego wprowadzonego dokumentu PZ dla {$_COOKIE["mjid"][$mjid]}. Prosz� najpierw wprowadzi� dokument PZ.");
        }

        foreach ($this->arrayDodaneRecznie as $towar) {
            $arrayRow = array(
                $towar->get_twid(),
                $towar->get_ilosc(),
                $towar->get_cena(),
            );

            array_push($arrayTowar, $arrayRow);
        }

        $this->inwManager->insert_into_pz_pozycja($pzid, $arrayTowar, true);
    }

    /**
     * Zwraca wartosc logiczna, czy wszystkie towary na liscie maja przypisana cene
     *
     * @return boolean Czy wszystkie towary maja przypisana cene
     */
    public function is_complete(): bool
    {
        foreach ($this->arrayPelnySpisTowarow as $towar) {
            if ($towar->get_cena() == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Zwraca tabele z wygenerowanym spisem inwentaryzacyjnym
     * 
     * @return string Tabela HTML
     */
    public function to_string(bool $allowEditable = false): string
    {
        $str = "";
        $pozycja = 1;
        $wartoscSuma = 0;

        foreach ($this->arrayPelnySpisTowarow as $key => $towar) {
            $wczytanaCena = 0;

            foreach ($this->arrayCenyTymczasowe as $row) {
                if ($row['ic_twid'] == $towar->get_twid()) {
                    $wczytanaCena = floatval($row['ic_cena']);
                    break;
                }
            }
            
            $cena = $wczytanaCena > 0 ? $wczytanaCena : $towar->get_cena();
            $wartoscSuma += round($cena * $towar->get_ilosc(), 2);

            $isEditable = $allowEditable && ($towar->get_cena() <= 0) ? true : false;
            $str .= $towar->get_table_row($pozycja, $isEditable, $wczytanaCena);
            $pozycja++;
        }

        $wartoscSuma = number_format($wartoscSuma, 2, ".", " ");

        if (!$allowEditable) {
            $str .=
                "<tr>
                <td style='border: none;'></td>
                <td style='border: none;'></td>
                <td style='border: none;'></td>
                <td style='border: none;'></td>
                <td style='border: none;'></td>
                <td style='border: none;'></td>
                <td style='text-align: right;'>{$wartoscSuma}</td>
                </tr>";
        } else {
            $iloscPozycji = $pozycja - 1;
            $str .= PHP_EOL . "<input type='hidden' id='ilosc-pozycji' value='{$iloscPozycji}'>";
        }

        return $str;
    }

    /**
     * Zwraca ilosc wszystkich pozycji w spisie inwentaryzacyjnym
     * 
     * @return int ilosc pozycji
     */
    public function get_ilosc_pozycji(): int
    {
        return count($this->arrayPelnySpisTowarow);
    }

    /**
     * Zwraca pelny spis towarow z cenami
     * 
     * @return array Pelny spis towarow z cenami
     */
    public function get_pelny_spis_towarow(): array
    {
        return $this->arrayPelnySpisTowarow;
    }

    /**
     * Zwraca ID inwentaryzacji
     * 
     * @return int ID inwentaryzacji
     */
    public function get_iwid(): int
    {
        return $this->iwid;
    }

    /**
     * Zwraca tablice towarow policzonych w inwentaryzacji
     * 
     * @param int $iwid Identyfikator inwentaryzacji
     * @return array Tablica towarow policzonych w inwentaryzacji
     */
    private function get_array_towary_policzone(int $iwid): array
    {
        $resourceTowarPoliczony = $this->inwManager->get_towar_policzony_by_inwentaryzacja_resource($iwid, true);
        $arrayResult = array();

        while ($row = sqlsrv_fetch_array($resourceTowarPoliczony, SQLSRV_FETCH_ASSOC)) {
            $towar = (new Towar())
                ->set_twid($row['tw_id'])
                ->set_nazwa($row['tw_nazwa'])
                ->set_ean($row['te_code'])
                ->set_ilosc($row['ti_ilosc'])
                ->set_jmid($row['tw_jmid']);

            array_push($arrayResult, $towar);
        }

        return $arrayResult;
    }

    /**
     * Zwraca tablice towarow z wybranego dokumentu PZ
     * 
     * @param int $pzid
     * @return array Tablica towarow z wybranego dokumentu PZ
     */
    private function get_array_towary_pz(int $pzid): array
    {
        $resourceTowarPz = $this->inwManager->get_pozycje_pz_resource($pzid);
        $arrayResult = array();

        while ($row = sqlsrv_fetch_array($resourceTowarPz, SQLSRV_FETCH_ASSOC)) {
            $towar = (new Towar())
                ->set_twid($row['tw_id'])
                ->set_nazwa($row['tw_nazwa'])
                ->set_ean($row['te_code'])
                ->set_ilosc($row['pp_ilosc'])
                ->set_cena($row['pp_cena'])
                ->set_jmid($row['tw_jmid'])
                ->set_ppid($row['pp_id'])
                ->set_pznr($row['pz_nr']);

            array_push($arrayResult, $towar);
        }

        return $arrayResult;
    }

    /**
     * Zwraca tablice towarow z dokumentu PZ z cache lub ja pobiera z bazy danych
     */
    private function get_towary_from_pz(int $pzid): array
    {
        if (!array_key_exists($pzid, $this->arrayDokumentyPz)) {
            $this->arrayDokumentyPz[$pzid] = $this->get_array_towary_pz($pzid);
        }

        return $this->arrayDokumentyPz[$pzid];
    }

    /**
     * Przetwarza pojedynczy towar, dopasowujac go do cen z dokumentow PZ
     * 
     * @param Towar $towarPoliczony Towar policzony w inwentaryzacji
     * @param Towar $towarPz Towar z dokumentu PZ
     * @param int &$iloscSuma Referencja do zmiennej przechowujacej sume ilosci
     * @return void
     */
    private function process_towar(Towar $towarPoliczony, Towar $towarPz, float &$iloscSuma): void
    {
        $klucz = $towarPz->get_identifier();

        $isOnTheList = key_exists($klucz, $this->arrayPelnySpisTowarow);
        $doesntExceedMax = round($iloscSuma + $towarPz->get_ilosc(), 3) <= $towarPoliczony->get_ilosc();
        $iloscDoDodania = $doesntExceedMax ? $towarPz->get_ilosc() : round($towarPoliczony->get_ilosc() - $iloscSuma, 3);

        if ($isOnTheList) {
            $this->arrayPelnySpisTowarow[$klucz]->add_ilosc($iloscDoDodania);
            $this->arrayPelnySpisTowarow[$klucz]->set_cena($towarPz->get_cena());
        } else {
            $towar = clone $towarPz;
            $towar->set_ilosc($iloscDoDodania);
            $this->arrayPelnySpisTowarow[$klucz] = $towar;
        }

        $iloscSuma = round($iloscSuma + $iloscDoDodania, 3);
    }

    /**
     * Przetwarza i dopasowuje ceny towarow w dokumentach PZ do towarow policzonych w inwentaryzacji
     * @return void
     */
    private function process_results(): void
    {
        foreach ($this->arrayTowaryPoliczone as $towarPoliczony) {
            // Zmienna zliczajaca ilosc towaru niezaleznie od ceny
            $iloscSuma = 0;

            foreach ($this->arrayDokumentyPzIds as $id) {
                $arrayTowaryPz = $this->get_towary_from_pz($id);

                foreach ($arrayTowaryPz as $towarPz) {
                    if ($iloscSuma == $towarPoliczony->get_ilosc()) {
                        break 2;
                    }

                    if ($towarPoliczony->get_twid() == $towarPz->get_twid()) {
                        $this->process_towar($towarPoliczony, $towarPz, $iloscSuma);
                    }
                }
            }

            if ($iloscSuma < $towarPoliczony->get_ilosc()) {
                $niedopasowanaIlosc = $towarPoliczony->get_ilosc() - $iloscSuma;
                $pozostalyTowar = (new Towar())
                    ->set_twid($towarPoliczony->get_twid())
                    ->set_nazwa($towarPoliczony->get_nazwa())
                    ->set_ean($towarPoliczony->get_ean())
                    ->set_ilosc($niedopasowanaIlosc)
                    ->set_jmid($towarPoliczony->get_jmid());
                $this->arrayPelnySpisTowarow[$pozostalyTowar->get_identifier()] = $pozostalyTowar;
            }
        }
    }
}
