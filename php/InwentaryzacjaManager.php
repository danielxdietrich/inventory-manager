<?php

namespace Inwentaryzacja;

use Encoding;
use Inwentaryzacja\DatabaseManager;

require_once "Encoding.php";
require_once "Market.php";
require_once "DatabaseManager.php";
require_once "models/SpisInwentaryzacja.php";

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

/**
 * Klasa odpowiadajaca za zarzadzanie aplikacja
 * 
 * Podstrony wymagajace dodatkowej funkcjonalnosci powinny posiadac klase dziedziczaca po tej klasie
 * @author Daniel Dietrich
 */
class InwentaryzacjaManager
{
    public const APP_NAME = "Inwentaryzacja";
    public const APP_VERSION = "1.24";

    public const NO_EAN_CODE = "-1";
    public const WEIGHT_PRECISION = 0.001;
    public const TIMESTAMP_FORMAT = "d/m/Y H:i";
    public const DATE_PRINT_FORMAT = "d/m/Y";
    public const DATE_FORMAT = "Y-m-d";

    protected ?DatabaseManager $db = null;

    protected string $ean;
    protected int $twid;

    protected string $pageTitle;
    protected string $pageIcon;
    protected int $resourcesVersion = 78;

    public function __construct(string $title = "", string $icon = "")
    {
        if (is_null($this->db)) {
            $this->db = new DatabaseManager();
        }

        $this->pageTitle = $title;
        $this->pageIcon = $icon;

        $this->handle_ean();
        $this->handle_twid();
    }

    /**
     * Funkcja rozpoczyna dzia�anie aplikacji i odpowiednio zarzadza przekierowaniem uzytkownika
     *
     * @return void
     */
    final public static function launch(): void
    {
        self::clear_session(true);

        $inw = new self();
        if ($inw->is_logged_in()) {
            if ($inw->get_ean_exists()) {
                self::head_to("php/pages/TowarIstniejacy.php");
            } else {
                self::head_to("php/pages/TowarNowy.php?ean={$inw->get_ean()}");
            }
        } else {
            self::head_to("php/pages/Logowanie.php");
        }
    }

    /**
     * Metoda przekierowuje uzytkownika do wybranej podstrony
     *
     * @param string $path Sciezka do pliku (domyslnie ../../index.php)
     * @return void
     */
    final public static function head_to(string $path = "../../index.php"): void
    {
        ob_start();
        header("Location: {$path}");
        ob_end_flush();
    }

    /**
     * Funkcja umozliwia usuwanie wszystkich przechowywanych ciasteczek
     *
     * @return void
     */
    final public static function clear_cookies(): void
    {
        $past = time() - 3600;

        if (isset($_SERVER["HTTP_COOKIE"])) {
            $cookies = explode(";", $_SERVER["HTTP_COOKIE"]);
            foreach ($cookies as $cookie) {
                $parts = explode("=", $cookie);
                $name = trim($parts[0]);

                setcookie($name, "", $past);
                setcookie($name, "", $past, "/");
            }
        }
    }

    /**
     * Metoda usuwa przechowywane zmienne sesyjne
     *
     * @param bool $deepReset Czy usunac wszystkie zmienne
     * @return void
     */
    final public static function clear_session(bool $deepReset = false): void
    {
        unset($_SESSION["nazwa"]);
        unset($_SESSION["jm"]);
        unset($_SESSION["mj"]);

        unset($_SESSION["inwentaryzacja_added"]);
        unset($_SESSION["towar_added"]);

        unset($_SESSION["ean_exists"]);
        unset($_SESSION["towar_exists"]);

        if ($deepReset) {
            unset($_SESSION["ean"]);
            unset($_SESSION["twid"]);
        }
    }

    /**
     * Metoda odpowiada za przypisywanie odpowiedniego kodu EAN w obiekcie
     *
     * @return void
     */
    final protected function handle_ean(): void
    {
        if (!empty($_GET["ean"])) {
            $thatEan = trim($_GET["ean"]);

            if ($_GET["ean"] != self::NO_EAN_CODE) {
                $thatEan = $this->extract_ean($thatEan);
                $this->set_ean($thatEan);
            } else {
                $this->set_ean($thatEan);
                $this->set_ean_exists(false);
            }
        } elseif (!empty($_POST["ean"])) {
            if ($_POST["ean"] != self::NO_EAN_CODE) {
                $thatEan = trim($_POST["ean"]);
                $this->set_ean($thatEan);
            } else {
                $this->set_ean(self::NO_EAN_CODE);
                $this->set_ean_exists(false);
            }
        } elseif (!empty($_SESSION["ean"])) {
            $this->set_ean($_SESSION["ean"]);
        } else {
            $this->set_ean(self::NO_EAN_CODE);
            $this->set_ean_exists(false);
        }

        if (isset($this->ean)) {
            $_SESSION["ean"] = $this->get_ean();
        }
    }

    /**
     * Metoda odpowiada za przypisywanie odpowiedniego id towaru w obiekcie
     *
     * @param integer $twid ID towaru do przypisania w obiekcie
     * @return void
     */
    final protected function handle_twid(int $twid = null): void
    {
        if (!$this->is_logged_in()) {
            return;
        }

        if (!is_null($twid)) {
            $this->set_twid($twid);
        } elseif (!empty($_POST["twid"])) {
            $this->set_twid($_POST["twid"]);
        } elseif (!empty($_SESSION["twid"])) {
            $this->set_twid($_SESSION["twid"]);
        } elseif ($this->get_ean_exists()) {
            // OPTIMIZATION: query also asks for tw_nazwa and saves it in cache
            $query = "SELECT TOP(1)
                    tw_id,
                    tw_nazwa
                FROM tw__towar
                INNER JOIN te_ean
                ON tw_id = te_twid
                WHERE te_code = ?";
            $params = array($this->get_ean());
            $result = $this->db->fetch_all($query, $params);

            $this->set_twid($result[0]);
            $this->set_towar_nazwa($result[1]);
        }

        if (isset($this->twid)) {
            $_SESSION["twid"] = $this->get_twid();
        }
    }

    /**
     * Metoda odpowiada za probe wyodrebnienia prawidlowego kodu EAN z odebranego kodu i
     * przypisaniu przetworzonego kodu do kodu EAN danego obiektu
     *
     * @param string $code Kod do przetwarzania
     * @return string Przetworzony kod EAN, badz nierozpoznany kod
     */
    final private function extract_ean(string $code): string
    {
        $scaleCodes = [22, 29];
        $extractedEan = null;

        if (isset($_GET["ean"])) {
            $code = trim($_GET["ean"]);
        }

        $codeRegexLong = '/^01(\d{14})15(\d{6})10(\d{10})$/';
        $codeRegexScale = '/^(' . implode('|', $scaleCodes) . ')(\d{5})(\d{6})$/';
        $codeRegexEan13 = '/^(\d{2})(\d{5})(\d{5})(\d)$/';
        $codeRegexEan8 = '/^(\d{4})(\d{4})$/';
        $foundMatches = array();

        if (preg_match($codeRegexLong, $code) == 1) {
            $regex = '/(?<=01)(\d{14})(?=15)/';
            preg_match($regex, $code, $foundMatches, PREG_UNMATCHED_AS_NULL);

            $extractedEan = $foundMatches[1];
            if ("0" == $extractedEan[0] && strlen($extractedEan) > 13) {
                $extractedEan = substr($extractedEan, 1);
            }
        } elseif (preg_match($codeRegexScale, $code) == 1) {
            $regex = '/^(\d{2})(\d{5})(\d{6})$/';
            preg_match($regex, $code, $foundMatches, PREG_UNMATCHED_AS_NULL);

            $extractedEan = $foundMatches[1] . $foundMatches[2] . "000000";
        } elseif (preg_match($codeRegexEan13, $code) == 1) {
            $regex = '/^(\d{2})(\d{5})(\d{6})$/';
            preg_match($regex, $code, $foundMatches, PREG_UNMATCHED_AS_NULL);

            $extractedEan = $foundMatches[0];
        } elseif (preg_match($codeRegexEan8, $code) == 1) {
            $regex = '/^(\d{4})(\d{4})$/';
            preg_match($regex, $code, $foundMatches, PREG_UNMATCHED_AS_NULL);

            $extractedEan = $foundMatches[0];
        } else {
            $extractedEan = $code; // WARNING: Accepts any type of code as EAN! 
        }

        return $extractedEan;
    }

    /**
     * Metoda ustawia kod EAN w obiekcie
     * 
     * @param string $ean Kod EAN do przypisania w obiekcie
     */
    private function set_ean(string $ean): void
    {
        $this->ean = $ean;
    }

    /**
     * Metoda zwraca przechowywany w obiekcie kod EAN
     *
     * @return string Kod EAN
     */
    public function get_ean(): string
    {
        if (isset($this->ean)) {
            return $this->ean;
        } else {
            throw new \LogicException("Zmienna ean w obiekcie nie posiada przypisanej zadnej wartosci");
        }
    }

    /**
     * Metoda ustawia tw_id w obiekcie
     * 
     */
    private function set_twid(int $twid): void
    {
        $this->twid = $twid;
    }

    /**
     * Metoda zwraca przechowywany w obiekcie tw_id
     *
     * @return int Towar ID
     */
    public function get_twid(): int
    {
        if (isset($this->twid)) {
            return $this->twid;
        } else {
            throw new \LogicException("Zmienna twid w obiekcie nie posiada zadnej wartosci");
        }
    }

    /**
     * Metoda zwraca wartosc o ile powinna byc zwiekszana ilosc towaru,
     * zaleznie od typu jego jednostki miary (kg, szt, op)
     *
     * @param string|null $jm Opcjonalna nazwa jednostki miary
     * @return string 0.001 dla kg, 1 dla szt/op
     */
    public function get_step(string $jm = null): float
    {
        if (is_null($jm)) {
            $jm = $this->get_towar_jm();
        }

        if ($jm == "kg") {
            return self::WEIGHT_PRECISION;
        } else {
            return 1;
        }
    }

    /**
     * Metoda ustawia zmienna w sesji wskazujaca na istnienie kodu EAN w bazie danych
     *
     * @param boolean $exists Czy kod EAN istnieje
     * @return void
     */
    public function set_ean_exists(bool $exists): void
    {
        $_SESSION["ean_exists"] = $exists ? 1 : 0;
    }

    /**
     * Metoda sprawdzajaca czy kod EAN przechowywany w obiekcie istnieje w bazie danych
     *
     * @param boolean $forceCheck Wymusza sprawdzenie istnienia kodu EAN w bazie danych
     * @return boolean Wartosc logiczna wskazujaca na to, czy kod EAN przechowywany w obiekcie juz istnieje w bazie danych
     */
    public function get_ean_exists(bool $forceCheck = false): bool
    {
        if (!$forceCheck) {
            if ($this->is_without_ean()) {
                return false;
            } elseif (isset($_SESSION["ean_exists"])) {
                return (1 == $_SESSION["ean_exists"]);
            }
        }

        $query = "SELECT 1
            FROM te_ean
            WHERE te_code = ?";
        $params = array($this->get_ean());

        $result = $this->db->has_rows($query, $params);
        $this->set_ean_exists($result);

        return $result;
    }

    /**
     * Metoda ustawia zmienna w sesji wskazujaca na istnienie towaru w bazie danych
     *
     * @param boolean $exists Czy towar istnieje
     * @return void
     */
    public function set_towar_exists(bool $exists): void
    {
        $_SESSION["towar_exists"] = $exists ? 1 : 0;
    }

    /**
     * Metoda sprawdza czy tw_id przechowywane w obiekcie istnieje w bazie danych
     *
     * @return boolean Wartosc logiczna wskazujaca na to, czy towar o id przechowywanym w obiekcie juz istnieje w bazie danych
     */
    public function get_towar_exists(): bool
    {
        if (isset($_SESSION["towar_exists"])) {
            return ($_SESSION["towar_exists"] == 1);
        }

        $query = "SELECT 1
            FROM tw__towar
            WHERE tw_id = ?";
        $params = array($this->get_twid());

        $result = $this->db->has_rows($query, $params);
        $this->set_towar_exists($result);

        return $result;
    }

    /**
     * Metoda sprawdza czy EAN przechowywany w obiekcie oznacza towar bez kodu EAN
     *
     * @return boolean Wartosc logiczna wskazujaca na to, czy aktualny towar nie posiada kodu EAN
     */
    protected function is_without_ean(): bool
    {
        return ($this->get_ean() == self::NO_EAN_CODE);
    }

    /**
     * Metoda zwraca nazwe towaru przechowywanego w obiekcie, badz wyszukuje ja w bazie danych
     *
     * @return string Nazwa towaru przychowywanego w obiekcie
     */
    public function get_towar_nazwa(): string
    {
        if (!empty($_SESSION["nazwa"])) {
            return $_SESSION["nazwa"];
        }

        $query = "SELECT tw_nazwa
            FROM tw__towar
            WHERE tw_id = ?";

        $params = array($this->get_twid());
        $result = $this->db->fetch_first($query, $params);

        if (!empty($result)) {
            $this->set_towar_nazwa($result);
        }

        return $result;
    }

    /**
     * Metoda ustawia nazwe towaru przechowywanego w obiekcie
     *
     * @param string $nazwa Nazwa towaru
     * @return void
     */
    public function set_towar_nazwa(string $nazwa): void
    {
        $_SESSION["nazwa"] = $nazwa;
    }

    /**
     * Metoda zwraca jednostke miary towaru przechowywanego w obiekcie, lub wyszukuje ja w bazie danych
     *
     * @param bool $force Wymus sprawdzenie jednostki miary w bazie danych
     * @return string Nazwa jednostki miary
     */
    public function get_towar_jm(bool $force = false): string
    {
        if (!$force) {
            if (!empty($_SESSION["jm"])) {
                return $_SESSION["jm"];
            } elseif (!empty($_SESSION["jmid"])) {
                return $_COOKIE["sl_jm"][$_SESSION["jmid"]];
            }
        }

        $query = "SELECT jm_nazwa
            FROM sl_jm
            INNER JOIN tw__towar
            ON jm_id = tw_jmid
            WHERE tw_id = ?";

        $params = array($this->get_twid());
        $result = $this->db->fetch_first($query, $params);

        if (!is_null($result)) {
            $_SESSION["jm"] = $result;
        }

        return $result;
    }

    /**
     * Metoda ustawia jednostke miary towaru przechowywanego w obiekcie
     * 
     * @param string $jmid ID jednostki miary
     */
    public function set_towar_jmid(int $jmid): void
    {
        $_SESSION["jmid"] = $jmid;
    }

    /**
     * Metoda zwraca miejsce towaru przechowywanego w obiekcie
     *
     * @return string Nazwa miejsca towaru przychowywanego w obiekcie
     */
    public function get_towar_mj(): string
    {
        if (!empty($_SESSION["mj"])) {
            return $_SESSION["mj"];
        }

        $result = $_COOKIE["sl_mj"][$this->get_mjid()];

        if (!is_null($result)) {
            $_SESSION["mj"] = $result;
        }

        return $result;
    }

    /**
     * Metoda zwraca czas waznosci wszystkich powiazanych z uzytkownikiem ciasteczek.
     * Czas waznosci jest generowany przy logowaniu uzytkownika.
     *
     * @return integer Czas waznosci. (This is a Unix timestamp so is in number of seconds since the epoch.)
     */
    public function get_cookies_expiration_time(): int
    {
        $expireTime = -1;
        if (isset($_COOKIE["auth_expire"])) {
            $expireTime = $_COOKIE["auth_expire"];
        } else {
            self::clear_cookies();
            self::head_to();
        }

        return $expireTime;
    }

    /**
     * Metoda zwraca wersje pliku CSS
     *
     * @return string Wersja pliku CSS
     */
    public function get_resources_version(): int
    {
        return $this->resourcesVersion;
    }

    /**
     * Metoda ustawia tytul strony
     * 
     * @param string $title Tytul strony
     */
    public function set_title(string $title): void
    {
        $this->pageTitle = $title;
    }

    /**
     * Metoda zwraca tytul strony
     *
     * @return string Tytul strony
     */
    public function get_title(): string
    {
        return $this->pageTitle;
    }

    /**
     * Metoda zwraca przechowywana tablice jednostek miary jako select w HTML 
     *
     * @return string Select w HTML
     */
    public function display_jm_for_select(): string
    {
        $str = "";
        foreach ($_COOKIE["sl_jm"] as $id => $nazwa) {
            $selected = ($nazwa == "szt") ? " selected" : "";
            $str .= "<option value='{$id}'{$selected}>{$nazwa}</option>";
        }

        return $str;
    }

    /**
     * Metoda zwraca przechowywana tablice miejsc jako select w HTML 
     *
     * @return string Select w HTML
     */
    public function display_mj_for_select(): string
    {
        $str = "";
        foreach ($_COOKIE["sl_mj"] as $id => $nazwa) {
            $selected = ($id == $this->get_mjid()) ? "selected" : "";
            $str .= "<option value='{$id}' {$selected}>{$nazwa}</option>";
        }
        return $str;
    }

    /**
     * Metoda zwraca liste uzytkownikow jako select w HTML dla podstrony KierownikTowaryPoliczone.php
     *
     * @param int $mjid ID dzialu uzytkownika
     * @return string Select w HTML
     */
    public function display_uzytkownicy_for_select(int $mjid): string
    {
        $str = "";
        foreach ($this->get_all_uzytkownicy($mjid) as $user) {
            $str .= "<option value='{$user[0]}'>{$user[1]} {$user[2]}</option>\n";
        }

        return $str;
    }

    /**
     * Metoda zwraca liste kontrahentow jako select w HTML
     *
     * @param string $userInput Czesc nazwy wyszukiwanego kontrahenta
     * @return string Select w HTML
     */
    public function display_kontrahenci_for_select(string $userInput): string
    {
        $col = is_numeric($userInput) ? "kh_nip" : "kh_nazwa";

        $query = "SELECT
                kh_id,
                kh_nazwa
            FROM kh__kontrahent
            WHERE {$col} LIKE ?";

        $params = array("%{$userInput}%");

        $result = $this->db->query($query, $params);

        $str = "";
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $str .= "<option value='{$row["kh_id"]}'>{$row["kh_nazwa"]}</option>\n";
        }

        return $str;
    }

    /**
     * Metoda zwraca tablice jednostek miary z bazy danych
     *
     * @return array Tablica jednostek miary (jm_id => jm_nazwa)
     */
    public function get_all_jm(): array
    {
        $query = "SELECT
                jm_id,
                jm_nazwa
            FROM sl_jm";

        $result = $this->db->query($query);

        $jmData = null;
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $jmData[$row["jm_id"]] = $row["jm_nazwa"];
        }

        return $jmData;
    }

    /**
     * Metoda zwraca tablice miejsc z bazy danych
     *
     * @return array Tablica miejsc (mj_id => mj_nazwa)
     */
    public function get_all_mj(): array
    {
        $query = "SELECT
                mj_id,
                mj_nazwa
            FROM sl_mj";

        $result = $this->db->query($query);

        $mjData = null;
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            $mjData[$row["mj_id"]] = $row["mj_nazwa"];
        }

        return $mjData;
    }

    /**
     * Metoda pobiera liste wszystkich uzytkownikow przypisanych do miejsca o podanym id
     * a nastepnie zapisuje ich (id, imie, nazwisko) jako zakodowana tablica w ciasteczkach
     * o nazwie users_mjid_{id}
     *
     * @param integer $mjid ID miejsca
     * @return array Tablica uzytkownikow (id, imie, nazwisko)
     */
    public function get_all_uzytkownicy(int $mjid): array
    {
        $cookieName = "users_mjid_{$mjid}";

        if (!empty($_COOKIE[$cookieName])) {
            $decodedData = json_decode($_COOKIE[$cookieName]);
            $this->deep_to_Windows1250($decodedData);

            return $decodedData;
        }

        $query = "SELECT
                uz_id,
                uz_imie,
                uz_nazwisko
            FROM uz__uzytkownik
            WHERE uz_mjid = ?";

        $params = array($mjid);
        $result = $this->db->query($query, $params);

        $uzytkownicyData = array();
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            array_push($uzytkownicyData, array($row["uz_id"], $row["uz_imie"], $row["uz_nazwisko"]));
        }

        $this->deep_to_UTF8($uzytkownicyData);

        $encodedData = json_encode($uzytkownicyData);
        $this->set_cookie($cookieName, $encodedData);

        return $uzytkownicyData;
    }

    /**
     * Metoda ustawia ciasteczko o podanej nazwie i wartosci z domyslnym czasem wygasniecia
     * 
     * @param string $name Nazwa ciasteczka
     * @param string $value Wartosc ciasteczka
     * @param bool $for24h Czy ciasteczko ma wygasnac po 24h
     */
    protected function set_cookie(string $name, string $value, bool $for24h = false): void
    {
        $expireTime = $this->get_cookies_expiration_time();

        if ($for24h) {
            $expireTime = time() + (3600 * 24);
        }

        setcookie($name, $value, $expireTime, "/");
    }

    /**
     * Metoda zamienia enkodowanie wszystkich element�w string w tablicy na UTF-8
     *
     * @param array $items Referencja do tablicy z elementami string
     * @return void
     */
    protected function deep_to_UTF8(array &$items): void
    {
        foreach ($items as &$item) {
            if (is_array($item)) {
                $this->deep_to_UTF8($item);
            } elseif (is_string($item)) {
                $item = Encoding\Encoding::toUTF8($item);
            }
        }
    }

    /**
     * Metoda zamienia enkodowanie wszystkich element�w string w tablicy na Windows-1250/Windows-1252
     *
     * @param array $items Referencja do tablicy z elementami string
     * @return void
     */
    protected function deep_to_Windows1250(array &$items): void
    {
        foreach ($items as &$item) {
            if (is_array($item)) {
                $this->deep_to_Windows1250($item);
            } elseif (is_string($item)) {
                $item = Encoding\Encoding::toWin1252($item);
            }
        }
    }

    /**
     * Metoda zwraca rekordy z tabeli iw__inwentaryzacja o wybranych ID
     *
     * @param integer $iwid ID inwentaryzacji
     * @return array Tablica slownikowa gdzie kluczami sa nazwy kolumn
     */
    public function get_iw__inwentaryzacja_dane(int $iwid): array
    {
        $query = "SELECT *
            FROM iw__inwentaryzacja
            WHERE iw_id = ?";
        $params = array($iwid);

        return $this->db->fetch_all($query, $params);
    }

    /**
     * Metoda zwraca resource zapytania z danymi policzonego towaru dla danej inwentaryzacji
     *
     * @param integer $iwid ID inwentaryzacji
     * @param boolean $subtotal Wartosc logiczna wskazujaca na to, czy korzystac z sum czesciowych
     */
    public function get_towar_policzony_by_inwentaryzacja_resource(int $iwid, bool $subtotal)
    {
        if ($subtotal) {
            $query = "SELECT
                    MAX(tw_id) AS tw_id, 
                    MAX(tw_nazwa) AS tw_nazwa,
                    MAX(te_code) AS te_code, 
                    SUM(ti_ilosc) AS ti_ilosc, 
                    MAX(tw_jmid) AS tw_jmid,
                    MAX(ti_timestamp) AS ti_timestamp, 
                    MAX(CONCAT(uz_imie, ' ', uz_nazwisko)) AS imie_nazwisko 
                FROM ti_inwentaryzacja
                INNER JOIN tw__towar
                ON ti_twid = tw_id
                INNER JOIN uz__uzytkownik
                ON ti_uzid = uz_id
                OUTER APPLY (SELECT TOP(1) te_code AS te_code FROM te_ean WHERE te_twid = tw_id) AS te_code
                WHERE ti_iwid = ? 
                GROUP BY tw_nazwa
                ORDER BY ti_timestamp DESC";
        } else {
            $query = "SELECT 
                    tw_id,
                    tw_nazwa, 
                    (SELECT TOP(1) te_code FROM te_ean WHERE te_twid = tw_id) AS te_code, 
                    ti_ilosc,
                    tw_jmid,
                    ti_timestamp, 
                    CONCAT(uz_imie, ' ', uz_nazwisko) AS imie_nazwisko 
                FROM ti_inwentaryzacja
                INNER JOIN tw__towar
                ON ti_twid = tw_id
                INNER JOIN uz__uzytkownik
                ON ti_uzid = uz_id
                WHERE ti_iwid = ? 
                ORDER BY ti_timestamp DESC";
        }

        $params = array($iwid);
        $result = $this->db->query($query, $params);

        return $result;
    }

    /**
     * Metoda zwraca tablice ID dokumentow PZ do wybranej daty
     *
     * @param string $date Data do ktorej szukamy dokumentow (YYYY-MM-DD)
     * @param integer $ilosc Ilosc dokumentow do pobrania
     * @param integer $mjid ID miejsca
     * @return array Tablica ID dokumentow PZ
     */
    public function get_dokumenty_pz_do(string $date, int $ilosc, int $mjid): array
    {
        $query = "SELECT TOP(?)
                pz_id
            FROM pz__dokument
            WHERE pz_data <= ? AND pz_mjid = ?
            ORDER BY pz_data DESC";

        $params = array($ilosc, $date, $mjid);
        $result = $this->db->query($query, $params);

        $arrayPzId = array();
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            array_push($arrayPzId, $row["pz_id"]);
        }

        return $arrayPzId;
    }

    /**
     * Metoda zwraca resource z danymi o pozycjach przypisanymi do wybranego dokumentu PZ
     *
     * @param integer $pzid ID dokumentu PZ
     * @return resource Resource z danymi o pozycjach przypisanymi do wybranego dokumentu PZ
     */
    public function get_pozycje_pz_resource(int $pzid, $groupby = false)
    {
        $query = "";

        if ($groupby) {
            $query = "SELECT
                    tw_id, 
                    MAX(tw_nazwa) AS tw_nazwa, 
                    (SELECT TOP(1) te_code FROM te_ean WHERE te_twid = tw_id) AS te_code, 
                    MAX(tw_jmid) AS tw_jmid,
                    MAX(pz_nr) AS pz_nr,
                    MAX(pp_id) AS pp_id,
                    SUM(pp_ilosc) as pp_ilosc,
                    MAX(pp_cena) AS pp_cena
                FROM pz__dokument
                INNER JOIN pz_pozycja
                ON pz_id = pp_pzid
                INNER JOIN tw__towar
                ON pp_twid = tw_id
                WHERE pz_id = ?
                GROUP BY tw_id, pp_cena";
        } else {
            $query = "SELECT
                    tw_id, 
                    tw_nazwa, 
                    (SELECT TOP(1) te_code FROM te_ean WHERE te_twid = tw_id) AS te_code, 
                    tw_jmid,
                    pz_nr,
                    pp_id,
                    pp_ilosc,
                    pp_cena
                FROM pz__dokument
                INNER JOIN pz_pozycja
                ON pz_id = pp_pzid
                INNER JOIN tw__towar
                ON pp_twid = tw_id
                WHERE pz_id = ?";
        }

        $params = array($pzid);
        $result = $this->db->query($query, $params);

        return $result;
    }

    /**
     * Metoda zwraca tymczasowe ceny towarow z bazy danych
     * 
     * @param int $iwid ID inwentaryzacji
     * @return array Tablica z tymczasowymi cenami towarow
     */
    public function get_ic_ceny(int $iwid): array
    {
        $query = "SELECT
                ic_cena,
                ic_twid
            FROM ic_ceny
            WHERE ic_iwid = ?";
        $params = array($iwid);
        $result = $this->db->query($query, $params);

        $arrayResult = array();
        while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)) {
            array_push($arrayResult, $row);
        }

        return $arrayResult;
    }

    /**
     * Metoda wyszukuje towary w bazie danych na podstawie nazwy lub kodu EAN oraz
     * zwraca resource zapytania
     *
     * @param string $userInput Wyszukiwana nazwa lub kod EAN
     */
    protected function get_towary_resource(string $userInput)
    {
        $isEan = is_numeric($userInput);
        $userInput = iconv("UTF-8", "Windows-1250", $userInput);

        $params = null;
        $query = null;

        if ($isEan) {
            $query = "SELECT TOP(100)
                    tw_nazwa,
                    te_code,
                    tw_jmid,
                    tw_id
                FROM tw__towar
                INNER JOIN te_ean
                ON te_twid = tw_id
                WHERE te_code LIKE ?";
            $params = array("{$userInput}%");
        } else {
            $query = "SELECT TOP(100)
                    tw_nazwa,
                    te_code,
                    tw_jmid,
                    tw_id
                FROM tw__towar
                OUTER APPLY (SELECT TOP(1) te_code FROM te_ean WHERE te_twid = tw_id) AS te_code
                WHERE tw_nazwa LIKE ?";
            $params = array("%{$userInput}%");
        }

        $result = $this->db->query($query, $params);

        return $result;
    }

    /**
     * Metoda wstawia pozycje konkrentego dokumentu
     *
     * @param integer $pzid ID dokumentu
     * @param array $pozycje Tablica tablic, gdzie:
     * [0]: twid,
     * [1]: ilosc,
     * [2]: cena
     */
    public function insert_into_pz_pozycja(int $pzid, array $pozycje, bool $data = false)
    {
        $query = "";
        $params = array();

        $dataDodania = $data ? date(self::DATE_FORMAT) : null;
        foreach ($pozycje as $row) {
            $tmpTwid = $row[0];
            $ilosc = $row[1];
            $cena = $row[2];

            if (empty($ilosc) || empty($cena)) {
                continue;
            }

            $query .= "INSERT
                INTO pz_pozycja (pp_twid, pp_ilosc, pp_cena, pp_pzid, pp_data_dodania)
                VALUES (?, ?, ?, ?, ?);";

            array_push($params, $tmpTwid, $ilosc, $cena, $pzid, $dataDodania);
        }

        $stmt = $this->db->query($query, $params);

        return $stmt;
    }

    /**
     * Metoda zwraca pelne imie i nazwisko
     *
     * @param string|null $login Login uzytkownika
     * @return string Imie i nazwisko uzytkownika
     */
    public function get_full_name(string $login = ""): string
    {
        if (!empty($_COOKIE["imie_nazwisko"])) {
            return $_COOKIE["imie_nazwisko"];
        } elseif (empty($login) && !empty($_COOKIE["user_login"])) {
            $login = $_COOKIE["user_login"];
        }

        $query = "SELECT
                CONCAT(uz_imie, ' ', uz_nazwisko)
            FROM uz__uzytkownik
            WHERE uz_login = ?";

        $params = array($login);
        $result = $this->db->fetch_first($query, $params);

        if (!is_null($result)) {
            $this->set_cookie("imie_nazwisko", $result);
        }

        return $result;
    }

    /**
     * Metoda zwraca aktualnie przechowywane id miejsca przypisanego uzytkownikowi
     *
     * @param string|null $login Login uzytkownika
     * @return string ID miejsca przypisanego uzytkownikowi
     */
    public function get_mjid(string $login = null): string
    {
        if (!empty($_COOKIE["mjid"])) {
            return $_COOKIE["mjid"];
        }
        $login = $login ?? $_COOKIE["user_login"];

        $query = "SELECT uz_mjid
            FROM uz__uzytkownik
            WHERE uz_login = ?";

        $params = array($login);
        $result = $this->db->fetch_first($query, $params);

        if (!is_null($result)) {
            $this->set_cookie("mjid", $result);
        }

        return $result;
    }

    /**
     * Metoda zwraca aktualnie przechowywane id uzytkownika w obiekcie
     *
     * @param string|null $login Login uzytkownika
     * @return string ID uzytkownika
     */
    public function get_uzid(string $login = null): string
    {
        if (!empty($_COOKIE["uzid"])) {
            return $_COOKIE["uzid"];
        }
        $login = $login ?? $_COOKIE["user_login"];

        $query = "SELECT uz_id
            FROM uz__uzytkownik
            WHERE uz_login = ?";

        $params = array($login);
        $result = $this->db->fetch_first($query, $params);

        if (!is_null($result)) {
            $this->set_cookie("uzid", $result);
        }

        return $result;
    }

    /**
     * Metoda zwracajaca wartosc logiczna, wskazujaca na to czy uzytkownik jest kierownikiem
     *
     * @param string|null $login Login uzytkownika
     * @return string Wartosc logiczna wskazujaca na to czy uzytkownik jest kierownikiem
     */
    public function is_manager(string $login = null): bool
    {
        if (!empty($_COOKIE["is_manager"])) {
            return (1 == $_COOKIE["is_manager"]);
        }
        $login = $login ?? $_COOKIE["user_login"];

        $query = "SELECT uz_kierownik
            FROM uz__uzytkownik
            WHERE uz_login = ?";

        $params = array($login);
        $result = $this->db->fetch_first($query, $params);

        $this->set_cookie("is_manager", $result);

        return ("1" == $result);
    }

    /**
     * Metoda sprawdza czy uzytkownik jest zalogowany
     *
     * @return boolean Czy jest ustawione ciasteczko 'user_login'
     */
    public function is_logged_in(): bool
    {
        return isset($_COOKIE["user_login"]);
    }

    /**
     * Metoda zwraca obiekty klas, ktore sa podstronami menu glownego uzytkownika
     *
     * @return array Tablica obiektow klas (nazwa_pliku => obiekt)
     */
    private function get_main_menu_subpages(): array
    {
        // Wczytanie podklas AbstractPageUzytkownik, ktore powinny byc widoczne w menu glownym
        {
            $abortDisplaying = true;
            include_once "TowarNowy.php";
            include_once "TowarLista.php";
            include_once "TowarPoliczony.php";
            unset($abortDisplaying);
        }

        $abstractClass = new \ReflectionClass("Inwentaryzacja\Pages\Parent\AbstractPageUzytkownik");
        $arrayObjects = array();

        foreach (get_declared_classes() as $className) {
            $reflectionClass = new \ReflectionClass($className);
            if ($reflectionClass->isSubclassOf($abstractClass) && !$reflectionClass->isAbstract()) {

                $uzytkownikPage = new $className(true);

                if ($uzytkownikPage->get_show_in_main_menu()) {
                    $fileName = basename($reflectionClass->getFileName());
                    $arrayObjects[$fileName] = $uzytkownikPage;
                }
            }
        }

        $titles = array();
        foreach ($arrayObjects as $key => $row) {
            $titles[$key] = $row->get_main_menu_position();
        }
        array_multisort($titles, SORT_ASC, $arrayObjects);

        return $arrayObjects;
    }

    /**
     * Metoda zwraca kod HTML menu dla wybranych podstron
     *
     * @param array $pages Tablica obiektow podklas AbstractPageUzytkownik (nazwa_pliku => obiekt)
     * @return string Kod HTML menu 
     */
    protected function get_menu(array $pages): string
    {
        $menu = "";
        foreach ($pages as $filename => $page) {
            $menu .= trim("
                <button type='button' onclick=\"window.location.href='{$filename}'\" class='menuItem'>
                    <table style='margin: 0; table-layout: auto;'>
                        <tr>
                            <td style='padding: 0;' class='bigger-icon' width='8px'>
                                <i class='{$page->get_icon()}' style='display: inline-block; vertical-align: middle; padding: 8px;'></i>
                            </td>
                            <td style='padding: 0;'>{$page->get_title()}</td>
                        </tr>
                    </table>
                </button>");
        }

        return $menu;
    }

    /**
     * Metoda zwraca kod HTML menu glownego
     *
     * @return string Kod HTML menu glownego
     */
    public function get_main_menu(): string
    {
        return $this->get_menu($this->get_main_menu_subpages());
    }

    /**
     * Metoda wyswietla element graficzny zawierajacy detale ostatniej wprowadzonej pozycji w inwentaryzacji
     *
     * @return string Element div wyswietlajacy przechowywane w ciasteczkach informacje o ostatnim policzonym towarze
     */
    public function display_last_inwentaryzacja(): string
    {
        $lastInwentaryzacja = "";
        if (isset($_COOKIE["last_towar_nazwa"]) && isset($_COOKIE["last_towar_ilosc"]) && isset($_COOKIE["last_towar_jm"])) {
            $towarNazwa = $_COOKIE["last_towar_nazwa"];
            $towarIlosc = $_COOKIE["last_towar_ilosc"];
            $towarJm = $_COOKIE['last_towar_jm'];

            $lastInwentaryzacja = "
                <div class='notification-last'>
                    <div style='text-align:center; padding: 3px;'><strong>Ostatnia wprowadzona pozycja</strong></div>
                    <table width='100%'>
                        <tr>
                            <td width='80%' style='text-align: left'>{$towarNazwa}</td>
                            <td width='20%' style='text-align: right'>{$towarIlosc} {$towarJm}</td>
                        </tr>
                    </table>
                </div>";
        }

        return trim($lastInwentaryzacja);
    }

    public function try_print_ean_tr(): string
    {
        $trEan = "";

        if ($this->get_ean_exists()) {
            $trEan = "
                <tr>
                    <td><strong>EAN</strong></td>
                    <td>{$this->get_ean()}</td>
                </tr>";
        }

        return $trEan;
    }

    /**
     * Metoda wyswietla standardowy panel tytulowy wraz z odpowiednia ikonka i przyciskiem menu, strony glownej
     * oraz dodaje domyslnie ukryty element menu.
     *
     * @param string $iconClass Nazwa klasy ikonki
     * @param bool $showMenu Czy wyswietlac przycisk menu
     * @param bool @homeButton Czy wyswietlac przycisk powrotu do strony glownej
     * @return string Wygenerowany kod HTML ukrytego menu oraz panelu tytulowego
     */
    public function display_title_bar(bool $showMenu = true, bool $backButton = false): string
    {
        $menu = "";

        $optionalBackButton = "";
        $optionalMenuButton = "";
        $optionalScripts = "";
        $optionalButtons = "";

        if ($showMenu) {
            $menu = $this->display_menu();
            $optionalMenuButton = "
                <span style='float: right; max-width: 20%;'>
                    <h2 style='margin-top: 12px;'>
                        <button style='padding: 4px 10px; margin: 0;' class='hamburger'>
                            <i class='iconoir-menu' style='display: inline-block; vertical-align: middle; padding-bottom:4px;'></i>
                        </button>
                    </h2>
                </span>";
            $optionalScripts = "<script src='../../js/menu.js?v={$this->get_resources_version()}'></script>";
        }

        if ($backButton) {
            $optionalBackButton = "
                <span style='float: right; max-width: 20%; margin-right: 0.5em;'>
                    <h2 style='margin-top: 12px;'>
                        <a href='KierownikPanel.php'>
                            <button style='padding: 4px 10px; margin: 0;'>
                                <i class='iconoir-arrow-left' style='display: inline-block; vertical-align: middle; padding-bottom:4px;'></i>
                            </button>
                        </a>
                    </h2>
                </span>";
        }

        if ($showMenu || $backButton) {
            $optionalButtons = trim("
            <span class='buttons'>
                {$optionalMenuButton}
                {$optionalBackButton}
            </span>");
        }

        $str = "
            {$menu}
            <caption>
                <span>
                    <h2 style='display: inline-block; max-width: 80%; margin-top: 16px;'>
                        <i class='{$this->pageIcon}' style='display: inline-block; vertical-align: middle; margin-right: 4px;'></i> {$this->pageTitle}
                    </h2>
                </span>
                {$optionalButtons}
            </caption>
            {$optionalScripts}";

        return trim($str);
    }

    /**
     * Metoda odpowiada za umieszczanie domyslnie ukrytych elementow menu na stronie
     *
     * @return string Wygenerowany kod HTML menu
     */
    private function display_menu(): string
    {
        $jsConstraint = "if(!window.matchMedia('only screen and (max-width: 760px)').matches){window.location.href='KierownikPanel.php'}";

        $btnPanelKierownika = !($this->is_manager()) ? "" : trim("
                <button type='button' onclick=\"{$jsConstraint}\" class='menuItem' style='padding-top: 6px; padding-bottom: 6px;'>
                    <table style='margin: 0; table-layout: auto;'>
                        <tr>
                            <td style='padding: 0;' class='bigger-icon' width='8px'>
                                <i class='iconoir-user-circle-alt' style='display: inline-block; vertical-align: middle; padding: 8px;'></i>
                            </td>
                            <td style='padding: 0;'>
                                <span class='standout-text bold'>Panel kierownika</span>
                            </td>
                        </tr>
                    </table>
                </button>");

        $menu = "
            <div class='menu'>
                <ul style='height:60%; margin-top: 15vh'>
                    {$btnPanelKierownika}
                    {$this->get_main_menu()}

                    <button type='button' onclick=\"logout()\" style='margin-top: 10%;'>
                        <span class='bold'>Wyloguj si�</span>
                    </button>
                </ul>
            </div>";

        return trim($menu);
    }

    /**
     * Metoda odpowiada za umieszczanie stopki na stronie z informacjami o wersji aplikacji oraz zalogowanym uzytkowniku
     *
     * @return string Wygenerowany kod HTML stopki
     */
    public function display_statusbar(): string
    {
        $version = self::APP_VERSION;

        $statusBar = "
            <br/>
            <div class='statusbar'>
                <table style='width: 100%;'>
                    <tr>
                        <td style='width: 50%; text-align: left;'>
                            <p>Zalogowany u�ytkownik: <strong>{$this->get_full_name()}</strong></p>
                        </td>
                        <td style='width: 50%; text-align: right;'>
                            <p>Inwentaryzacja <strong>{$version}</strong></p>
                        </td>
                    </tr>
                </table>
            </div>";

        return trim($statusBar);
    }

    /**
     * Metoda odpowiada za umieszczanie tagu head na stronie z informacjami o tytule, kodowaniu znakow, viewportie oraz stylach
     * 
     * @param string $title Tytul strony
     * @return string Wygenerowany kod HTML tagu head
     */
    public function display_head(): string
    {
        $title = self::APP_NAME . " - " . strip_tags($this->pageTitle);

        $head = "
            <!DOCTYPE html>
            <html lang='pl-PL'>
            
            <head>
                <title>{$title}</title>
                <meta charset='Windows-1250'>
                <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no' />
                <script src='../../js/sweetalert2@11.js'></script>
            
                <link rel='stylesheet' href='../../css/dark.css?v={$this->get_resources_version()}'>
                <link rel='stylesheet' href='../../css/iconoir.css?v={$this->get_resources_version()}'>
                <link rel='stylesheet' href='../../css/main.css?v={$this->get_resources_version()}'>

                <noscript>
                    <style>body { display: none; }</style>
                </noscript>
            </head>

            <noscript>
                <h3 style='display: block;'>Strona wymaga w��czonego JavaScriptu.</h3>
            </noscript>
            ";

        return trim($head);
    }
}