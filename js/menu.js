const menu = document.querySelector(".menu");
const hamburger = document.querySelector(".hamburger");
const hamburgerIcon = document.querySelector(".iconoir-menu");

function toggleMenu() {
    if (menu.classList.contains("showMenu")) {
        menu.classList.remove("showMenu");

        hamburgerIcon.classList.add("iconoir-menu");
        hamburgerIcon.classList.remove("iconoir-cancel");
        enableScroll();
    } else {
        menu.classList.add("showMenu");

        hamburgerIcon.classList.add("iconoir-cancel");
        hamburgerIcon.classList.remove("iconoir-menu")
        disableScroll();
    }
}

function disableScroll() {
    // Get the current page scroll position
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    let scrollLeft = window.pageXOffset || document.documentElement.scrollLeft;

    // if any scroll is attempted, set this to the previous value
    window.onscroll = function () {
        window.scrollTo(scrollLeft, scrollTop);
    };
}

function enableScroll() {
    window.onscroll = function () { };
}

function logout() {
    const data = {
        method: "handle_clear_cookies",
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }

    }).then((response) => { if (response.ok) { window.location.href = '../../index.php' } });
}


hamburger.addEventListener("click", toggleMenu);