"use strict";
class Context {
    constructor(target_selector, contents = []) {
        const style = document.createElement("style");
        style.textContent = `
:root {
    --text_color: white;
    --background_color: rgba(22, 31, 39, 0.9);
    --corner_radius: 4px;
    --font-family: sans-serif;
}

.modern_context_js_outer {
    background: var(--background_color);
    position: absolute;
    border-radius: var(--corner_radius);
    border: 1px solid rgba(255, 255, 255, 0.2);
    box-shadow: 0.1em 0.1em 0.5em rgba(0, 0, 0, 0.5);
    padding: 0.25em;
    display: none;
    overflow: hidden;
    transition: 0.3s cubic-bezier(0.5, 0, 0, 1);
    cursor: default;
    user-select: none;
    backdrop-filter: blur(0.25em);
    font-family: var(--font-family);
    z-index: 99;
}

.modern_context_js_outer hr {
    height: 0;
    background: var(--text_color);
    border: none;
    margin: 0.25em 1em;
    opacity: 0.99;
}

.modern_context_js_outer .context_item {
    font-size: 0.9em;
    width: 100%;
    padding: 0.25em 0.5em;
    color: var(--text_color);
    box-sizing: border-box;
    position: relative;
    cursor: pointer;
}

.modern_context_js_outer .context_item i {
    margin-right: 0.5em;
}

.modern_context_js_outer .context_item::before {
    content: "";
    display: block;
    width: 100%;
    height: 100%;
    transition: 0.1s;
    position: absolute;
    border-radius: var(--corner_radius);
    top: 0;
    left: 0;
    background: var(--text_color);
    opacity: 0;
}

.modern_context_js_outer .context_item.hover::before {
    opacity: 0.15;
}

.modern_context_js_outer .context_item .context_item_inner {
    transition: 0.1s ease-out; 
}

.modern_context_js_outer .context_item:active .context_item_inner {
    transform: scale(0.95);
}
        `;
        document.body.appendChild(style);
        this.context = document.createElement("div");
        this.context.className = "modern_context_js_outer";
        document.body.appendChild(this.context);
        this.add_contents(contents);
        document.querySelectorAll(target_selector).forEach((target) => {
            target.addEventListener("contextmenu", (event) => {
                this.open(event);
                event.preventDefault();
            });
        });
        document.addEventListener("click", (event) => {
            if (event.target !== this.context)
                this.close();
        }, false);
        // detect right mouse button click and close context menu
        document.addEventListener("mousedown", (event) => {
            if (event.button === 2)
                this.close();
        }, false);
        // detect scroll and close context menu
        document.addEventListener("scroll", () => {
            this.close();
        }, false);
        // detect scroll in div with id table-towary
        const container = document.getElementsByClassName("table-container");
        if (container[0]) {
            container[0].addEventListener("scroll", () => {
                this.close();
            }, false);
        }
        // detect scroll in container and close context menu
        document.querySelectorAll(".scrollable").forEach((element) => {
            element.addEventListener("scroll", () => {
                this.close();
            }, false);
        });

        document.addEventListener("keydown", this._watch_keydown.bind(this), false);
        this.is_visible = false;

    }
    add_item(icon, label, callback = () => { }) {
        const item = document.createElement("div");
        item.className = "context_item";
        item.addEventListener("click", () => {
            callback();
        });
        item.addEventListener("mouseover", () => {
            this._hover(item);
        });
        item.addEventListener("mouseleave", () => {
            this._reset_all_hover_status();
        });
        const inner = document.createElement("div");
        inner.className = "context_item_inner";
        if (icon) {
            inner.innerHTML = icon;
        }
        if (label) {
            inner.innerHTML += label;
        }
        // inner.textContent = label;
        item.appendChild(inner);
        this.context.appendChild(item);
    }
    add_separator() {
        this.context.appendChild(document.createElement("hr"));
    }
    add_contents(contents) {
        for (let i = 0; i < contents.length; i++) {
            const content = contents[i];
            const types = ["item", "separator"];
            if (types.includes(content.type) === false)
                continue;
            switch (content.type) {
                case "item":
                    const item = {
                        ...{
                            icon: "",
                            label: "",
                            callback: () => { }
                        },
                        ...content
                    };
                    if (item.callback)
                        this.add_item(item.icon, item.label, item.callback);
                    else
                        this.add_item(item.icon, item.label);
                    break;
                case "separator":
                    this.add_separator();
                    break;
            }
        }
    }
    open(event) {
        const context_show_transition_ms = 300;
        this.context.style.transition = "none";
        if (event.screenY < window.innerHeight / 2) {
            this.context.style.bottom = "auto";
            this.context.style.top = `${event.pageY}px`;
        }
        else {
            this.context.style.top = "auto";
            this.context.style.bottom = `${window.innerHeight - event.pageY}px`;
        }
        if (event.screenX < window.innerWidth / 2) {
            this.context.style.right = "auto";
            this.context.style.left = `${event.pageX}px`;
        }
        else {
            this.context.style.left = "auto";
            this.context.style.right = `${window.innerWidth - event.pageX}px`;
        }
        this.context.style.display = "block";
        const context_height = window.getComputedStyle(this.context).getPropertyValue("height");
        this.context.style.height = "0";
        this.context.style.transition = `${context_show_transition_ms}ms`;
        setTimeout(() => {
            this.context.style.height = `${context_height}`;
            setTimeout(() => {
                this.context.style.height = "auto";
            }, context_show_transition_ms);
        }, 1);
        this.is_visible = true;
    }
    close() {
        this.context.style.display = "none";
        this._reset_all_hover_status();
        this.is_visible = false;
    }
    _watch_keydown(key_event) {
        if (this.is_visible === false)
            return;
        const current_selected_item = this.context.querySelector(".context_item.hover") || this.context.querySelector(".context_item");
        const number_of_items = this.context.querySelectorAll(".context_item").length;
        const hovered_item_index = this._hovered_item_index();
        switch (key_event.key) {
            case "Escape":
                const div = document.createElement("div");
                div.style.display = "none";
                document.body.appendChild(div);
                div.click();
                div.remove();
                break;
            case "ArrowDown":
                if (hovered_item_index === null)
                    this._hover(0);
                else
                    this._hover(hovered_item_index + 1 < number_of_items ? hovered_item_index + 1 : 0);
                break;
            case "ArrowUp":
                if (hovered_item_index === null)
                    this._hover(number_of_items - 1);
                else
                    this._hover(hovered_item_index - 1 >= 0 ? hovered_item_index - 1 : number_of_items - 1);
                break;
            case "Enter":
                current_selected_item.click();
                break;
        }
        key_event.preventDefault();
    }
    _reset_all_hover_status() {
        this.context.querySelectorAll(".context_item.hover").forEach((element) => {
            element.classList.remove("hover");
        });
    }
    _hover(item) {
        this._reset_all_hover_status();
        if (typeof (item) == "number") {
            this.context.querySelectorAll(".context_item").item(item).classList.add("hover");
        }
        else if (typeof (item) === "object") {
            item.classList.add("hover");
        }
    }
    _hovered_item_index() {
        const hovered_item = this.context.querySelector(".context_item.hover");
        const context_items = this.context.querySelectorAll(".context_item");
        if (!hovered_item) {
            return null;
        }
        for (let i = 0; i < context_items.length; i++) {
            if (hovered_item === context_items[i])
                return i;
        }
        return null;
    }
}
