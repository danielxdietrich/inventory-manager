const inputDataOd1 = document.getElementById('input-data-od1');
const inputDataDo1 = document.getElementById('input-data-do1');
const inputInwentaryzacjaNr = document.getElementById('input-inwentaryzacja-nr');
const inputSumyCzesciowe = document.getElementById('input-sumy-czesciowe');
const inputIwid = document.getElementById('input-iwid');
const inputMjid = document.getElementById('input-mjid');

const divRestrict = document.getElementById('div-restrict');
const divRestrict2 = document.getElementById('div-restrict-2');
const selectDzial1 = document.getElementById('select-dzial1');
const spanInwentaryzacjaTitle = document.getElementById('span-inwentaryzacja-title');
const spanListaInwentaryzacji = document.getElementById('span-lista-inwentaryzacji');
const buttonDrukowanie = document.getElementById('button-drukowanie');

const tbodyInwentaryzacje = document.getElementById('tbody-inwentaryzacje');
const tbodyPoliczone = document.getElementById('tbody-policzone');

let iwid = null;
let idnr = null;
let iwdata = null;

displayInwentaryzacje();

function updateRestriction() {
    if (tbodyInwentaryzacje.innerHTML == '') {
        divRestrict.classList.add('disabled-zone');
    } else {
        divRestrict.classList.remove('disabled-zone');
    }
}

function displayInwentaryzacje() {
    const data = {
        method: "handle_display_inwentaryzacje_for_table",
        inw_nr: inputInwentaryzacjaNr.value,
        mj_id: selectDzial1.value,
        data_od: inputDataOd1.value,
        data_do: inputDataDo1.value,
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => tbodyInwentaryzacje.innerHTML = newInnerHTML)
        .then(updateRestriction)
        .then(() => {
            spanInwentaryzacjaTitle.innerHTML = '';
            spanListaInwentaryzacji.innerHTML = ` &rarr; ${selectDzial1.options[selectDzial1.selectedIndex].text}`;
            tbodyPoliczone.innerHTML = '';
            buttonDrukowanie.disabled = true;
            divRestrict2.classList.add('disabled-zone');
        });
}

function subtotalClick() {
    if (this.iwid != null) {
        displayTowarPoliczonyByInwentaryzacja(this.iwid, this.iwnr, this.iwdata);
    }
}

function displayTowarPoliczonyByInwentaryzacja(thatIwid, thatIwnr, thatIwdata) {
    spanInwentaryzacjaTitle.innerHTML = `&rarr; <span class='standout-text'>${selectDzial1.options[selectDzial1.selectedIndex].text}, ${thatIwnr}</span>`;
    buttonDrukowanie.disabled = false;
    divRestrict2.classList.remove('disabled-zone');
    inputIwid.value = thatIwid;
    inputMjid.value = selectDzial1.value;

    this.iwid = thatIwid;
    this.iwnr = thatIwnr;
    this.iwdata = thatIwdata;

    const trs = document.querySelectorAll('tr[id^="tr-"]');
    trs.forEach(tr => {
        if (tr.id == `tr-${thatIwid}`) {
            tr.classList.add('selected-row');
        } else {
            tr.classList.remove('selected-row');
        }
    });

    const data = {
        method: "handle_display_towar_policzony_by_inwentaryzacja_for_table",
        iw_id: thatIwid,
        subtotal: inputSumyCzesciowe.checked
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => tbodyPoliczone.innerHTML = newInnerHTML);
}
