const inputEanNazwa = document.getElementById('input-ean-nazwa');
const inputNazwa = document.getElementById('nazwa');
const inputEan = document.getElementById("ean");
const inputJm = document.getElementById("jm");
const inputTwid = document.getElementById("twid");
const inputSubmit = document.getElementById("submit-input");

const tbodyElement = document.getElementById('tbody-element');

function displayTowaryForTable() {
    const userInput = inputEanNazwa.value;

    if (userInput.length >= 3) {
        const data = {
            method: "handle_display_towary_for_table",
            userInput: userInput,
        }

        fetch("../InwentaryzacjaAPI.php", {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json; charset=Windows-1250;"
            }
        })
            .then((response) => response.json())
            .then((newInnerHTML) => tbodyElement.innerHTML = newInnerHTML);
    } else {
        tbodyElement.innerHTML = '';
    }
}

function select(nazwa, ean, jm, twid) {
    inputNazwa.value = nazwa;
    inputEan.value = ean;
    inputJm.value = jm;
    inputTwid.value = twid;

    document.getElementById("myForm").submit();
}