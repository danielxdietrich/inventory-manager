const inputIlosc = document.getElementById('input-ilosc');
const inputJm = document.getElementById('jm');
const inputEan = document.getElementById('ean');
const inputCheckboxEan = document.getElementById('input-ean');

function confirmSubmit() {
    if (!isEmpty(inputIlosc.value) && parseFloat(inputIlosc.value) > 0.001) {
        document.getElementById("myForm").submit();
    }
}

function isEmpty(str) {
    return !str.trim().length;
}
