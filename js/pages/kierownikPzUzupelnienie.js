const iloscPozycji = document.getElementById('ilosc-pozycji');
const spanSuma = document.getElementById('span-suma');
const spanIwid = document.getElementById('span-iwid');

let localStorageArray = JSON.parse(localStorage.getItem(spanIwid.innerText)) || [];
let localStorageDisabledArray = JSON.parse(localStorage.getItem(spanIwid.innerText + '-disabled')) || [];

// tryLoadCenyFromLocalStorage();
tryLoadDisabledRowsFromLocalStorage();
updateSuma();

function updateWartosc(pz_pozycja) {
    const trPoz = document.getElementById('poz-' + pz_pozycja);

    const tdIlosc = document.getElementById(`td-ilosc-${pz_pozycja}`);
    const tdCena = document.getElementById(`td-cena-${pz_pozycja}`);
    const inputCena = document.getElementById(`input-cena-${pz_pozycja}`);
    const tdWartosc = document.getElementById(`td-wartosc-${pz_pozycja}`);

    const inputZl = document.getElementById(`input-zl-${pz_pozycja}`);
    const inputGr = document.getElementById(`input-gr-${pz_pozycja}`);
    const inputIlosc = document.getElementById(`input-ilosc-${pz_pozycja}`);

    if (inputCena) {
        if (inputCena.value != '') {
            tdWartosc.innerHTML = (inputCena.value * inputIlosc.value).toFixed(2).replace(/,/g, '');

            inputZl.value = Math.floor(inputCena.value) || 0;
            inputGr.value = Math.round((inputCena.value - inputZl.value) * 100) || 0;
        } else {
            tdWartosc.innerHTML = '0.00';
            inputZl.value = '0';
            inputGr.value = '0';
        }

        if (inputCena.value > 0) {
            inputCena.style.backgroundColor = 'var(--next-border-color)';
        } else {
            inputCena.style.backgroundColor = 'var(--error-bg-color)';
        }
        // trySaveCenaToLocalStorage(pz_pozycja);
    } else if (tdCena) {
        tdWartosc.innerHTML = (tdCena.innerText * tdIlosc.innerText).toFixed(2).replace(/,/g, '');
        inputZl.value = tdCena.innerText.split('.')[0] || 0;
        inputGr.value = tdCena.innerText.split('.')[1] || 0;
    }

    updateSuma();
}

function savePriceTemp(pz_pozycja) {
    const cena = document.getElementById(`input-cena-${pz_pozycja}`).value;
    const twid = document.getElementById(`input-twid-${pz_pozycja}`).value;
    const iwid = spanIwid.innerText;

    const data = {
        method: "handle_update_or_insert_to_ic_ceny",
        twid: twid,
        iwid: iwid,
        cena: cena
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then(() => {

        });
}

function updateSuma() {
    const rows = document.querySelectorAll("#table-towary tr:not(.disabled)");

    let subTotal = Array.from(rows).slice(1, -1).reduce((total, row) => {
        if (row.cells[6].innerHTML.includes(',')) {
            row.cells[6].innerHTML = row.cells[6].innerHTML.replace(/,/g, '');
        }

        return total + parseFloat(row.cells[6].innerHTML);
    }, 0);

    spanSuma.innerHTML = `${subTotal.toFixed(2)} z�`;
}

function updateNazwa(pz_pozycja) {
    const twid = document.getElementById(`input-twid-${pz_pozycja}`).value;
    const nazwa = document.getElementById(`input-nazwa-${pz_pozycja}`).value;
    const tdNazwaClass = `td-nazwa-twid-${twid}`;
    const inputNazwaClass = `input-nazwa-twid-${twid}`;

    const tdNazwa = document.getElementsByClassName(tdNazwaClass);
    const inputNazwa = document.getElementsByClassName(inputNazwaClass);

    Swal.fire({
        title: "Zmie� nazw�",
        input: 'text',
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonColor: '#1ed671',
        confirmButtonText: 'Zmie�',
        cancelButtonText: 'Anuluj',
        inputValue: nazwa,
        buttonsStyling: false,
        customClass: {
            confirmButton: 'btn-next',
        },
        inputValidator: (value) => {
            if (!value) {
                return 'Pole nie mo�e by� puste!'
            }
        }
    }).then((result) => {
        if (result.isConfirmed) {
            const data = {
                method: "handle_update_nazwa_in_tw__towar",
                twid: twid,
                nazwa: result.value
            }

            fetch("../InwentaryzacjaAPI.php", {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json; charset=Windows-1250;"
                }
            })
                .then(() => {
                    for (let i = 0; i < tdNazwa.length; i++) {
                        tdNazwa[i].innerHTML = result.value;
                        inputNazwa[i].value = result.value;
                    }
                });
        }
    });
}

function updateCena(pz_pozycja) {
    const twid = document.getElementById(`input-twid-${pz_pozycja}`).value;
    const ppid = document.getElementById(`input-ppid-${pz_pozycja}`).value;
    const nazwa = document.getElementById(`input-nazwa-${pz_pozycja}`).value;

    const inputZl = document.getElementById(`input-zl-${pz_pozycja}`);
    const inputGr = document.getElementById(`input-gr-${pz_pozycja}`);
    const inputDodaneRecznie = document.getElementById(`input-dodane-recznie-${pz_pozycja}`);
    const tdCena = document.getElementById(`td-cena-${pz_pozycja}`);

    Swal.fire({
        title: "Zmie� cen�",
        text: `Modyfikuje cen� w powi�zanym dokumencie PZ`,
        input: 'text',
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonColor: '#1ed671',
        confirmButtonText: 'Zmie�',
        cancelButtonText: 'Anuluj',
        inputValue: parseFloat(tdCena.innerText).toFixed(2),
        buttonsStyling: false,
        customClass: {
            confirmButton: 'btn-next',
        },
        inputValidator: (value) => {
            if (!value) {
                return 'Pole nie mo�e by� puste!'
            } else if (isNaN(value)) {
                return 'Pole musi by� liczb�!'
            }
        }
    }).then((result) => {
        if (result.isConfirmed) {
            const data = {
                method: "handle_update_cena_in_pz_pozycja",
                ppid: ppid,
                cena: result.value,
            }

            fetch("../InwentaryzacjaAPI.php", {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json; charset=Windows-1250;"
                }
            })
                .then(() => {
                    inputDodaneRecznie.value = 1;
                    tdCena.innerHTML = parseFloat(result.value).toFixed(2);

                    if (result.value.includes('.')) {
                        inputZl.value = result.value.split('.')[0];
                        inputGr.value = result.value.split('.')[1];
                    } else {
                        inputZl.value = result.value;
                        inputGr.value = '00';
                    }

                    updateWartosc(pz_pozycja);
                });
        }
    });
}

function updateJm(pz_pozycja) {
    const twid = document.getElementById(`input-twid-${pz_pozycja}`).value;
    const jmid = document.getElementById(`input-jmid-${pz_pozycja}`).value;
    const nazwa = document.getElementById(`input-nazwa-${pz_pozycja}`).value;

    const tdJmClass = `td-jm-twid-${twid}`;
    const inputJmidClass = `input-jmid-twid-${twid}`;

    const tdJm = document.getElementsByClassName(tdJmClass);
    const inputJmid = document.getElementsByClassName(inputJmidClass);

    Swal.fire({
        title: "Zmie� jednostk� miary",
        text: nazwa,
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonColor: '#1ed671',
        confirmButtonText: 'Zmie�',
        cancelButtonText: 'Anuluj',
        inputValue: jmid,
        input: 'select',
        inputOptions: {
            '1': 'KG',
            '2': 'SZT',
            '3': 'OP'
        },
        buttonsStyling: false,
        customClass: {
            confirmButton: 'btn-next',
        },
    }).then((result) => {
        if (result.isConfirmed) {
            const data = {
                method: "handle_update_jmid_in_tw__towar",
                twid: twid,
                jm: result.value
            }

            fetch("../InwentaryzacjaAPI.php", {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json; charset=Windows-1250;"
                }
            })
                .then(() => {
                    let jm = '';

                    switch (result.value) {
                        case '1':
                            jm = 'kg';
                            break;
                        case '2':
                            jm = 'szt';
                            break;
                        case '3':
                            jm = 'op';
                            break;
                    }

                    for (let i = 0; i < tdJm.length; i++) {
                        tdJm[i].innerHTML = jm;
                        inputJmid[i].value = result.value;
                    }
                });
        }
    });
}

// function changeIlosc(pz_pozycja) {
//     const tdIlosc = document.getElementById('td-ilosc-' + pz_pozycja);

//     const inputIlosc = document.getElementById('input-ilosc-' + pz_pozycja);
//     const inputNazwa = document.getElementById('input-nazwa-' + pz_pozycja);

//     const iloscOld = inputIlosc.value;
//     let iloscNew = inputIlosc.value;

//     Swal.fire({
//         title: inputNazwa.value,
//         input: 'number',
//         showCancelButton: true,
//         reverseButtons: true,
//         text: "Nowa ilo��:",
//         confirmButtonColor: '#1ed671',
//         confirmButtonText: 'Zatwierd�',
//         cancelButtonText: 'Anuluj',
//         buttonsStyling: false,
//         customClass: {
//             confirmButton: 'btn-next',
//         }
//     }).then((result) => {
//         if (result.isConfirmed && isFloat(result.value)) {
//             iloscNew = result.value;

//             const data = {
//                 method: "handle_update_ilosc_in_ti_inwentaryzacja",
//                 tiid: tiid,
//                 ilosc: result.value,
//             }

//             fetch("../InwentaryzacjaAPI.php", {
//                 method: "POST",
//                 body: JSON.stringify(data),
//                 headers: {
//                     "Content-Type": "application/json; charset=Windows-1250;"
//                 }
//             })
//                 .then(() => {
//                     iloscSpan.innerText = result.value;
//                 });
//         }
//     })
//         .then(() => {
//             if (iloscOld != iloscNew) {
//                 Swal.fire({
//                     position: 'center',
//                     icon: 'success',
//                     text: 'Pomy�lnie zmieniono ilo�� z ' + iloscOld + ' na ' + iloscNew,
//                     showConfirmButton: false,
//                     timer: 1000
//                 });
//             }
//         });
// }

function getToken(pozycja) {
    const inputTwid = document.getElementById(`input-twid-${pozycja}`);

    if (inputTwid) {
        const twid = document.getElementById(`input-twid-${pozycja}`).value;
        const ilosc = document.getElementById(`td-ilosc-${pozycja}`).innerText;
        const token = `${pozycja}-${twid}-${ilosc}`;
        return token;
    }
}

function tryLoadCenyFromLocalStorage() {
    const table = document.getElementById("table-towary");

    for (let i = 1; i < table.rows.length - 1; i++) {
        const token = getToken(i);
        const cena = localStorageArray.find(x => x.token === token);

        if (cena) {
            const inputCena = document.getElementsByClassName(token)[0];
            if (inputCena) {
                inputCena.value = cena.cena;
                updateWartosc(i);
            }
        }
    }
}

function trySaveCenaToLocalStorage(pozycja) {
    const token = getToken(pozycja);
    const inputCena = document.getElementsByClassName(token)[0] || null;

    if (inputCena) {
        const cena = inputCena.value || 0;

        if (cena >= 0) {
            const cenaFromLocalStorage = localStorageArray.find(x => x.token === token);

            if (cenaFromLocalStorage) {
                cenaFromLocalStorage.cena = cena;
            } else {
                localStorageArray.push({
                    token: token,
                    cena: cena
                });
            }

            localStorage.setItem(spanIwid.innerText, JSON.stringify(localStorageArray));
        }
    }
}

function tryLoadDisabledRowsFromLocalStorage() {
    const table = document.getElementById("table-towary");

    for (let i = 1; i < table.rows.length - 1; i++) {
        const token = getToken(i);
        const disabled = localStorageDisabledArray.find(x => x.token === token);

        if (disabled) {
            toggleRow(i);
        }
    }
}

function trySaveDisabledRowToLocalStorage(pozycja) {
    const token = getToken(pozycja);
    const disabled = localStorageDisabledArray.find(x => x.token === token);

    if (disabled && !isRowDisabled(pozycja)) {
        localStorageDisabledArray = localStorageDisabledArray.filter(x => x.token !== token);
    } else if (!disabled && isRowDisabled(pozycja)) {
        localStorageDisabledArray.push({
            token: token
        });
    }

    localStorage.setItem(spanIwid.innerText + '-disabled', JSON.stringify(localStorageDisabledArray));
}

function searchTable() {
    const input = document.getElementById("input-search");
    const filter = input.value.toUpperCase();
    const table = document.getElementById("table-towary");
    const tr = table.getElementsByTagName("tr");

    for (let i = 1; i < tr.length - 1; i++) {
        const tdNazwa = tr[i].getElementsByTagName("td")[1];
        const tdEAN = tr[i].getElementsByTagName("td")[2];

        if (tdNazwa || tdEAN) {
            const txtValueNazwa = tdNazwa.textContent || tdNazwa.innerText;
            const txtValueEAN = tdEAN.textContent || tdEAN.innerText;

            if (txtValueNazwa.toUpperCase().indexOf(filter) > -1 || txtValueEAN.toUpperCase().indexOf(filter) > -1) {
                tr[i].classList.remove("hidden");
            } else {
                tr[i].classList.add("hidden");
            }
        }
    }
}

function resetAllInputs() {
    const table = document.getElementById("table-towary");

    for (let i = 1; i < table.rows.length - 1; i++) {
        const inputCena = document.getElementsByClassName(getToken(i))[0];

        if (inputCena) {
            inputCena.value = 0;
            updateWartosc(i);
        }
    }
}

function clearLocalStorage() {
    Swal.fire({
        title: "Usuni�cie wprowadzonych cen",
        text: "Usun�� wszystkie ceny zapisane w pami�ci przegl�darki?",
        showCancelButton: true,
        reverseButtons: true,
        confirmButtonColor: '#1ed671',
        confirmButtonText: 'Tak',
        cancelButtonText: 'Nie',
        buttonsStyling: false,
        customClass: {
            confirmButton: 'btn-next',
        }
    }).then((result) => {
        if (result.isConfirmed) {
            localStorageArray = [];
            localStorage.removeItem(spanIwid.innerText);

            trySaveCenaToLocalStorage();
            resetAllInputs();
        }
    });
}

function createHeaderRow() {
    let headerRow = document.querySelector("#table-towary tr:first-child");
    let cols = headerRow.querySelectorAll("th");
    let header = [];
    for (let i = 0; i < cols.length; i++) {
        header.push(cols[i].innerText);
    }
    return header.join(";");
}

function exportTableToCSV(filename) {
    let csv = [];
    let rows = document.querySelectorAll("#table-towary tr");
    let headerRow = rows[0];
    let cols = headerRow.querySelectorAll("th");
    let header = [];
    for (let i = 0; i < cols.length; i++) {
        header.push(cols[i].innerText);
    }
    csv.push(header.join(";"));
    rows = Array.prototype.slice.call(rows, 1);
    rows = rows.sort(function (a, b) {
        return a.querySelector("td:first-child").innerText - b.querySelector("td:first-child").innerText;
    });

    for (let i = 0; i < rows.length; i++) {
        let row = [], cols = rows[i].querySelectorAll("td, th");

        for (let j = 0; j < cols.length; j++) {
            let input = cols[j].querySelector("input");
            if (input) {
                row.push(input.value);
            } else {
                if (j > 3) {
                    row.push(cols[j].innerText.replace(".", ","));
                } else {
                    row.push(cols[j].innerText);
                }
            }
        }

        csv.push(row.join(";"));
    }

    downloadCSV(csv.join("\n"), filename);
}

function downloadCSV(csv, filename) {
    let encodedCsv = new TextEncoder().encode(csv);
    let csvFile = new Blob(["\ufeff", encodedCsv], { type: "text/csv;charset=utf-8;" });
    let downloadLink = document.createElement("a");

    downloadLink.download = filename;
    downloadLink.href = window.URL.createObjectURL(csvFile);
    downloadLink.style.display = "none";
    document.body.appendChild(downloadLink);
    downloadLink.click();

    document.body.removeChild(downloadLink);
}

function addContextMenu() {
    const table = document.getElementById("table-towary");

    for (let i = 1; i < table.rows.length - 1; i++) {
        const row = table.rows[i];
        const nazwa = row.cells[1];
        const ean = row.cells[2];
        const cena = row.cells[5];
        const context = new Context(`#poz-${i}`);

        const separator = {
            type: "separator"
        };

        const itemCena = {
            type: "item",
            label: "Poka� �r�d�o ceny",
            icon: "<i class='iconoir-info-empty' style='display: inline-block; vertical-align: middle;'></i>",
            callback: () => {
                showDetails(i);
            }
        };

        const contents = [
            {
                type: "item",
                label: "W��cz/wy��cz pozycj�",
                icon: "<i class='iconoir-eye-off' style='display: inline-block; vertical-align: middle;'></i>",
                callback: () => {
                    toggleRow(i);
                }
            },
            separator,
            {
                type: "item",
                label: "Kopiuj nazw�",
                icon: "<i class='iconoir-copy' style='display: inline-block; vertical-align: middle;'></i>",
                callback: () => {
                    copyToClipboard(nazwa.innerText);
                }
            },
            {
                type: "item",
                label: "Kopiuj EAN",
                icon: "<i class='iconoir-copy' style='display: inline-block; vertical-align: middle;'></i>",
                callback: () => {
                    copyToClipboard(ean.innerText);
                }
            },
            separator,
            {
                type: "item",
                label: "Zmie� cen�",
                icon: "<i class='iconoir-edit-pencil' style='display: inline-block; vertical-align: middle;'></i>",
                callback: () => {
                    updateCena(i);
                }
            },
            {
                type: "item",
                label: "Zmie� nazw�",
                icon: "<i class='iconoir-edit-pencil' style='display: inline-block; vertical-align: middle;'></i>",
                callback: () => {
                    updateNazwa(i);
                }
            },
            {
                type: "item",
                label: "Zmie� jedn. miary",
                icon: "<i class='iconoir-edit-pencil' style='display: inline-block; vertical-align: middle;'></i>",
                callback: () => {
                    updateJm(i);
                }
            },
            // {
            //     type: "item",
            //     label: "Zmie� EAN tymczasowo",
            //     icon: "<i class='iconoir-edit-pencil' style='display: inline-block; vertical-align: middle;'></i>",
            //     callback: () => {
            //         updateEanTemp(i);
            //     }
            // }
        ];

        if (!cena.querySelector("input")) {
            contents.push(separator);
            contents.push(itemCena);
        }

        context.add_contents(contents);
    }
}

function copyToClipboard(textToCopy) {
    if (navigator.clipboard && window.isSecureContext) {
        return navigator.clipboard.writeText(textToCopy);
    } else {
        let textArea = document.createElement("textarea");
        textArea.value = textToCopy;
        textArea.style.position = "fixed";
        textArea.style.left = "-999999px";
        textArea.style.top = "-999999px";
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        return new Promise((res, rej) => {
            document.execCommand('copy') ? res() : rej();
            textArea.remove();
        });
    }
}

function updateEanTemp(id) {
    const twid = document.getElementById(`input-twid-${id}`).value;

    const tdEan = document.getElementById(`td-ean-${id}`);
    const inputEan = document.getElementById(`input-ean-${id}`);
    const tdEanClass = `td-ean-twid-${twid}`;
    const inputEanClass = `input-ean-twid-${twid}`;

    if (tdEan) {
        Swal.fire({
            title: "Zmie� EAN tymczasowo",
            input: 'text',
            showCancelButton: true,
            reverseButtons: true,
            confirmButtonColor: '#1ed671',
            confirmButtonText: 'Zmie�',
            cancelButtonText: 'Anuluj',
            inputValue: tdEan.innerText,
            buttonsStyling: false,
            customClass: {
                confirmButton: 'btn-next',
            },
            inputValidator: (value) => {
                if (!value) {
                    return;
                }

                if (!isNumeric(value)) {
                    return 'Pole musi sk�ada� si� z liczb!'
                }

            }
        }).then((result) => {
            if (result.isConfirmed) {
                for (let i = 0; i < document.getElementsByClassName(tdEanClass).length; i++) {
                    document.getElementsByClassName(tdEanClass)[i].innerText = result.value;
                    document.getElementsByClassName(inputEanClass)[i].value = result.value;
                }
            }
        });
    }
}

function isNumeric(str) {
    if (typeof str != "string") return false // we only process strings!  
    return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
        !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
}

function updateInputPozNames() {
    let rows = document.querySelectorAll("#table-towary tr:not(.disabled)");

    rows = Array.prototype.slice.call(rows).sort((a, b) => {
        return a.id.split("-")[1] - b.id.split("-")[1];
    });

    for (let id = 1; id < rows.length - 1; id++) {
        const row = rows[id];
        const poz = row.cells[0].innerText;

        const inputTwid = document.getElementById(`input-twid-${poz}`);
        const inputJmid = document.getElementById(`input-jmid-${poz}`);
        const inputIlosc = document.getElementById(`input-ilosc-${poz}`);
        const inputZl = document.getElementById(`input-zl-${poz}`);
        const inputGr = document.getElementById(`input-gr-${poz}`);
        const inputNazwa = document.getElementById(`input-nazwa-${poz}`);
        const inputEan = document.getElementById(`input-ean-${poz}`);
        const inputDodaneRecznie = document.getElementById(`input-dodane-recznie-${poz}`);
        // New
        const inputPpid = document.getElementById(`input-ppid-${poz}`);
        const inputPznr = document.getElementById(`input-pznr-${poz}`);

        inputTwid.name = `towar[${id}][twid]`;
        inputJmid.name = `towar[${id}][jmid]`;
        inputIlosc.name = `towar[${id}][ilosc]`;
        inputZl.name = `towar[${id}][zl]`;
        inputGr.name = `towar[${id}][gr]`;
        inputNazwa.name = `towar[${id}][nazwa]`;
        inputEan.name = `towar[${id}][ean]`;
        // New
        inputPpid.name = `towar[${id}][ppid]`;
        inputPznr.name = `towar[${id}][pznr]`;

        if (inputDodaneRecznie) {
            inputDodaneRecznie.name = `towar[${id}][dodane_recznie]`;
        }
    }
}

function showDetails(id) {
    const row = document.getElementById(`poz-${id}`);
    const nazwa = row.cells[1].innerText;
    const cena = row.cells[5].innerText;
    const pznr = document.getElementById(`input-pznr-${id}`).value;

    Swal.fire(
        {
            showConfirmButton: false,
            html: `
            <table style="table-layout: auto;">

            <tbody>
                <tr>
                    <td><strong>Nazwa</strong></td>
                    <td>${nazwa}</td>    
                </tr>
                <tr>
                    <td><strong>Cena</strong></td>
                    <td>${cena}</td>    
                </tr>
                <tr>
                    <td><strong>Numer PZ</strong></td>
                    <td>${pznr}</td>    
                </tr>
            </tbody>
        </table>`
        }
    );
}

function toggleRow(id) {
    const row = document.getElementById(`poz-${id}`);

    if (row.classList.contains('disabled')) {
        row.classList.remove('disabled');

        const inputs = row.querySelectorAll("input");
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].disabled = false;
        }
    } else {
        row.classList.add('disabled');

        const inputs = row.querySelectorAll("input");
        for (let i = 0; i < inputs.length; i++) {
            inputs[i].disabled = true;
        }
    }

    updateInputPozNames();
    updateWartosc(id);
    updateSuma();
    trySaveDisabledRowToLocalStorage(id);
}

function isRowDisabled(id) {
    const row = document.getElementById(`poz-${id}`);

    return row.classList.contains('disabled');
}



addContextMenu();