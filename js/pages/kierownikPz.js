const inputDataOd = document.getElementById('input-data-od');
const inputDataDo = document.getElementById('input-data-do');
const inputNumer = document.getElementById('input-numer');
const inputKontrahent = document.getElementById('input-kontrahent');

const selectDzial = document.getElementById('select-dzial');
const selectKontrahent = document.getElementById('select-kontrahent');

const divRestrict = document.getElementById('div-restrict');
const spanPzTitle = document.getElementById('span-dokument-pz-title');
const spanListaDokumentow = document.getElementById('span-lista-dokumentow');

const tbodyDokumentyPz = document.getElementById('tbody-dokumenty-pz');
const tbodyPozycje = document.getElementById('tbody-pozycje');
const tdSuma = document.getElementById('td-suma');

let pzid = null;
let pznr = null;
let pzdata = null;

getDokumentyPz();

function updateRestriction() {
    if (tbodyDokumentyPz.innerHTML == '') {
        divRestrict.classList.add('disabled-zone');
    } else {
        divRestrict.classList.remove('disabled-zone');
    }
}

function clearData() {
    tbodyPozycje.innerHTML = '';
    spanPzTitle.innerHTML = '';
    tdSuma.innerHTML = '0 z�';
}

function getKontrahenci() {
    if (inputKontrahent.value.length < 3) {
        selectKontrahent.innerHTML = '';
        updateRestriction();
        return;
    }

    const data = {
        method: "handle_display_kontrahenci_for_select",
        input_kontrahent: inputKontrahent.value,
    };

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => selectKontrahent.innerHTML = newInnerHTML)
        .then(updateRestriction);
}

function getDokumentyPz() {
    const data = {
        method: "handle_display_dokumenty_pz_for_table",
        pz_nr: inputNumer.value,
        mj_id: selectDzial.value,
        data_od: inputDataOd.value,
        data_do: inputDataDo.value,
        kh_id: selectKontrahent.value,
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => {
            tbodyDokumentyPz.innerHTML = newInnerHTML;
            spanListaDokumentow.innerHTML = ` &rarr; ${selectDzial.options[selectDzial.selectedIndex].text}`;
        })
        .then(clearData)
        .then(updateRestriction);
}

function getPozycjeByDokumentPz(thatPzid, thatPznr, thatPzdata) {
    spanPzTitle.innerHTML = `&rarr; <span class='standout-text'>${selectDzial.options[selectDzial.selectedIndex].text}, ${thatPznr}</span>`;

    this.pzid = thatPzid;
    this.pznr = thatPznr;
    this.pzdata = thatPzdata;

    const trs = document.querySelectorAll('tr[id^="tr-"]');
    trs.forEach(tr => {
        if (tr.id == `tr-${thatPzid}`) {
            tr.classList.add('selected-row');
        } else {
            tr.classList.remove('selected-row');
        }
    });

    const data = {
        method: "handle_display_pozycje_by_dokument_pz_for_table",
        pz_id: thatPzid,
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => tbodyPozycje.innerHTML = newInnerHTML)
        .then(updateSuma);
}

function updateSuma() {
    const table = document.getElementById("table-pozycje");

    let subTotal = Array.from(table.rows).slice(1, -2).reduce((total, row) => {
        return total + parseFloat(row.cells[4].innerHTML);
    }, 0);

    tdSuma.innerHTML = `${subTotal.toFixed(2)} z�`;
}