const tbodyElement = document.getElementById('tbody-element');
const inputSumyCzesciowe = document.getElementById('input-sumy-czesciowe');
const inputTylkoUzytkownika = document.getElementById('input-tylko-uzytkownika');
const inputSearch = document.getElementById('input-search');
const tablePoliczone = document.getElementById('table-policzone');

function searchTable() {
    const filter = inputSearch.value.toUpperCase();
    const tr = tablePoliczone.getElementsByTagName('tr');

    for (let i = 0; i < tr.length; i++) {
        const td = tr[i].getElementsByTagName('td')[0];
        if (td) {
            const txtValue = td.textContent || td.innerText;
            if (txtValue.toUpperCase().indexOf(filter) > -1) {
                tr[i].style.display = "";
            } else {
                tr[i].style.display = "none";
            }
        }
    }
}

function displayTowarPoliczony() {
    const data = {
        method: "handle_display_towar_policzony_by_for_table",
        subtotal: inputSumyCzesciowe.checked,
        byUzytkownik: inputTylkoUzytkownika.checked,
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => tbodyElement.innerHTML = newInnerHTML);
}

function editTowar(tiid) {
    const nazwa = document.getElementById('td-nazwa-' + tiid);
    const ilosc = document.getElementById('td-ilosc-' + tiid);
    const iloscSpan = document.getElementById('span-ilosc-' + tiid);

    Swal.fire({
        title: nazwa.innerText,
        showDenyButton: true,
        text: "Wprowadzona ilo��: " + ilosc.innerText,
        confirmButtonColor: '#1ed671',
        confirmButtonText: 'Zmie� ilo��',
        denyButtonText: 'Usu�',
        buttonsStyling: false,
        customClass: {
            denyButton: 'btn-danger',
        }
    }).then((result) => {
        if (result.isConfirmed) {
            changeIlosc(tiid);
        } else if (result.isDenied) {
            deleteTowar(tiid);
        }
    });
}

function changeIlosc(tiid) {
    const nazwa = document.getElementById('td-nazwa-' + tiid);
    const ilosc = document.getElementById('td-ilosc-' + tiid);
    const iloscSpan = document.getElementById('span-ilosc-' + tiid);

    const iloscOld = document.getElementById('span-ilosc-' + tiid).innerText;
    var iloscNew = iloscOld;

    Swal.fire({
        title: nazwa.innerText,
        input: 'number',
        showCancelButton: true,
        reverseButtons: true,
        text: "Nowa ilo��:",
        confirmButtonColor: '#1ed671',
        confirmButtonText: 'Zatwierd�',
        cancelButtonText: 'Anuluj',
        buttonsStyling: false,
        customClass: {
            confirmButton: 'btn-next',
        }
    }).then((result) => {
        if (result.isConfirmed) {
            if (isFloat(result.value)) {
                iloscNew = result.value;

                const data = {
                    method: "handle_update_ilosc_in_ti_inwentaryzacja",
                    tiid: tiid,
                    ilosc: result.value,
                }

                fetch("../InwentaryzacjaAPI.php", {
                    method: "POST",
                    body: JSON.stringify(data),
                    headers: {
                        "Content-Type": "application/json; charset=Windows-1250;"
                    }
                })
                    .then(() => {
                        iloscSpan.innerText = result.value;
                    });
            }
        }
    })
        .then(() => {
            if (iloscOld != iloscNew) {
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    text: 'Pomy�lnie zmieniono ilo�� z ' + iloscOld + ' na ' + iloscNew,
                    showConfirmButton: false,
                    timer: 1000
                });
            }
        });
}

function deleteTowar(tiid) {
    const nazwa = document.getElementById('td-nazwa-' + tiid);
    const ilosc = document.getElementById('td-ilosc-' + tiid);
    const iloscSpan = document.getElementById('span-ilosc-' + tiid);

    Swal.fire({
        title: nazwa.innerText,
        showCancelButton: true,
        reverseButtons: true,
        text: "Czy na pewno chcesz usun�� wybran� inwentaryzacj�?",
        confirmButtonColor: '#1ed671',
        confirmButtonText: 'Tak',
        cancelButtonText: 'Nie',
        buttonsStyling: false,
        customClass: {
            confirmButton: 'btn-next',
        }
    }).then((result) => {
        if (result.isConfirmed) {
            const data = {
                method: "handle_delete_from_ti_inwentaryzacja",
                tiid: tiid,
            }

            fetch("../InwentaryzacjaAPI.php", {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json; charset=Windows-1250;"
                }
            })
        }
    }).then(() => {
        Swal.fire({
            position: 'center',
            icon: 'success',
            text: 'Pomy�lnie usuni�to inwentaryzacj� ' + nazwa.innerText,
            showConfirmButton: false,
            timer: 1000
        });

        //wait 2500ms and reload page
        setTimeout(function () {
            location.reload();
        }, 1000);
    });
}

function isFloat(val) {
    val = parseFloat(val);

    return !isNaN(val);
}