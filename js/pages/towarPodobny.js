const inputNazwa = document.getElementById('nazwa');
const inputEan = document.getElementById('ean');
const inputSubmit = document.getElementById('submit-input');
const inputNowy = document.getElementById('towar_nowy');
const inputTwid = document.getElementById('twid');

const buttonsTowary = document.querySelectorAll('.btn-item');

const defaultNazwa = inputNazwa.value;
const defaultEan = inputEan.value;

const strNowyTowar = 'Kontynuuj tworzenie nowego towaru';
const strWybranyTowar = 'Kontynuuj z wybranym towarem';
const strWybranyTowarEan = 'Powi�� wybrany towar z nowym EAN';

const selectedClass = 'btn-selected';

let iSelectedBtn = 0;
inputSubmit.value = strNowyTowar;

hideNoEanValue();
inputEan.style.transition = "none";

for (let i = 0; i < buttonsTowary.length; i++) {
    buttonsTowary[i].addEventListener('click', () => {

        if (iSelectedBtn != i) {
            buttonsTowary[iSelectedBtn].classList.remove(selectedClass);
        }
        buttonsTowary[i].classList.toggle(selectedClass);

        iSelectedBtn = i;

        if (areAllDisabled()) {
            inputNazwa.value = defaultNazwa;
            inputEan.value = defaultEan;
            inputSubmit.value = strNowyTowar;
            inputNowy.value = '1';
            inputTwid.value = '-1';
        } else {
            inputNazwa.value = buttonsTowary[i].children[0].children[0].children[0].children[0].textContent.trim();
            if (defaultEan == '-1') {
                inputEan.value = buttonsTowary[i].children[0].children[0].children[0].children[1].children[0].textContent.trim();
                console.log(inputEan.value);
            }
            if (inputEan.value == '-1' || defaultEan == '-1') {
                inputSubmit.value = strWybranyTowar;
                inputNowy.value = '1';
                inputTwid.value = buttonsTowary[i].id;
            } else {
                inputSubmit.value = strWybranyTowarEan;
                inputNowy.value = '0';
                inputTwid.value = buttonsTowary[i].id;
            }
        }

        hideNoEanValue();
    });
}

function areAllDisabled() {
    for (const element of buttonsTowary) {
        if (element.classList.contains(selectedClass)) {
            return false;
        }
    }
    return true;
}

function hideNoEanValue() {
    if (inputEan.value == '-1') {
        inputEan.classList.add("hide-text");
    } else {
        inputEan.classList.remove("hide-text");
    }
}