const showConfirm = true;
const noEanCode = '-1';

const inputNazwa = document.getElementById('nazwa');
const inputJm = document.getElementById('jm');
const inputEan = document.getElementById('ean');
const labelEan = document.getElementById('label-ean');
const inputCheckboxEan = document.getElementById('input-ean');
const brEan = document.getElementById('br-ean');
const eanCode = (inputEan.value == noEanCode) ? '' : inputEan.value;

inputCheckboxEan.checked = (inputEan.value == noEanCode);
updateEANCheckBox();

function handleEANCheckBoxClick() {
    updateEANCheckBox();
}

function updateEANCheckBox() {
    let isChecked = inputCheckboxEan.checked;
    inputEan.readOnly = isChecked;

    if (isChecked) {
        inputEan.classList.add("hide-text");
        inputEan.style.cursor = "not-allowed";
        inputEan.style.opacity = "0.5";
        inputEan.value = noEanCode;
        inputEan.style.transition = "none";
    } else {
        inputEan.classList.remove("hide-text");
        inputEan.style.cursor = "auto";
        inputEan.style.opacity = "1.0";
        inputEan.value = eanCode;
        inputEan.style.transition = "none";
    }
}

function confirmSubmit() {
    if (!isEmpty(inputNazwa.value) && /^[a-zA-Z0-9 .,-]*$/.test(inputNazwa.value) && !isEmpty(inputEan.value) && !isEmpty(inputJm.value) && /^-?[0-9]+$/.test(inputEan.value)) {
        if (showConfirm) {
            const msgHtml = `
                <table width="100%">
                    <tbody>
                        <tr>
                            <td width="25%"><strong>Nazwa</strong></td>
                            <td>${inputNazwa.value}</td>
                        </tr>
                        ` + ((inputEan.value != '-1') ? `
                        <tr>
                            <td><strong>EAN</strong></td>
                            <td>${inputEan.value}</td>
                        </tr>` : ``) + `
                        <tr>
                            <td><strong>Jm</strong></td>
                            <td>${inputJm.options[inputJm.selectedIndex].text}</td>
                        </tr>
                    </tbody>
                </table>`;

            Swal.fire({
                title: 'Czy dane si� zgadzaj�?',
                html: msgHtml,
                showCancelButton: true,
                confirmButtonColor: '#1ed671',
                confirmButtonText: '<strong>Tak</strong>',
                cancelButtonText: 'Nie',
                reverseButtons: true,
                buttonsStyling: false,
                customClass: {
                    confirmButton: 'btn-next',
                }
            }).then((result) => {
                if (result.isConfirmed) {
                    document.getElementById("myForm").submit();
                }
            });
        } else {
            document.getElementById("myForm").submit();
        }
    }
}

function isEmpty(str) {
    return !str.trim().length;
}
