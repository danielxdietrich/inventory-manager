const inputOsoba = document.getElementById('input-osoba');
const inputDataOd1 = document.getElementById('input-data-od1');
const inputDataDo1 = document.getElementById('input-data-do1');
const inputDataOd2 = document.getElementById('input-data-od2');
const inputDataDo2 = document.getElementById('input-data-do2');
const inputInwentaryzacjaNr = document.getElementById('input-inwentaryzacja-nr');
const inputInwentaryzacjaData = document.getElementById('input-inwentaryzacja-data');

const selectOsoba = document.getElementById('select-osoba');
const selectDzial1 = document.getElementById('select-dzial1');
const selectDzial2 = document.getElementById('select-dzial2');
const selectStan = document.getElementById('select-stan');

const h3Lista = document.getElementById('h3-lista');
const tbodyPoliczone = document.getElementById('tbody-policzone');
const divRestrict = document.getElementById('div-restrict');

getUzytkownicy(1);
displayTowarPoliczony();

function updateRestriction() {
    if (tbodyPoliczone.innerHTML == '') {
        divRestrict.classList.add('disabled-zone');
    } else {
        divRestrict.classList.remove('disabled-zone');
    }
}

function handleCheckboxOsoba() {
    selectOsoba.disabled = !inputOsoba.checked;
}

function selectDzial1Click() {
    displayTowarPoliczony();
    getUzytkownicy(selectDzial1.value);
    selectDzial2.value = selectDzial1.value;
}

function getUzytkownicy(mjid) {
    const data = {
        method: "handle_display_uzytkownicy_for_select",
        mj_id: mjid,
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => selectOsoba.innerHTML = newInnerHTML);
}

function displayTowarPoliczony() {
    const data = {
        method: "handle_display_towar_policzony_for_table",
        mj_id: selectDzial1.value,
        data_od: inputDataOd1.value,
        data_do: inputDataDo1.value,
        uz_id: selectOsoba.disabled ? "-1" : selectOsoba.value,
        stan: selectStan.value
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => tbodyPoliczone.innerHTML = newInnerHTML)
        .then(updateRestriction)
        .then(() => {
            displayLastActivity();

            const dateString = (inputDataOd1.value).split('T')[0].replace(".", "-");
            const date = new Date(dateString);
            const lastDayCurrentMonth = new Date(date.getFullYear(), date.getMonth() + 1, 1);

            inputInwentaryzacjaData.value = lastDayCurrentMonth.toISOString().split('T')[0];
            inputDataOd2.value = inputDataOd1.value;
            inputDataDo2.value = inputDataDo1.value;
        });
}

function displayLastActivity() {
    const data = {
        method: "handle_display_last_activity",
        mjid: selectDzial1.value,
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => h3Lista.innerHTML = newInnerHTML);
}

function getLastDayOfMonth(year, month) {
    return new Date(year, month + 1, 0);
}


function assignTowaryPoliczoneToInwentaryzacja() {
    const data = {
        method: "handle_assign_towary_policzone_to_inwentaryzacja",
        inw_nr: inputInwentaryzacjaNr.value,
        mj_id: selectDzial2.value,
        data_inw: inputInwentaryzacjaData.value,
        data_od: inputDataOd2.value,
        data_do: inputDataDo2.value
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newData) => {
            let notificationIcon = '';
            let notificationText = '';

            if (newData == 'OK') {
                displayTowarPoliczony();

                notificationIcon = 'success';
                notificationText = `Inwentaryzacja '${inputInwentaryzacjaNr.value} zosta�a utworzona`;

                inputInwentaryzacjaNr.value = '';
                inputInwentaryzacjaData.value = '';
                inputDataOd2.value = '';
                inputDataDo2.value = '';
            } else if (newData == 'ERROR') {
                notificationIcon = 'error';
                notificationText = 'Niekt�re pozycje w danym zakresie dat zosta�y ju� zamkni�te';
            }

            Swal.fire({
                position: 'center',
                icon: notificationIcon,
                text: notificationText,
                showConfirmButton: false,
                timer: 2500
            });
        });
}