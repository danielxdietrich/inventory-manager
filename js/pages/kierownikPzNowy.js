const inputKontrahent = document.getElementById('input-kontrahent');
const inputNumer = document.getElementById('input-numer');
const inputData = document.getElementById('input-data');
const inputEanNazwa = document.getElementById('input-ean-nazwa');

const selectKontrahent = document.getElementById('select-kontrahent');
const selectTowar = document.getElementById('select-towar');
const selectDzial = document.getElementById('select-dzial');

const table = document.getElementById("table-towary");
const tbodyTowaryKontrahent = document.getElementById('tbody-towary-kontrahent');
const tdSuma = document.getElementById('td-suma');
const divRestrict = document.getElementById('div-restrict');

let trPozycje = document.querySelectorAll('.tr-pozycja');

// [0]: RW, [1]: FP
let dokumentyCacheArray = ["", ""];
let miejscaSelectCache = "";

getMiejscaForSelect();
getKontrahenci();

function updateRestriction() {
    if (selectKontrahent.innerHTML == '') {
        divRestrict.classList.add('disabled-zone');
    } else {
        divRestrict.classList.remove('disabled-zone');
    }
}

function clearTowarForm() {
    inputEanNazwa.value = '';
    selectTowar.innerHTML = '';
}

function clearKontrahent() {
    inputKontrahent.value = '';
    inputData.value = '';
    inputNumer.value = '';
}

function getKontrahenci() {
    if (inputKontrahent.value.length < 3) {
        selectKontrahent.innerHTML = '';
        tbodyTowaryKontrahent.innerHTML = '';

        clearTowarForm();
        updateRestriction();
        return;
    }

    const data = {
        method: "handle_display_kontrahenci_for_select",
        input_kontrahent: inputKontrahent.value,
    };

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => selectKontrahent.innerHTML = newInnerHTML)
        .then(updateRestriction);
}

function displayTowaryForSelect() {
    if (inputEanNazwa.value.length < 3) {
        selectTowar.innerHTML = '';
        return;
    }

    const data = {
        method: "handle_display_towary_for_select",
        userInput: inputEanNazwa.value,
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => selectTowar.innerHTML = newInnerHTML);
}

function displayTowaryKontrahentaForTable(numer) {
    if (numer == "") { return; }

    const data = {
        method: "handle_display_towary_kontrahenta_for_table",
        nr: numer
    }

    Swal.fire({
        position: 'center',
        text: 'Trwa pobieranie danych z dokumentu',
        showConfirmButton: false,
        allowOutsideClick: false,
        allowEscapeKey: false,
        didOpen: () => {
            Swal.showLoading();
        },
    });

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => tbodyTowaryKontrahent.innerHTML = newInnerHTML)
        .then(() => { this.trPozycje = document.querySelectorAll('.tr-pozycja'); })
        .then(updateSuma)
        .then(() => { Swal.close() })
        .catch((error) => {
            Swal.close();
            Swal.fire({
                title: "Wyst�pi� b��d",
                html: `Nie uda�o si� pobra� danych z dokumentu<br/><br/><code> ${error} </code>`,
                icon: "error",
                confirmButtonText: "Ok",
                customClass: {
                    confirmButton: 'btn-next',
                }
            });
        });

}

function displayTowarKontrahentaForTable(ktid) {
    const data = {
        method: "handle_display_towar_kontrahenta_for_table",
        kt_id: ktid,
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newInnerHTML) => tbodyTowaryKontrahent.insertAdjacentHTML("afterbegin", newInnerHTML))
        .then(() => { this.trPozycje = document.querySelectorAll('.tr-pozycja'); });
}

function assignTowarToKontrahent() {
    if (selectTowar.innerHTML == '') { return; }

    const data = {
        method: "handle_assign_towar_to_kontrahent",
        kh_id: selectKontrahent.value,
        tw_id: selectTowar.value,
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((ktid) => {
            if (ktid == "ERROR") {
                return;
            }

            inputEanNazwa.value = '';
            selectTowar.innerHTML = '';

            displayTowarKontrahentaForTable(ktid);
            this.trPozycje = document.querySelectorAll('.tr-pozycja');
        });
}

function newCompleteDokumentPz() {
    const msgHtml = `
                <table width="100%">
                    <tbody>
                        <tr>
                            <td width="25%"><strong>Numer</strong></td>
                            <td>${inputNumer.value}</td>
                        </tr>
                        <tr>
                            <td><strong>Data</strong></td>
                            <td>${inputData.value}</td>
                        </tr>
                        <tr>
                            <td><strong>Dzia�</strong></td>
                            <td>${selectDzial.options[selectDzial.selectedIndex].text}</td>
                        </tr>
                    </tbody>
                </table>`;

    Swal.fire({
        title: 'Czy dane si� zgadzaj�?',
        html: msgHtml,
        showCancelButton: true,
        confirmButtonColor: '#1ed671',
        confirmButtonText: '<strong>Tak</strong>',
        cancelButtonText: 'Nie',
        reverseButtons: true,
        buttonsStyling: false,
        customClass: {
            confirmButton: 'btn-next',
        }
    }).then((result) => {
        if (result.isConfirmed) {
            const data = {
                method: "handle_check_if_pz_already_exists",
                numer: inputNumer.value,
                data: inputData.value,
            };

            fetch("../InwentaryzacjaAPI.php", {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json; charset=Windows-1250;"
                }
            })
                .then((response) => response.json())
                .then((newData) => {
                    if (newData == 'YES') {
                        Swal.fire({
                            position: 'center',
                            icon: 'info',
                            text: 'Dokument o podanym numerze ju� istnieje',
                            showConfirmButton: false,
                            timer: 2500
                        });
                    } else if (newData == 'NO') {
                        const data = {
                            method: "handle_new_complete_dokument_pz",
                            numer: inputNumer.value,
                            data: inputData.value,
                            khid: selectKontrahent.value,
                            mjid: selectDzial.value,
                            pozycje: JSON.stringify(getTableData()),
                        };

                        fetch("../InwentaryzacjaAPI.php", {
                            method: "POST",
                            body: JSON.stringify(data),
                            headers: {
                                "Content-Type": "application/json; charset=Windows-1250;"
                            }
                        })
                            .then((response) => response.json())
                            .then((newData) => {
                                let notificationIcon = '';
                                let notificationText = '';

                                if (newData == 'OK') {
                                    notificationIcon = 'success';
                                    notificationText = `Dokument ${inputNumer.value} zosta� utworzony`;

                                    clearKontrahent();
                                    getKontrahenci();
                                } else if (newData == 'ERROR') {
                                    notificationIcon = 'error';
                                    notificationText = 'Wyst�pi� b��d podczas tworzenia dokumentu i jego pozycji';
                                }

                                Swal.fire({
                                    position: 'center',
                                    icon: notificationIcon,
                                    text: notificationText,
                                    showConfirmButton: false,
                                    timer: 2500
                                });
                            });
                    }
                });
        }
    });
}

function wczytajDokumentMain() {
    const msgHtml = `
        <div style='margin: 5%;' width="100%">
            <select onchange='getNumeryDokumentowMain();' name='select-miejsca-main' id='select-miejsca-main' style='width: 100%' value='${selectDzial.value}'>${miejscaSelectCache}</select>
            <input oninput='filtrujNumeryDokumentowMain()' placeholder='Numer dokumentu' type='text' id='input-numer-main' name='input-numer-main' style='width: 100%; padding-left: 0; padding-right: 0; margin-right: 0; text-align: center;'>
            <select name='select-dokument-main' id='select-dokument-main' style='width: 100%'></select>
        </table>`;

    Swal.fire({
        title: 'Wyszukaj numer dokumentu',
        html: msgHtml,
        confirmButtonColor: '#1ed671',
        confirmButtonText: '<strong>Wczytaj wybrany dokument z marketu</strong>',
        reverseButtons: true,
        buttonsStyling: false,
        customClass: {
            confirmButton: 'btn-next',
        }
    }).then((result) => {
        const nr = document.getElementById('select-dokument-main').value;
        const dzial = document.getElementById('select-miejsca-main').selectedIndex;

        if (result.isConfirmed) {
            const data = {
                method: "handle_get_date_dokumentu",
                numer: nr,
            }

            fetch("../InwentaryzacjaAPI.php", {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json; charset=Windows-1250;"
                }
            })
                .then((response) => response.json())
                .then((dataDokumentu) => {
                    inputKontrahent.value = "";
                    inputNumer.value = nr;
                    selectDzial.selectedIndex = dzial;
                    inputData.value = dataDokumentu;

                    getKontrahenci();
                    displayTowaryKontrahentaForTable(nr);
                });
        }
    });

    getNumeryDokumentowMain();
}

function getMiejscaForSelect() {
    if (miejscaSelectCache != "") {
        return miejscaSelectCache;
    }

    const data = {
        method: "handle_get_mj_for_select",
    }

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((miejsca) => { miejscaSelectCache = miejsca; });

    return miejscaSelectCache;
}

function filtrujNumeryDokumentowMain() {
    const input = document.getElementById('input-numer-main');
    const select = document.getElementById('select-dokument-main');

    for (let i = 0; i < select.options.length; i++) {
        if (select.options[i].text.includes(input.value)) {
            select.options[i].style.display = 'block';
        } else {
            select.options[i].style.display = 'none';
        }
    }

    select.selectedIndex = getFirstNotHiddenIndex();
}

function getFirstNotHiddenIndex() {
    const select = document.getElementById('select-dokument-main');
    for (let i = 0; i < select.options.length; i++) {
        if (select.options[i].style.display == 'block') {
            return i;
        }
    }
}

function getNumeryDokumentowMain() {
    const selectDokumenty = document.getElementById('select-dokument-main');
    const selectDzialmain = document.getElementById('select-miejsca-main');

    const isRW = (selectDzialmain.value == 1 || selectDzialmain.value == 3) ? "1" : "0";

    if (dokumentyCacheArray[isRW] != "") {
        selectDokumenty.innerHTML = dokumentyCacheArray[isRW];
        return;
    }

    const data = {
        method: "handle_get_numery_dokumentow_main",
        rw: isRW,
    };

    fetch("../InwentaryzacjaAPI.php", {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
            "Content-Type": "application/json; charset=Windows-1250;"
        }
    })
        .then((response) => response.json())
        .then((newData) => {
            selectDokumenty.innerHTML = newData;
            dokumentyCacheArray[isRW] = newData;
        });
}

function getTableData() {
    let data = new Array();
    this.tbodyTowaryKontrahent = document.getElementById('tbody-towary-kontrahent');

    // LOOP THROUGH EACH ROW OF THE TABLE AFTER HEADER.
    for (let i = 0; i < tbodyTowaryKontrahent.rows.length; i++) {
        // GET THE CELLS COLLECTION OF THE CURRENT ROW.
        let objCells = tbodyTowaryKontrahent.rows.item(i).cells;

        let twid = tbodyTowaryKontrahent.rows.item(i).attributes["twid"].value;
        let ilosc = objCells.item(2).childNodes[1].value;
        let cena = objCells.item(4).childNodes[1].value;

        if (ilosc == '' || cena == '') {
            continue;
        }

        data[i] = new Array(twid, ilosc, cena);
    }

    return filter_array(data);
}

function updateWartosc(ktid) {
    const inputIlosc = document.getElementById(`input-ilosc-${ktid}`);
    const inputCena = document.getElementById(`input-cena-${ktid}`);
    const inputWartosc = document.getElementById(`input-wartosc-${ktid}`);

    if (inputIlosc.value != '' || inputCena.value != '') {
        inputIlosc.required = true;
        inputCena.required = true;
    } else {
        inputIlosc.required = false;
        inputCena.required = false;
    }

    if (inputIlosc.value != '' && inputCena.value != '') {
        const ilosc = parseFloat(inputIlosc.value.replace(',', '.'));
        const cena = parseFloat(inputCena.value.replace(',', '.'));

        const wartosc = (ilosc * cena).toFixed(2);

        inputWartosc.innerHTML = `${wartosc}`;
    } else {
        inputWartosc.innerHTML = ``;
    }

    updateSuma();
}

function filter_array(test_array) {
    let index = -1,
        arr_length = test_array ? test_array.length : 0,
        resIndex = -1,
        result = [];

    while (++index < arr_length) {
        let value = test_array[index];

        if (value) {
            result[++resIndex] = value;
        }
    }

    return result;
}

function updateSuma() {
    let subTotal = Array.from(table.rows).slice(1, -1).reduce((total, row) => {
        const value = row.cells[5].innerText.trim() || 0;
        return Number(parseFloat(total) + parseFloat(value)).toFixed(2);
    }, 0);

    tdSuma.innerHTML = `${subTotal} z�`;
}
